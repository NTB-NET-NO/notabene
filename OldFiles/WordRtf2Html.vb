Imports Microsoft.Office.Interop
Imports System.Text
Imports System.Xml.Xsl
Imports System.Xml
Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

Public Class WordRtf2Html
    'Implements System.ComponentModel.ISynchronizeInvoke

    'Public Overridable Overloads Function BeginInvoke(ByVal method As System.Delegate, ByVal args() As Object) As System.IAsyncResult

    'End Function

    Private objWord As Word.ApplicationClass
    Private objWordDoc As Word.DocumentClass
    Private xslTrans As Xsl.XslTransform

    Public Sub New()
        objWord = New Word.ApplicationClass()
        xslTrans = New Xsl.XslTransform()
        xslTrans.Load("..\XSLT\WordBody2nitf.xsl")
        'objWordDoc = New DocumentClass()
    End Sub

    Public Sub Close()
        objWord.Quit()
        objWordDoc = Nothing
        objWord = Nothing
    End Sub

    Private xsltNitf As XslTransform
    Private xsltHead As XslTransform

    Private xslt4Proc As MSXML2.IXSLProcessor  ' XSLT Problem workaround

    Public Sub LoadXsltFiles(ByVal xsltFile As String, ByVal strXsltHead As String)
        xsltNitf = New XslTransform()
        xsltNitf.Load(xsltFile)

        xsltHead = New XslTransform()
        xsltHead.Load(strXsltHead)

        ' XSLT Problem workaround, using MSXML4.DLL instead of .NET-xml components :       
        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate()
        xsltDoc = New MSXML2.FreeThreadedDOMDocument()

        xsltDoc.load(xsltFile)
        xmlError = xsltDoc.parseError

        If (xmlError.errorCode <> 0) Then
            MsgBox(xmlError.reason & " i linje: " & xmlError.line)
        End If
        xslt.stylesheet = xsltDoc
        xslt4Proc = xslt.createProcessor
        ' End XSLT Problem workaround:        

    End Sub

    Public Function ConvertDoc(ByVal strFileName As String, Optional ByVal strXmlHead As String = "", Optional ByVal strTempFile As String = "", Optional ByVal bDeleteTempFiles As Boolean = False) As String

        If strTempFile = "" Then
            strTempFile = Path.GetDirectoryName(strFileName) & Path.DirectorySeparatorChar & Path.GetFileNameWithoutExtension(strFileName) & ".xml"
        End If

        objWordDoc = New Word.DocumentClass()

        objWordDoc = objWord.Documents.Open(strFileName)

        Try
        Catch e As Exception
            If e.Message = "The RPC server is unavailable." Then
                objWord = New Word.ApplicationClass()
                objWordDoc = objWord.Documents.Open(strFileName)
            Else
                Throw e
            End If
        End Try

        'Lagrer som XML og lukker
        'objWordDoc.SaveAs(strTempFile, Word.WdSaveFormat.wdFormatHTML)
        objWordDoc.SaveAs(strTempFile, Word.WdSaveFormat.wdFormatXML)
        objWordDoc.Close()
        objWordDoc = Nothing

        'Dim strWordML As String = LogFile.ReadFile(strTempFile)

        Dim xlmDoc As New XmlDocument()
        xlmDoc.Load(strTempFile)

        Dim strm As New IO.MemoryStream()
        Dim sr As New StreamReader(strm, Encoding.GetEncoding("iso-8859-1"))
        Dim xmlWr As New XmlTextWriter(strm, Encoding.GetEncoding("iso-8859-1"))

        xslTrans.Transform(xlmDoc, Nothing, strm)

        Dim strNITFBody As String
        'strm.Position = 0
        'strNITFBody  = sr.ReadToEnd


        Stop

        If bDeleteTempFiles Then
            File.Delete(strTempFile)
        End If

        Return strNITFBody
        'Return WordXmlTidy(strHtml, strXmlHead)

    End Function

    Public Function WordXmlTidy(ByVal strHtml As String, Optional ByRef strXmlHead As String = "") As String
        Dim swXml As StringBuilder = New StringBuilder()
        Dim intStart, intSlutt, i As Integer
        Dim chrOne As Char
        Dim bTag, bAttr, bSetQuot, bEndTag, bEqual As Boolean
        Dim strTemp As String
        Dim intStartHtml, intStartElem, intEndElem As Integer

        'Dim strHtml As String = strHtmlInn.Replace("&nbsp;", "")

        swXml.Append("<html>")

        'BulkReplace(strHtml, "<style>", "</style>", "<style/>", 0, -1)
        'BulkReplace(strHtml, "<!--[if", "<![endif]-->", "<if-endif/>", 0, -1)
        'BulkReplace(strHtml, "<![if", "<![endif]>", "<if-endif2/>", 0, -1)
        'BulkReplace(strHtml, "<span ", ">", "<span>", 0, -1)

        strHtml = BulkReplace(strHtml, "<style>", "</style>", "<style/>", 0, -1)
        strHtml = BulkReplace(strHtml, "<!--[if", "<![endif]-->", "<if-endif/>", 0, -1)
        strHtml = BulkReplace(strHtml, "<![if", "<![endif]>", "<if-endif2/>", 0, -1)
        strHtml = BulkReplace(strHtml, "<span ", ">", "<span>", 0, -1)

        strHtml = BulkReplace(strHtml, "<link", ">", "<link/>", 0, -1)
        'strHtml = PairReplace(strHtml, "<link", ">", "<link", "/>", 0, -1)

        strHtml = PairReplace(strHtml, "<meta", ">", "<meta", "/>", 0, -1)
        strHtml = PairReplace(strHtml, "<img", ">", "<img", "/>", 0, -1)
        strHtml = PairReplace(strHtml, "<br", ">", "<br", "/>", 0, -1)


        intStartHtml = strHtml.IndexOf(">") + 1
        intEndElem = intStartHtml
        intStart = intStartHtml

        Do
            intStartElem = strHtml.IndexOf("<", intEndElem)
            If intStartElem = -1 Then
                Exit Do
            Else
                swXml.Append(strHtml.Substring(intStart, intStartElem - intStart))
            End If

            intEndElem = strHtml.IndexOf(">", intStartElem) + 1

            ' Behandler innhold av tagger: (atributter)
            strTemp = strHtml.Substring(intStartElem, intEndElem - intStartElem)
            strTemp = CleanAttributes(strTemp)
            swXml.Append(strTemp)

            intStart = intEndElem

            'For Debug:
            'strTemp = swXml.ToString
        Loop

        swXml.Append(strHtml.Substring(intStart))

        swXml.Replace("&nbsp;", "")

        '       swXml.Replace("<br>", "<br/>")
        'swXml.Replace("&nbsp;", "#160;")

        'swXml.Replace("<!--[if gte mso 9]>", "")
        'swXml.Replace("<!--[if gte mso 10]>", "")
        'swXml.Replace("<!--[if !mso]>", "")
        'swXml.Replace("<![endif]-->", "")

        swXml.Replace("<o:", "<o-")
        swXml.Replace("</o:", "</o-")
        'swXml.Replace("<w:", "<w-")
        'swXml.Replace("</w:", "</w-")

        'swXml.Replace("<![if gte mso 9]>", "")
        'swXml.Replace("<![if gte mso 10]>", "")
        'swXml.Replace("<![if !mso]>", "")

        'swXml.Replace("<!--[if gte vml 1]>", "")
        'swXml.Replace("<![if !vml]>", "")
        'swXml.Replace("<![endif]>", "")
        'swXml.Replace("<v:", "<")
        'swXml.Replace("</v:", "</")
        'swXml.Replace(" v:", " ")
        'swXml.Replace(" o:", " ")

        ' Konverter spesialtegn
        swXml.Replace(ChrW(&H95), "<bullet>-</bullet>")
        swXml.Replace(ChrW(&H96), "-")
        swXml.Replace(ChrW(&H94), """")
        swXml.Replace(vbCrLf, " ")

        'swXml.Replace("xmlns=""http://www.w3.org/TR/REC-html40""", "")

        swXml.Insert(0, "<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf)

        ' Fjern <head>-start og slutt tag
        strXmlHead = strXmlHead.Replace("<head>", "")
        strXmlHead = strXmlHead.Replace("</head>", "")

        ' Sett inn header etter <head>-tag
        If strXmlHead <> "" Then
            Dim intPos As Integer
            intPos = swXml.ToString.IndexOf("<head>") + 6
            If intPos = -1 Then
                intPos = swXml.ToString.IndexOf("<head>") + 6
            End If
            If intPos > -1 Then
                swXml.Insert(intPos, strXmlHead)
            End If
        End If

        Return swXml.ToString
    End Function

    Protected Function CleanAttributes(ByVal strDoc As String) As String
        Dim intStart, intSlutt As Integer
        Dim strAttrQuot, strReplace, strReplace2 As String

        Do
            intStart = strDoc.IndexOf("=", intStart)

            If intStart = -1 Then
                Exit Do
            End If

            If strDoc.Substring(intStart + 1, 1) = """" Then
                strAttrQuot = """"
                strReplace = "="
                strReplace2 = """"
            ElseIf strDoc.Substring(intStart + 1, 1) = "'" Then
                strAttrQuot = "'"
                strReplace = "="
                strReplace2 = "'"
            Else
                strAttrQuot = " "
                strReplace = "='"
                strReplace2 = "' "
            End If

            intSlutt = strDoc.IndexOf(strAttrQuot, intStart + 2)

            If intSlutt = -1 Then
                If strAttrQuot = " " Then
                    intSlutt = strDoc.IndexOf("/", intStart + 1)
                    If intSlutt > -1 Then
                        strReplace2 = "'/"
                        strDoc = strDoc.Substring(0, intStart) & strReplace & strDoc.Substring(intStart + 1, intSlutt - intStart - 1) & strReplace2 & strDoc.Substring(intSlutt + 1)
                    Else
                        intSlutt = strDoc.IndexOf(">", intStart + 1)
                        If intSlutt > -1 Then
                            strReplace2 = "'>"
                            strDoc = strDoc.Substring(0, intStart) & strReplace & strDoc.Substring(intStart + 1, intSlutt - intStart - 1) & strReplace2 & strDoc.Substring(intSlutt + 1)
                        End If
                    End If
                End If

                Exit Do
            End If

            strDoc = strDoc.Substring(0, intStart) & strReplace & strDoc.Substring(intStart + 1, intSlutt - intStart - 1) & strReplace2 & strDoc.Substring(intSlutt + 1)
            intStart = intSlutt
        Loop

        Return strDoc

    End Function

    Public Sub LagNitfFile(ByRef strBodyXml As String, ByVal xslFile As String, ByVal strNitfFile As String)
        'Har problemer med formattering .NET XslTransform, b�r bruke MSXML4.dll i stedet!
        '.NET XslTransform gir ny linje f�r end-tag og klarer ikke tomme tagger som: <p/> (skriver alltid da <p></p>)

        Dim xwNitf As XmlTextWriter = New XmlTextWriter(strNitfFile, System.Text.Encoding.GetEncoding("iso-8859-1"))
        Dim xmlBody As XmlDocument = New XmlDocument()
        xmlBody.LoadXml(strBodyXml)
        'Dim xslNitf As XslTransform = New XslTransform()
        'xslNitf.Load(xslFile)

        xwNitf.Formatting = Formatting.Indented
        xwNitf.IndentChar = vbTab
        xwNitf.Indentation = 1

        xwNitf.WriteStartDocument(True)

        xsltNitf.Transform(xmlBody, Nothing, xwNitf)

        xwNitf.Flush()
        xwNitf.Close()
    End Sub

    Public Sub LagNitfFile(ByRef strBodyXml As String, ByVal strNitfFile As String)
        '.NET XslTransform Problem workaround:        
        'Har problemer med formattering .NET XslTransform, og bruker her MSXML4.dll i stedet!

        Dim xwNitf As XmlTextWriter = New XmlTextWriter(strNitfFile, System.Text.Encoding.GetEncoding("iso-8859-1"))
        Dim xmlBody As XmlDocument = New XmlDocument()
        xmlBody.LoadXml(strBodyXml)

        xwNitf.Formatting = Formatting.Indented
        xwNitf.IndentChar = vbTab
        xwNitf.Indentation = 1

        'Try
        Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument()

        'xml4Doc.preserveWhiteSpace = False

        xml4Doc.loadXML(strBodyXml)

        xslt4Proc.input = xml4Doc

        xslt4Proc.transform()
        Dim strXml4 As String = xslt4Proc.output
        'xwNitf.WriteRaw(Replace(strXml4, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1))
        xwNitf.WriteRaw(Replace(strXml4, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1))

        xwNitf.Flush()
        xwNitf.Close()

        'Catch err As Exception
        'MsgBox(Err())

        'End Try

    End Sub

    Protected Function MakeXmlHeaderFromXml(ByRef strXml As String) As String

        Dim ioXmlHead As IO.MemoryStream = New IO.MemoryStream()
        Dim srXmlHead As IO.StreamReader = New StreamReader(ioXmlHead, System.Text.Encoding.GetEncoding("iso-8859-1"))
        Dim xwHead As XmlTextWriter = New XmlTextWriter(ioXmlHead, System.Text.Encoding.GetEncoding("iso-8859-1"))

        Dim xmlHead As XmlDocument = New XmlDocument()

        'xwHead.WriteStartDocument(False)
        'xwHead.Formatting = Formatting.Indented
        xwHead.Indentation = 0

        xmlHead.LoadXml(strXml)
        xsltHead.Transform(xmlHead, Nothing, xwHead)

        ioXmlHead.Seek(0, SeekOrigin.Begin)
        Return srXmlHead.ReadToEnd

    End Function

    Public Function LoadNotabeneFieldsHeader(ByVal strMapFile As String, ByRef ObjMessage As Object, ByRef strError As String)
        Dim strFieldName As String
        Dim strXmlFieldName As String
        Dim strXmlFieldContent As String
        Dim strXmlHead As String

        Dim xmlMap As XmlDocument = New XmlDocument()
        xmlMap.Load(strMapFile)

        strError = ""
        strXmlHead = "<head>" & vbCrLf

        Dim dtNow As Date = Now

        AddXmlHeadNode(strXmlHead, "timestamp", Format(dtNow, "yyyy.MM.dd HH:mm:ss"))
        AddXmlHeadNode(strXmlHead, "NitfDate", Format(dtNow, "yyyyMMddTHHmmss"))
        AddXmlHeadNode(strXmlHead, "ntb-date", Format(dtNow, "dd.MM.yyyy HH:mm:ss"))

        Dim mapNode As XmlNode
        For Each mapNode In xmlMap.SelectNodes("/notabene/field")
            Try
                strFieldName = mapNode.Attributes("notabene").InnerText
                strXmlFieldName = mapNode.Attributes("xml").InnerText

                ' Terje! Her kan du legge til en funksjon GetNotabeneFieldVal som lager henter feltverdier fra Notabene
                'strXmlFieldContent = GetNotabeneFieldVal(strFieldName)
                strXmlFieldContent = ObjMessage.Fields(strFieldName).Value

                ' Kode som legger til xml 
                AddXmlHeadNode(strXmlHead, strXmlFieldName, strXmlFieldContent)
            Catch e As Exception
                strError = e.Message & vbCrLf & e.StackTrace
            End Try
        Next

        strXmlHead &= "</head>" & vbCrLf
        Return strXmlHead
    End Function

    Protected Sub AddXmlHeadNode(ByRef strXmlHead As String, ByVal strName As String, ByVal strContent As String)
        strXmlHead &= "<meta name=""" & strName & """ content=""" & strContent & """/>" & vbCrLf
    End Sub

    Public Function getXmlHeaderAndRtf(ByVal strXmlInputFile, ByRef strRtfFile) As String
        Dim strXml As String
        Dim strRtf As String
        Dim strXmlHeader As String

        strXml = LogFile.ReadFile(strXmlInputFile)

        strRtf = GetStringPortion(strXml, "<rtftext><![CDATA[", "]]></rtftext>")
        LogFile.WriteFile(strRtfFile, strRtf, False)

        strXmlHeader = "<message>" & GetStringPortion(strXml, "<message>", "<rtftext>") & "</message>"

        Return MakeXmlHeaderFromXml(strXmlHeader)

    End Function

End Class
