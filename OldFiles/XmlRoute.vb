Imports System.Xml
Imports System.IO

Public Class XmlRoute

    Protected xmlCustomers As XmlDocument = New XmlDocument()
    Protected nodesCustomers As XmlNodeList

    Public Sub New(ByVal strFilename As String)
        Dim strOutPath As String
        Dim node As XmlNode

        xmlCustomers.Load(strFilename)
        nodesCustomers = xmlCustomers.SelectNodes("/kundeliste/kunde")

        For Each node In nodesCustomers
            'Make paths
            strOutPath = node.SelectSingleNode("filutpath").InnerText
            Directory.CreateDirectory(strOutPath)
        Next

    End Sub

    Public Function RouteFile(ByVal strFilename As String, Optional ByVal strOutRootPath As String = "") As String
        Dim xmlNitf As XmlDocument = New XmlDocument()
        Dim strOutPath As String
        Dim strOutFile As String

        Dim node As XmlNode

        xmlNitf.Load(strFilename)

        For Each node In nodesCustomers
            strOutPath = GetFileName(xmlNitf, node)
            If strOutPath <> "" Then
                strOutFile = strOutPath & Path.DirectorySeparatorChar & Path.GetFileName(strFilename)
                File.Copy(strFilename, strOutFile, True)
            End If
        Next
    End Function

    Protected Function GetFileName(ByRef xmlNitf As XmlDocument, ByRef node As XmlNode) As String
        Dim strXpath As String
        Dim nodeList As XmlNodeList
        Dim strOutPath As String
        Dim strCompare As String
        Dim intCompareHit As Integer
        Dim intHit As Integer
        Dim bCopy As Boolean

        strXpath = node.SelectSingleNode("xpath").InnerText
        strOutPath = node.SelectSingleNode("filutpath").InnerText

        strCompare = node.SelectSingleNode("compare").InnerText
        intCompareHit = node.SelectSingleNode("hit").InnerText

        nodeList = xmlNitf.SelectNodes(strXpath)
        intHit = nodeList.Count

        Select Case strCompare
            Case "eq"
                If intHit = intCompareHit Then
                    bCopy = True
                End If
            Case "lt"
                If intHit < intCompareHit Then
                    bCopy = True
                End If
            Case "gt"
                If intHit > intCompareHit Then
                    bCopy = True
                End If
            Case "ne"
                If intHit <> intCompareHit Then
                    bCopy = True
                End If
        End Select

        If bCopy Then
            Return strOutPath
        Else
            Return ""
        End If

    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
