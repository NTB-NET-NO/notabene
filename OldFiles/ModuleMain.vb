Imports System.Text
Imports System.Xml.Xsl
Imports System.Xml
Imports System.IO
Imports System
Imports System.Configuration.ConfigurationSettings
Imports NyRtf2Xml.WordRtf2Html

Module ModuleMain

    '-- Globale variabler --
    Public strXmlTempDir As String = AppSettings("xmlTempDir")
    Public strXmlInputDir As String = AppSettings("xmlInputDir")
    Public strXmlOutputDir As String = AppSettings("xmlOutputDir")

    Public xslFile As String = AppSettings("xslFile")
    Public strXmlMapFile As String = AppSettings("xmlMapFile")
    Public strConvFile As String = AppSettings("ConvFile")

    ' Global switch for utskift av ukonverterte RTF-koder
    Public bKeepRtfCodes As Boolean = LCase(AppSettings("KeepRtfCodes")) = "true" _
                                    Or LCase(AppSettings("KeepRtfCodes")) = "yes" _
                                    Or LCase(AppSettings("KeepRtfCodes")) = "1"

    Private isParEnd As Boolean

    Private htStyles As Hashtable = New Hashtable()
    Private htChars As Hashtable = New Hashtable()

    Public Sub Main()
        Init()
        Dim strRtf As String
        Dim strRtfFile As String
        Dim strNitfFile As String
        Dim strXHtml As String
        Dim strBodyXml As String
        Dim strOutFile As String
        Dim strXmlHead As String
        Dim strError As String

        For Each strRtfFile In Directory.GetFiles(strXmlInputDir, "*.rtf")

            strNitfFile = strXmlOutputDir & "\" & Path.GetFileNameWithoutExtension(strRtfFile) & ".xml"

            strRtf = LogFile.ReadFile(strRtfFile)

            ' Converterer RTF til XML-Body
            strBodyXml = Rtf2Xml(strRtf, strNitfFile)

            ' Lag XML-header fra Notabene fields
            ' Terje! Her kan du legge til kode som lager henter feltverdier fra Notabene

            strXmlHead = WordRtf2Html.LoadNotabeneFieldsHeader(strXmlMapFile, Nothing, strError)
            If strError <> "" Then
                'MsgBox(strError)
            End If

            strXHtml = "<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf
            strXHtml &= "<html>" & strXmlHead & strBodyXml & "</html>"

            ' Skriv ut XML (XHTML), for debug:
            strOutFile = strXmlTempDir & "\" & Path.GetFileNameWithoutExtension(strRtfFile) & "_body.xml"
            LogFile.WriteFile(strOutFile, strXHtml, False)

            _LagNitfFile(strXHtml, xslFile, strNitfFile)

        Next

    End Sub

    Public Sub Init()
        _LoadRtfChars(strConvFile)

        If Not Directory.Exists(strXmlTempDir) Then
            Directory.CreateDirectory(strXmlTempDir)
        End If

        If Not Directory.Exists(strXmlOutputDir) Then
            Directory.CreateDirectory(strXmlOutputDir)
        End If

    End Sub

    Private Sub _LoadRtfChars(ByVal strXmlMapFile As String)
        Dim strRtfVal As String
        Dim strAnsiVal As String
        Dim xmlMap As XmlDocument = New XmlDocument()

        htChars.Clear()
        xmlMap.Load(strXmlMapFile)

        Dim mapNode As XmlNode
        For Each mapNode In xmlMap.SelectNodes("/rtfchar/field")
            Try
                strRtfVal = mapNode.Attributes("rtf").InnerText
                strAnsiVal = mapNode.Attributes("ansi").InnerText
                'If strAnsiVal = "" Then
                'htChars.Add(strRtfVal, Nothing)
                'Else
                htChars.Add(strRtfVal, strAnsiVal)
                'End If

            Catch e As Exception
                'MsgBox(e.Message & vbCrLf & e.StackTrace)
            End Try
        Next
    End Sub

    'Private Function _LoadNotabeneFieldsHeader(ByVal strMapFile As String, ByRef ObjMessage As Object, ByRef strError As String)
    '    Dim strFieldName As String
    '    Dim strXmlFieldName As String
    '    Dim strXmlFieldContent As String
    '    Dim strXmlHead As String

    '    Dim xmlMap As XmlDocument = New XmlDocument()
    '    xmlMap.Load(strMapFile)

    '    strError = ""
    '    strXmlHead = "<head>" & vbCrLf

    '    AddXmlHeadNode(strXmlHead, "timestamp", Format(Now, "yyyy.MM.dd HH:mm:ss"))

    '    Dim mapNode As XmlNode
    '    For Each mapNode In xmlMap.SelectNodes("/notabene/field")
    '        Try
    '            strFieldName = mapNode.Attributes("notabene").InnerText
    '            strXmlFieldName = mapNode.Attributes("xml").InnerText

    '            ' Terje! Her kan du legge til en funksjon GetNotabeneFieldVal som lager henter feltverdier fra Notabene
    '            'strXmlFieldContent = GetNotabeneFieldVal(strFieldName)
    '            strXmlFieldContent = ObjMessage.Fields(strFieldName).Value

    '            ' Kode som legger til xml 
    '            AddXmlHeadNode(strXmlHead, strXmlFieldName, strXmlFieldContent)
    '        Catch e As Exception
    '            strError = e.Message & vbCrLf & e.StackTrace
    '        End Try
    '    Next

    '    strXmlHead &= "</head>" & vbCrLf
    '    Return strXmlHead
    'End Function

    'Private Sub AddXmlHeadNode(ByRef strXmlHead As String, ByVal strName As String, ByVal strContent As String)
    '    strXmlHead &= "<meta name=""" & strName & """ content=""" & strContent & """/>" & vbCrLf
    'End Sub

    Public Function Rtf2Xml(ByRef strRtf As String, ByVal strFileName As String)
        Dim strFirstXml As String
        Dim strOutFile As String
        Dim xmlRtf As XmlDocument = New XmlDocument()
        Dim strFileNoExt As String = Path.GetFileNameWithoutExtension(strFileName)

        ' Lag f�rste versjon av XML
        strFirstXml = _MakeFirstXml(strRtf)

        ' Skriv ut for debug:
        strOutFile = strXmlTempDir & "\" & strFileNoExt & "_first.xml"
        LogFile.WriteFile(strOutFile, strFirstXml, False)

        ' Legg XML-string in i et XMLDocument-objekt
        xmlRtf.LoadXml(strFirstXml)

        ' Ekstraher alle stilene inn i en HashTabell
        _LoadRtfStyles(xmlRtf)

        Dim strBodyXml As String

        ' Lag XML-header:
        strBodyXml &= "<body>" & vbCrLf

        ' Behandle avsnittene i teksten
        Dim pardNodelist As XmlNodeList
        pardNodelist = xmlRtf.SelectNodes("/rtf/group/pard")
        Dim pardNode As XmlNode
        Dim strPard, strP, strStyleName As String

        ' For hvert avsnitt med ny stil
        isParEnd = True
        For Each pardNode In pardNodelist
            strPard = pardNode.InnerXml '*** Viser hele XML-innholdet i noden. Kun for debug.

            ' Finn stilnavnet
            strStyleName = _HentStil(pardNode.FirstChild.InnerText)

            ' Nytt avsnitt:
            'strBodyXml &= "<p class=""" & strStyleName & """>"
            ' Hent tekst og gj�r BR om til nye avsnitt
            _TraverseNodes(pardNode.ChildNodes, strBodyXml, 0, strStyleName)

            ' Slutt p� avsnitt:
            If Not isParEnd Then
                strBodyXml &= "</p>" & vbCrLf
                isParEnd = True
            End If
        Next
        strBodyXml &= "</body>"
        Return strBodyXml
    End Function

    Private Function _MakeFirstXml(ByRef strRtf As String) As String
        Dim sbRtf As StringBuilder = New StringBuilder()
        sbRtf.Append("<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>")
        sbRtf.Append("<rtf>" & strRtf & "</rtf>")

        sbRtf.Replace("{", "<group>")
        sbRtf.Replace("}", "</group>")

        sbRtf.Replace("\pard", "<pard/>")
        sbRtf.Replace("\par ", "<br/>")
        sbRtf.Replace("\par", "<br/>")

        sbRtf.Replace("\trowd", "<trowd/>")
        sbRtf.Replace("\cell", "<cell/>")
        sbRtf.Replace("\row", "<row/>")

        ' Fjerner et un�dig <br/> der det allikevel er slutt p� avsnitt:
        'sbRtf.Replace("<br/></group>", "</group>")

        sbRtf.Replace("<pard/>", "</pard><pard>")

        ' Lagger til siste </pard>:
        sbRtf.Replace("</group></rtf>", "</pard></group></rtf>")

        ' Fjerner f�rste </pard> som ikke kan v�re start-tag, og returnerer ny XML
        Return Replace(sbRtf.ToString, "</pard>", "", 1, 1)

    End Function

    Private Sub _LoadRtfStyles(ByRef xmlRtf As XmlDocument)
        Dim strStyle As String
        Dim strStyleName As String
        Dim strStyleNum As String
        Dim intStart As Integer
        Dim intSlutt As Integer

        htStyles.Clear()

        'Ekstraherer rtf-stylesheet til Hashtabell
        Dim styleNodelist As XmlNodeList

        'Finner alle stil-noder:
        styleNodelist = xmlRtf.SelectNodes("/rtf/group/group[text()='\stylesheet']/group")
        Dim styleNode As XmlNode
        For Each styleNode In styleNodelist
            strStyle = styleNode.InnerText
            intStart = strStyle.IndexOf("\s", 0)
            intSlutt = strStyle.IndexOf("\", intStart + 1)
            If intSlutt = -1 Or intStart = -1 Then
                strStyleNum = "0"
            Else
                strStyleNum = strStyle.Substring(intStart + 1, intSlutt - intStart - 1)
                'End If

                'If strStyleNum <> "*" Then
                intStart = strStyle.IndexOf("\snext", 1)
                intStart = strStyle.IndexOf(" ", intStart)
                strStyleName = strStyle.Substring(intStart + 1, strStyle.Length - intStart - 2)
                strStyleName = strStyleName.Replace("\'f8", "�")
                htStyles.Add(strStyleNum, strStyleName)
            End If
        Next
    End Sub

    Function _HentStil(ByVal strStyle As String) As String
        Dim strStyleNum, strStyleName As String
        Dim intStart, intSlutt As Integer
        ' Finn stilnavnet 
        intStart = strStyle.IndexOf("\s", 1)
        intSlutt = strStyle.IndexOf("\", intStart + 1)
        strStyleNum = strStyle.Substring(intStart + 1, intSlutt - intStart - 1).Trim
        strStyleName = htStyles(strStyleNum)
        If strStyleName = "" Then
            strStyleName = "normal"
        End If
        Return strStyleName
    End Function

    Private Sub _TraverseNodes(ByVal pardNodes As XmlNodeList, ByRef strBodyXml As String, ByVal intLevel As Integer, ByVal strStyleName As String)
        Dim strP As String

        Dim pNode As XmlNode
        For Each pNode In pardNodes
            strP = pNode.InnerXml '*** Viser hele XML-innholdet i noden. Kun for debug.

            Select Case pNode.NodeType
                Case XmlNodeType.Element
                    If pNode.Name = "br" Then
                        If isParEnd Then
                            'strBodyXml &= "<br/>" & vbCrLf
                            strBodyXml &= "<p class=""" & strStyleName & """/>" & vbCrLf
                            isParEnd = True
                        Else
                            strBodyXml &= "</p>" & vbCrLf '& "<p class=""" & strStyleName & """>"
                            isParEnd = True
                        End If
                    End If
                    If intLevel < 1 Then
                        'Rekursivt kall ett niv� ned i Node-treet:
                        _TraverseNodes(pNode.ChildNodes, strBodyXml, intLevel + 1, strStyleName)
                    End If
                Case XmlNodeType.Text
                    If intLevel = 1 Then
                        If isParEnd Then
                            strBodyXml &= "<p class=""" & strStyleName & """>" & _SubstChars(pNode.InnerText)
                            isParEnd = False
                        Else
                            strBodyXml &= _SubstChars(pNode.InnerText)
                        End If
                    End If
                Case Else
                    ' Will not appear
            End Select
        Next

    End Sub

    Sub _LagNitfFile(ByRef strBodyXml As String, ByVal xslFile As String, ByVal strNitfFile As String)
        Dim xwNitf As XmlTextWriter = New XmlTextWriter(strNitfFile, System.Text.Encoding.GetEncoding("iso-8859-1"))
        'Dim xwNitf As TextWriter = New StreamWriter(strNitfFile, False, System.Text.Encoding.GetEncoding("iso-8859-1"))

        'Har problemer med formattering .NET XslTransform, b�r bruke MSXML4.dll i stedet!
        '.NET XslTransform gir ny linje f�r end-tag og klarer ikke timme tagger som: <p/> (skriver alltid <p></p>)

        Dim xmlBody As XmlDocument = New XmlDocument()
        xmlBody.LoadXml(strBodyXml)
        Dim xslNitf As XslTransform = New XslTransform()
        xwNitf.Formatting = Formatting.Indented
        xwNitf.IndentChar = Chr(9)
        xwNitf.Indentation = 0

        xwNitf.WriteStartDocument(True)

        xslNitf.Load(xslFile)
        xslNitf.Transform(xmlBody, Nothing, xwNitf)

        xwNitf.Flush()
        xwNitf.Close()
    End Sub

    ' Kun for testing
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Beep()
    End Sub

    ' Kun for testing
    Private Sub _AddAttrMapFile()
        ' Legger til nye atributter i en eksisterende xml-fil
        Dim xmlMap As XmlDocument = New XmlDocument()
        Dim strFieldName As String

        xmlMap.Load(strXmlMapFile)

        Dim mapAttributes As XmlAttributeCollection
        Dim attr As XmlAttribute

        Dim mapNode As XmlNode
        For Each mapNode In xmlMap.SelectNodes("/notabene/field")
            strFieldName = mapNode.Attributes("notabene").InnerText
            'strXmlFieldName = mapNode.Attributes("xml").InnerText

            attr = xmlMap.CreateNode(XmlNodeType.Attribute, "xml", "")
            attr.Value = strFieldName

            mapAttributes = mapNode.Attributes
            mapAttributes.Append(attr)

            ' For debug:
            strFieldName = mapNode.OuterXml
        Next

        xmlMap.Save(Path.GetFullPath(strXmlMapFile) & Path.GetFileNameWithoutExtension(strXmlMapFile) & "_new.xml")

    End Sub

    Function _SubstChars(ByVal strLine As String) As String
        ' Bytter Internationale tegn fra RTF-coder
        'Dim chrLine(), chrNewLine(Len(strLine)) As Char
        Dim chrA As Char
        Dim i As Integer
        Dim strNewLine As String
        Dim strNewChar As String
        Dim bEscape As Boolean
        Dim bEscapeEnd As Boolean
        Dim strCharEscape As String

        'Return strLine

        Dim strChar As String
        For i = 0 To strLine.Length - 1
            chrA = strLine.Substring(i, 1)
            Select Case chrA
                Case "\"
                    If bEscape Then
                        bEscapeEnd = True
                        i -= 1
                    Else
                        If strLine.Substring(i + 1, 1) = "'" Then
                            '"\'f8"
                            strChar = strLine.Substring(i, 4)
                            strNewChar = htChars(strChar)
                            If strNewChar <> "" Then
                                strNewLine &= strNewChar
                            Else
                                strNewLine &= strChar
                            End If
                            i += 3
                        Else
                            bEscape = True
                            strCharEscape = "\"
                        End If
                    End If
                Case Chr(13)
                    If bEscape Then
                        bEscapeEnd = True
                    End If
                    ' Skip Cr
                Case Chr(10)
                    ' Skip Lf
                Case " "
                    If bEscape Then
                        bEscapeEnd = True
                    Else
                        strNewLine &= strLine.Substring(i, 1)
                    End If
                Case Else
                    If bEscape Then
                        strCharEscape &= strLine.Substring(i, 1)
                    Else
                        strNewLine &= strLine.Substring(i, 1)
                    End If
            End Select
            If bEscapeEnd Then
                strNewChar = htChars(strCharEscape)
                If strNewChar Is Nothing And bKeepRtfCodes Then
                    strNewLine &= strCharEscape
                ElseIf strNewChar = "" Then
                    ' Empty
                Else
                    strNewLine &= strNewChar
                End If
                bEscape = False
                bEscapeEnd = False
                strCharEscape = ""
            End If

        Next

        Return strNewLine

    End Function


End Module
