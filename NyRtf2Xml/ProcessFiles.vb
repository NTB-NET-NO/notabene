Imports System.Configuration.ConfigurationSettings
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Text.RegularExpressions

Public Class ProcessFiles

    Private Const TEXT_LINE As String = "----------------------------------------------------------------------------------"

    Private WithEvents Timer1 As System.Timers.Timer = New System.Timers.Timer
    Private WithEvents FileSystemWatcher1 As FileSystemWatcher = New FileSystemWatcher()

    Private bTimerEnabled As Boolean = LCase(AppSettings("timerEnabled")) = "yes"
    Private bEnableRaisingEvents As Boolean = LCase(AppSettings("enableRaisingEvents")) = "yes"
    Private bTimerEnabledPause As Boolean
    Private bEnableRaisingEventsPause As Boolean
    Private bBusy As Boolean

    Private intSleepSec As Integer = AppSettings("sleepSec")
    Private intPollingInterval As Integer = AppSettings("pollingInterval")
    Private rtfTempDir As String = AppSettings("rtfTempDir")
    Private deleteRtfTempFile As Boolean = LCase(AppSettings("deleteRtfTempFile")) = "yes"

    Private strXmlInputDir As String = AppSettings("xmlInputDir")
    Private strXmlOutputDir As String = AppSettings("xmlOutputDir")

    Private xmlDebugDir As String = AppSettings("xmlDebugDir")
    Private WordMlTempDir As String = AppSettings("WordMlTempDir")
    Private writeDebugXml As Boolean = LCase(AppSettings("writeDebugXml")) = "yes"
    Private deleteWordMlTempFile As Boolean = LCase(AppSettings("deleteWordMlTempFile")) = "yes"

    Private clipDirekte As Boolean = LCase(AppSettings("clipDirekte")) = "yes"
    Private clipNPKDirekte As Boolean = LCase(AppSettings("clipNPKDirekte")) = "yes"
    Private clipTikker As Boolean = LCase(AppSettings("clipTikker")) = "yes"

    Private xsltFile As String = AppSettings("xsltFile")
    Private xsltFileHead As String = AppSettings("xsltFileHead")
    Private xsltDirekteClipper As String = AppSettings("xsltDirekteClipper")
    Private xsltNPKDirekteClipper As String = AppSettings("xsltNPKDirekteClipper")
    Private xsltTikkerClipper As String = AppSettings("xsltTikkerClipper")

    Private strXmlMapFile As String = AppSettings("xmlMapFile")
    Private strConvFile As String = AppSettings("convFile")

    Private logPath As String = AppSettings("logPath")
    Private errLogPath As String = AppSettings("errLogPath")

    Private donePath As String = AppSettings("donePath")
    Private errorFilePath As String = AppSettings("errorFilePath")

    Private SqlConnectionString As String = AppSettings("SqlConnectionString")
    Private bitnameOfflineFile As String = AppSettings("BitnameOfflineFile")

    Private objBitnames As Bitnames
    Private objWordRtf2Html As WordMl2Nitf

    Public Sub New()
        CreateFolders()

        objWordRtf2Html = New WordMl2Nitf(xsltFile, xsltFileHead)
        objWordRtf2Html.deleteWordMlTempFile = deleteWordMlTempFile
        objWordRtf2Html.writeDebugXml = writeDebugXml
        objWordRtf2Html.strXmlMapFile = strXmlMapFile

        objBitnames = New Bitnames(SqlConnectionString, bitnameOfflineFile)

        FileSystemWatcher1.Path = strXmlInputDir
        FileSystemWatcher1.Filter = "*.*"
        Timer1.Interval = intPollingInterval * 1000
    End Sub

    Private Sub CreateFolders()
        Directory.CreateDirectory(strXmlInputDir)
        Directory.CreateDirectory(rtfTempDir)
        Directory.CreateDirectory(WordMlTempDir)
        Directory.CreateDirectory(xmlDebugDir)
        Directory.CreateDirectory(strXmlOutputDir)
        Directory.CreateDirectory(donePath)
        Directory.CreateDirectory(errorFilePath)
        Directory.CreateDirectory(logPath)
        Directory.CreateDirectory(errLogPath)
        Directory.CreateDirectory(Path.GetDirectoryName(bitnameOfflineFile))
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        bBusy = True
        Timer1.Enabled = False
        FileSystemWatcher1.EnableRaisingEvents = False

        Debug.WriteLine(Now & ": Timer1_Elapsed")

        Me.DoAllInputFiles()

        Timer1.Interval = intPollingInterval * 1000
        Timer1.Enabled = bTimerEnabledPause
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEventsPause
        bBusy = False
    End Sub

    Private Sub FileSystemWatcher1_Created(ByVal sender As System.Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher1.Created
        bBusy = True
        FileSystemWatcher1.EnableRaisingEvents = False
        Timer1.Enabled = False

        Debug.WriteLine(Now & ": Created: " & e.Name & ", ChangeType: " & e.ChangeType & ", NotifyFilter: " & FileSystemWatcher1.NotifyFilter)

        'Wait some seconds for last inputfile file to be written by external program, before reading
        Threading.Thread.Sleep(intSleepSec * 1000)

        Me.DoAllInputFiles()

        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEventsPause
        Timer1.Enabled = bTimerEnabledPause
        bBusy = False
    End Sub

    Public Sub Start()
        bTimerEnabledPause = bTimerEnabled
        'Timer1.Enabled = bTimerEnabled

        bEnableRaisingEventsPause = bEnableRaisingEvents
        FileSystemWatcher1.EnableRaisingEvents = bEnableRaisingEvents

        Timer1.Interval = 10
        Timer1.Enabled = True
    End Sub

    Public Function WaitWhileBusy(ByVal SleepRetrySeconds As Integer, Optional ByVal TimeOutSeconds As Integer = 15) As Boolean
        Dim i As Integer
        For i = 0 To TimeOutSeconds / SleepRetrySeconds
            If bBusy Then
                Threading.Thread.Sleep(SleepRetrySeconds * 1000)
            Else
                Return True
            End If
        Next
        Return False
    End Function

    Public Sub Pause()
        bTimerEnabledPause = False
        Timer1.Enabled = False
        bEnableRaisingEventsPause = False
        FileSystemWatcher1.EnableRaisingEvents = False
    End Sub

    Public Sub DoAllInputFiles()
        Dim strXmlInputFile As String
        Dim arrFileList() As String
        Dim intCount, i As Integer
        Dim dtStartTime As Date

        dtStartTime = TimeOfDay

        Try
            arrFileList = Directory.GetFiles(strXmlInputDir, "*.*")
        Catch e As Exception
            LogFile.WriteErr(errLogPath, "Directory.GetFiles feilet: " & strXmlInputDir, e)
            Debug.WriteLine(e.Message & e.StackTrace)
            Exit Sub
        End Try

        Threading.Thread.Sleep(intSleepSec * 1000)
        For Each strXmlInputFile In arrFileList
            Me.DoOneInputFile(strXmlInputFile)
            'intCount += 1
            'Application.DoEvents()
        Next

    End Sub

    Private Sub DoOneInputFile(ByVal strXmlInputFile As String)

        ' Split file into XML-header-string and save RTF-portion as file:
        Dim strDirSepChar As String = Path.DirectorySeparatorChar

        Dim dtDateTime As Date
        Dim strOrigFileName As String = Path.GetFileNameWithoutExtension(strXmlInputFile)
        Dim strRtfFile As String = rtfTempDir & strDirSepChar & strOrigFileName & ".rtf"

        Try
            'Split and get header and write RTF
            Dim strXmlHead As String = objWordRtf2Html.getXmlHeaderAndRtf(strXmlInputFile, strRtfFile)

            ' Add to XML head: NITF Categories and Evloc
            ' and make new filename from date and keyword without extension
            Dim xmlNitfHead As XmlDocument = objBitnames.MakeNotaben2NitfFields(strXmlHead)
            Dim strFileNameBase As String = MakeFileNameFromHead(xmlNitfHead)
            dtDateTime = GetTimeStampFromHead(xmlNitfHead)
            Dim strNitfHead As String = xmlNitfHead.InnerXml

            'Convert file to Word HTML:
            Dim tempFileName As String = WordMlTempDir & strDirSepChar & strFileNameBase & "-wordML.xml"
            Dim tempXMLDebug As String

            If writeDebugXml Then
                tempXMLDebug = xmlDebugDir & strDirSepChar & strFileNameBase & "-debug.xml"
            End If

            Dim strNITFBody As String = objWordRtf2Html.ConvertDoc(strRtfFile, strNitfHead, tempFileName, tempXMLDebug)

            If deleteRtfTempFile Then
                ' Delete Rtf-temp-file
                File.Delete(strRtfFile)
            End If

            ' Make NITF-XML-file
            Dim strNitfFile As String = strXmlOutputDir & strDirSepChar & strFileNameBase & ".xml"

            'objWordRtf2Html.LagNitfFile(strNITF, strNitfFile)

            'Write the XML in the correct Encoding and code the special chars 
            strNITFBody = Replace(strNITFBody, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

            'Do some cleanup of journalists use of "<<- and so on

            'Fix " -> � �
            Dim pattern As String = "([-\s\(>" & Chr(150) & "])[""" & Chr(147) & Chr(148) & Chr(132) & "]+([^<]+?)[""" & Chr(147) & Chr(148) & Chr(132) & "]+([^>])"
            strNITFBody = Regex.Replace(strNITFBody, pattern, "$1" & Chr(171) & "$2" & Chr(187) & "$3")

            'Fix tankestrek - alltid mellomrom bak
            pattern = "([\s>])[-" & Chr(150) & "] ?([A-Za-z������" & Chr(171) & "])"
            strNITFBody = Regex.Replace(strNITFBody, pattern, "$1" & Chr(150) & " $2")

            'Encode special chars
            pattern = "([" & Chr(159) & "-" & Chr(128) & "])"
            Dim rx As Regex = New Regex(pattern, RegexOptions.Compiled Or RegexOptions.IgnoreCase Or RegexOptions.Multiline)

            Dim m As Match
            For Each m In rx.Matches(strNITFBody)
                strNITFBody = strNITFBody.Replace(m.Groups(1).Value, "&#" & AscW(m.Groups(1).Value) & ";")
            Next

            LogFile.WriteFile(strNitfFile, strNITFBody, , "iso-8859-1")
            LogFile.WriteLog(logPath, "Fila er konvertert til: " & strNitfFile)

            'Klippe direktesaker her??
            If clipDirekte Then

                Dim folder, sendDir, sendDP As String
                Dim strXMLDirekte As String
                Dim xmlDirDoc As XmlDocument = New XmlDocument

                Try
                    xmlDirDoc.LoadXml(strNITFBody)
                    folder = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='foldername']/@content").InnerText
                    sendDir = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='NTBSendTilDirekte']/@content").InnerText
                    sendDP = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='NTBSendTilDirektePluss']/@content").InnerText
                Catch
                End Try

                'Jepp, tror det...
                If folder = "Ut-Satellitt" And (sendDir = "True" Or sendDP = "True") Then
                    Dim clipper As ClipDirekteFeed = New ClipDirekteFeed(xsltDirekteClipper)

                    ' Make NITF-XML-file
                    strFileNameBase = MakeFileNameFromHead(xmlDirDoc)
                    strNitfFile = strXmlOutputDir & strDirSepChar & strFileNameBase.Replace("Ut-Sat", "Ut-Dir") & ".xml"

                    'Transform
                    strXMLDirekte = clipper.ClipNITFDocument(strNITFBody, strNitfFile)
                    xmlDirDoc.LoadXml(strXMLDirekte)

                    ''Encode special chars
                    'For Each m In rx.Matches(strXMLDirekte)
                    '    strXMLDirekte = strXMLDirekte.Replace(m.Groups(1).Value, "&#" & AscW(m.Groups(1).Value) & ";")
                    'Next

                    'Write the XML in the correct Encoding
                    strXMLDirekte = Replace(strXMLDirekte, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)
                    LogFile.WriteFile(strNitfFile, strXMLDirekte, , "iso-8859-1")
                    LogFile.WriteLog(logPath, "Fila er konvertert til: " & strNitfFile)
                End If

            End If

            'Klippe NPK direktesaker her??
            If clipNPKDirekte Then
                Dim folder As String
                Dim strXMLDirekte As String
                Dim xmlDirDoc As XmlDocument = New XmlDocument

                Try
                    xmlDirDoc.LoadXml(strNITFBody)
                    folder = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='foldername']/@content").InnerText
                Catch
                End Try

                'Jepp, tror det...
                If folder = "Ut-NPKDirekte" Then
                    Dim clipper As ClipTikkerFeed = New ClipTikkerFeed(xsltNPKDirekteClipper)

                    ' Make NITF-XML-file
                    strFileNameBase = MakeFileNameFromHead(xmlDirDoc)
                    'strNitfFile = strXmlOutputDir & strDirSepChar & strFileNameBase.Replace("Ut-Sat", "Ut-Dir") & ".xml"

                    'Transform
                    strXMLDirekte = clipper.ClipNITFDocument(strNITFBody, strNitfFile)
                    xmlDirDoc.LoadXml(strXMLDirekte)

                    ''Encode special chars
                    For Each m In rx.Matches(strXMLDirekte)
                        strXMLDirekte = strXMLDirekte.Replace(m.Groups(1).Value, "&#" & AscW(m.Groups(1).Value) & ";")
                    Next

                    'Write the XML in the correct Encoding
                    strXMLDirekte = Replace(strXMLDirekte, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)
                    LogFile.WriteFile(strNitfFile, strXMLDirekte, , "iso-8859-1")
                    LogFile.WriteLog(logPath, "Fila er konvertert til: " & strNitfFile)
                End If
            End If

            'Klippe tikker her??
            If clipTikker Then
                Dim folder As String
                Dim strXMLDirekte As String
                Dim xmlDirDoc As XmlDocument = New XmlDocument

                Try
                    xmlDirDoc.LoadXml(strNITFBody)
                    folder = xmlDirDoc.SelectSingleNode("/nitf/head/meta[@name='foldername']/@content").InnerText
                Catch
                End Try

                'Jepp, tror det...
                If folder = "Ut-Tikker" Then
                    Dim clipper As ClipTikkerFeed = New ClipTikkerFeed(xsltTikkerClipper)

                    ' Make NITF-XML-file
                    strFileNameBase = MakeFileNameFromHead(xmlDirDoc)
                    'strNitfFile = strXmlOutputDir & strDirSepChar & strFileNameBase.Replace("Ut-Sat", "Ut-Dir") & ".xml"

                    'Transform
                    strXMLDirekte = clipper.ClipNITFDocument(strNITFBody, strNitfFile)
                    xmlDirDoc.LoadXml(strXMLDirekte)

                    ''Encode special chars
                    For Each m In rx.Matches(strXMLDirekte)
                        strXMLDirekte = strXMLDirekte.Replace(m.Groups(1).Value, "&#" & AscW(m.Groups(1).Value) & ";")
                    Next

                    'Write the XML in the correct Encoding
                    strXMLDirekte = Replace(strXMLDirekte, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)
                    LogFile.WriteFile(strNitfFile, strXMLDirekte, , "iso-8859-1")
                    LogFile.WriteLog(logPath, "Fila er konvertert til: " & strNitfFile)
                End If
            End If

            If deleteWordMlTempFile Then
                ' Delete WordML-temp-file
                File.Delete(tempFileName)
            End If

        Catch e As Exception
            LogFile.WriteErr(errLogPath, "Konvertering feilet: " & strXmlInputFile, e)

            LogFile.WriteLogNoDate(logPath, TEXT_LINE)
            LogFile.WriteLog(logPath, "ERROR: " & strXmlInputFile)
            LogFile.WriteLogNoDate(logPath, TEXT_LINE)

            Debug.WriteLine(e.Message & e.StackTrace)
            Try
                Dim errFileName As String = errorFilePath & "\" & Path.GetFileName(strXmlInputFile)
                File.Copy(strXmlInputFile, errFileName, True)
                File.Delete(strXmlInputFile)
            Catch e2 As Exception
                LogFile.WriteErr(errLogPath, "Konvertering feilet: " & strXmlInputFile, e2)
                Debug.WriteLine(e2.Message & e2.StackTrace)
            End Try
            Exit Sub
        End Try

        'Rydd opp og slett fila til slutt:
        Try
            MoveFile(strXmlInputFile, dtDateTime)
            LogFile.WriteLog(logPath, "Fila er slettet: " & strXmlInputFile)
        Catch e As Exception
            LogFile.WriteErr(errLogPath, "Sletting feilet: " & strXmlInputFile, e)
            Debug.WriteLine(e.Message & e.StackTrace)
        End Try

        Debug.WriteLine("Fila er konvertert og slettet: " & strXmlInputFile)

    End Sub

    Private Sub MoveFile(ByVal fileName As String, ByVal dtDateTime As Date)
        Dim outFile As String
        outFile = donePath & "\" & Path.GetFileName(fileName)

        outFile = FuncLib.MakeSubDirDate(outFile, dtDateTime)

        File.Copy(fileName, outFile, True)
        File.Delete(fileName)
    End Sub

    Private Function MakeFileNameFromHead(ByRef xmlHead As XmlDocument)
        Dim sbFileName As New System.Text.StringBuilder
        On Error Resume Next
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='timestamp']/@content").InnerText.Replace(" ", "_"))
        sbFileName.Append("_")
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='foldername']/@content").InnerText.Substring(0, 6))
        sbFileName.Append("_")

        'Standard header
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='NTBStoffgruppe']/@content").InnerText.Substring(0, 3).ToUpper)
        'Direkte clipping
        sbFileName.Append(xmlHead.SelectSingleNode("//head/tobject/@tobject.type").InnerText.Substring(0, 3).ToUpper)
        sbFileName.Append("_")

        'Standard header
        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='NTBUndergruppe']/@content").InnerText.Substring(0, 3).ToUpper)
        'Direkte clipping
        sbFileName.Append(xmlHead.SelectSingleNode("//head/tobject/tobject.property/@tobject.property.type").InnerText.Substring(0, 3).ToUpper)
        sbFileName.Append("_")

        sbFileName.Append(xmlHead.SelectSingleNode("//head/meta[@name='subject']/@content").InnerText.ToLower)

        sbFileName.Replace(" ", "_")
        sbFileName.Replace(":", "-")
        sbFileName.Replace(".", "-")
        sbFileName.Replace("/", "-")
        sbFileName.Replace("\", "-")
        sbFileName.Replace("&", "-")

        Dim strTemp As String = sbFileName.ToString

        strTemp = System.Text.RegularExpressions.Regex.Replace(strTemp, "[^0-9A-Za-z\ _\-�����]", "")

        sbFileName = New System.Text.StringBuilder(strTemp)

        sbFileName.Replace("�", "a")
        sbFileName.Replace("�", "o")
        sbFileName.Replace("�", "a")
        sbFileName.Replace("�", "a")
        sbFileName.Replace("�", "e")
        sbFileName.Replace("hast-", "HAST-")

        xmlHead.SelectSingleNode("//head/meta[@name='filename']/@content").InnerText = sbFileName.ToString & ".xml"
        Return sbFileName.ToString
    End Function

    Private Function GetTimeStampFromHead(ByRef xmlHead As XmlDocument) As Date
        Dim dtDate As Date
        Try
            Return Convert.ToDateTime(xmlHead.SelectSingleNode("head/meta[@name='timestamp']/@content").InnerText)
        Catch
            Return Now
        End Try
    End Function

    Public Sub Close()
        ' Rydder opp og sletter objekter:
        objWordRtf2Html.Close()
        objWordRtf2Html = Nothing
        objBitnames = Nothing
    End Sub

End Class
