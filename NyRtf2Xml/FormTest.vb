Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports System.Xml.Xsl

Public Class FormTest
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(48, 40)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(128, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Convert to NITF"
        '
        'FormTest
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(248, 262)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button1})
        Me.Name = "FormTest"
        Me.Text = "FormTest"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Dim Debug
    '-- Lokale variabler --
    Protected rtfTempDir As String = AppSettings("rtfTempDir")
    Protected deleteRtfTempFile As Boolean = LCase(AppSettings("deleteRtfTempFile")) = "yes"

    Protected htmlTempDir As String = AppSettings("htmlTempDir")
    Protected deleteHtmlTempFile As Boolean = LCase(AppSettings("deleteHtmlTempFile")) = "yes"

    Protected xmlDebugDir As String = AppSettings("xmlDebugDir")
    Protected writeDebugXml As Boolean = LCase(AppSettings("writeDebugXml")) = "yes"

    Protected strXmlInputDir As String = AppSettings("xmlInputDir")
    Protected strXmlOutputDir As String = AppSettings("xmlOutputDir")

    Protected xsltFile As String = AppSettings("xsltFile")
    Protected xsltFileHead As String = AppSettings("xsltFileHead")
    Protected strXmlMapFile As String = AppSettings("xmlMapFile")
    Protected strConvFile As String = AppSettings("convFile")

    Protected SqlConnectionString As String = AppSettings("SqlConnectionString")
    Protected BitnameOfflineFile As String = AppSettings("BitnameOfflineFile")

    Protected objBitnames2Xml As Bitnames

    Protected objWordRtf2Html As WordRtf2Html

    Private Sub FormTest_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Directory.CreateDirectory(rtfTempDir)
        Directory.CreateDirectory(htmlTempDir)
        Directory.CreateDirectory(xmlDebugDir)
        Directory.CreateDirectory(strXmlOutputDir)
        'Init()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        TestWordRtf2Xml()
    End Sub

    Public Sub TestWordRtf2Xml()
        'Dim strRtf As String
        Dim strXmlInputFile As String
        Dim arrFileList() As String
        Dim intCount, i As Integer
        Dim dtStartTime As Date

        objWordRtf2Html = New WordRtf2Html()
        objWordRtf2Html.LoadXsltFiles(xsltFile, xsltFileHead)
        objBitnames2Xml = New Bitnames(SqlConnectionString, BitnameOfflineFile)

        Me.Text = ""
        dtStartTime = TimeOfDay

        arrFileList = Directory.GetFiles(strXmlInputDir, "*.xml")
        For Each strXmlInputFile In arrFileList
            ProcessOneFile(strXmlInputFile)
            intCount += 1
            Me.Text = intCount
            Application.DoEvents()
        Next

        Me.Text = intCount & " - " & DateDiff(DateInterval.Second, dtStartTime, TimeOfDay)

        objWordRtf2Html.Quit()
        objWordRtf2Html = Nothing

        objBitnames2Xml = Nothing
    End Sub

    Sub ProcessOneFile(ByVal strXmlInputFile As String)
        Dim strRtfFile As String
        Dim strNitfFile As String
        Dim strXHtml As String
        Dim strXHtmlTemp As String
        Dim strBodyXml As String
        Dim strOutFile As String
        Dim strXmlHead As String
        Dim strHtmlFile As String
        Dim strFileNameBase As String
        Dim strOrigFileName As String
        Dim strDirSepChar As String

        ' Split file into XML-header-string and save RTF-portion as file:
        strOrigFileName = Path.GetFileNameWithoutExtension(strXmlInputFile)
        strDirSepChar = Path.DirectorySeparatorChar

        strRtfFile = rtfTempDir & strDirSepChar & strOrigFileName & ".rtf"
        strXmlHead = objWordRtf2Html.getXmlHeaderAndRtf(strXmlInputFile, strRtfFile)

        ' Make new filename from date and keyword without extension
        ' strFileNameBase = MakeFileNameFromHead(strXmlHead) '& "_" & strOrigFileName

        ' Add to XML head: NITF Categories and Evloc
        ' and make new filename from date and keyword without extension
        strXmlHead = objBitnames2Xml.MakeNotaben2NitfFields(strXmlHead, strFileNameBase)

        ' Convert file to Word HTML:
        strHtmlFile = htmlTempDir & strDirSepChar & strFileNameBase & ".htm"
        strXHtml = objWordRtf2Html.ConvertDoc(strRtfFile, strXmlHead, strHtmlFile, deleteHtmlTempFile)

        ' Write to file for debug and input for development of XSLT-stylesheet only:
        If writeDebugXml Then
            strXHtmlTemp = strXHtml.Replace(">", ">" & vbCrLf)
            strXHtmlTemp = strXHtmlTemp.Replace(vbCrLf & "</", "</")
            LogFile.WriteFile(xmlDebugDir & strDirSepChar & strFileNameBase & "_Debug.xml", strXHtmlTemp, False)
        Else
            'If not Debug mode: Delete Rtf-temp-file
            File.Delete(strRtfFile)
        End If

        ' Make NITF-XML-file
        strNitfFile = strXmlOutputDir & strDirSepChar & strFileNameBase & ".xml"
        'objWordRtf2Html.LagNitfFile(strXHtml, xslFile, strNitfFile)
        objWordRtf2Html.LagNitfFile(strXHtml, strNitfFile)

    End Sub

    Private Sub FormTest_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        ' Rydder opp og sletter objekter:
        On Error Resume Next
        objWordRtf2Html.Quit()
        objWordRtf2Html = Nothing
    End Sub

End Class
