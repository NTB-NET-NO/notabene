Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports System.Xml.Xsl
Imports System.ServiceProcess

#If Debug Then
Public Class Rtf2XmlService : Inherits System.Windows.Forms.Form

#Else
Public Class Rtf2XmlService : Inherits ServiceBase
#End If

    'Dim Debug
    '-- Lokale variabler --

    Private Const TEXT_STARTED As String = "NTB_Rtf2XmlService Service Started"
    Private Const TEXT_STOPPED As String = "NTB_Rtf2XmlService Service Stopped"
    Private Const TEXT_INIT As String = "NTB_Rtf2XmlService Init finished"
    Private Const TEXT_PAUSE As String = "NTB_Rtf2XmlService Service Paused"
    Private Const TEXT_LINE As String = "----------------------------------------------------------------------------------"

    Private errLogPath As String = AppSettings("errLogPath")
    Private logPath As String = AppSettings("logPath")

    Public objProcessFiles As ProcessFiles

#If Debug Then

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
        Me.Text = "Form1"
    End Sub

#End Region

    Private Sub Rtf2XmlService_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'MyBase.New() 'Rtf2XmlService())

        objProcessFiles = New ProcessFiles()

        'objProcessFiles.Start()
        objProcessFiles.DoAllInputFiles()

        objProcessFiles.Close()

    End Sub
#Else
    ' For Release: run as service
    Public Shared Sub Main()
        ServiceBase.Run(New Rtf2XmlService())
    End Sub

    Public Sub New()
        MyBase.New()
        CanStop = True
        CanShutdown = True
        CanPauseAndContinue = False
        ServiceName = "NTB_XmlXsltRoute"
    End Sub

    Protected Overrides Sub OnShutdown()
        OnStop()
    End Sub

    Protected Overrides Sub OnStop()
        objProcessFiles.WaitWhileBusy(1)
        objProcessFiles.Close()
        objProcessFiles = Nothing
        EventLog.WriteEntry(TEXT_STOPPED)
        LogFile.WriteLog(logPath, TEXT_STOPPED)
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        Try
            objProcessFiles = New ProcessFiles()
            objProcessFiles.Start()
            EventLog.WriteEntry(TEXT_STARTED)
            LogFile.WriteLog(logPath, TEXT_STARTED)
        Catch e As Exception
            LogFile.WriteErr(errLogPath, "Feil i initiering av programmet", e)
            EventLog.WriteEntry("Feil i initiering av programmet")
            End
        End Try
    End Sub
#End If

End Class
