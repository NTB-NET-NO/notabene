<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!--
<xsl:output method="xml" encoding="ISO-8859-1" standalone="yes" indent="yes"/>
-->
<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til NITF
	Felles mal for NTIF-Header
	Opprettet Av Roar Vestre 27.11.2001
	Sist endret Av Roar Vestre 29.11.2002
	Lagt til "meta"-tagger
	
	NB! M� endres for � passe med nytt Word/Notabene format
	Slik at alle NITF tagger blir mappet mot tilsvarende meta tagger 
	i Word/notabene xml-formatet
-->

<xsl:variable name="timestamp" select="/html/head/meta[@name='timestamp']/@content"/>

<xsl:variable name="date-no">
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 15, 2)" />
</xsl:variable>

<xsl:variable name="date-iso">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
	<xsl:value-of select="substring($timestamp, 6, 2)" />
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
	<xsl:value-of select="substring($timestamp, 15, 2)" />
	<xsl:value-of select="substring($timestamp, 18, 2)" />
<!--	<xsl:text>+0100</xsl:text>-->	
</xsl:variable>

<xsl:variable name="date-iso-norm">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 15, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 18, 2)" />
<!--	<xsl:text>+01:00</xsl:text>-->
</xsl:variable>

<!-- Main Template for NITF-header -->
<xsl:template name="messageHead"> 

<head>
<title><xsl:value-of select="head/meta[@name='NTBStikkord']/@content"/></title>

<xsl:copy-of select="head/meta[@name='timestamp'] | head/meta[@name='foldername']"/>
<xsl:copy-of select="head/meta[@name='filename' or @name='madebydistributor' or @name='SMSTextFelt']"/>

<meta name='ntb-dato'><xsl:attribute name="content"><xsl:value-of select="$date-no"/></xsl:attribute></meta>

<tobject>
	<xsl:attribute name="tobject.type"><xsl:value-of select="head/meta[@name='NTBStoffgruppe']/@content"/></xsl:attribute>
	<tobject.property>
		<xsl:attribute name="tobject.property.type"><xsl:value-of select="head/meta[@name='NTBUndergruppe']/@content"/></xsl:attribute>
	</tobject.property>
	<xsl:copy-of select="head/nitf/tobject.subject"/>
</tobject>

<!--
<tobject>
		<tobject.subject>
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.type"><xsl:value-of select="head/meta[@name='NTBHovedKategorier']/@content"/></xsl:attribute>
  		</tobject.subject>
  		<xsl:if test="head/meta[@name='NTBUnderKatKultur']">
			<tobject.subject tobject.subject.code="KUL">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatKultur']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
  		<xsl:if test="head/meta[@name='NTBUnderKatOkonomi']">
			<tobject.subject tobject.subject.code="OKO">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatOkonomi']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
  		<xsl:if test="head/meta[@name='NTBUnderKatKuriosa']">
			<tobject.subject tobject.subject.code="KUR">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatKuriosa']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
  		<xsl:if test="head/meta[@name='NTBUnderKatSport']">
			<tobject.subject tobject.subject.code="SPO">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatSport']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
</tobject>
-->

<docdata>

	<xsl:copy-of select="head/nitf/evloc"/>
<!--
	<evloc>
		<xsl:attribute name="state-prov"><xsl:value-of select="head/meta[@name='NTBOmraader']/@content"/></xsl:attribute>
		<xsl:attribute name="county-dist"><xsl:value-of select="head/meta[@name='NTBFylker']/@content"/></xsl:attribute>
	</evloc>
-->

	<doc-id regsrc="NTB">
		<xsl:attribute name="id-string"><xsl:value-of select="head/meta[@name='NTBID']/@content"/></xsl:attribute>
	</doc-id>

	<urgency>
		<xsl:attribute name="ed-urg"><xsl:value-of select="head/meta[@name='NTBPrioritet']/@content"/></xsl:attribute>
	</urgency>

	<date.issue>
		<xsl:attribute name="norm"><xsl:value-of select="$date-iso-norm"/></xsl:attribute>
	</date.issue>

	<ed-msg>
		<xsl:attribute name="info"><xsl:value-of select="head/meta[@name='NTBBeskjedTilRed']/@content"/></xsl:attribute>
	</ed-msg>

	<du-key>
		<xsl:attribute name="version"><xsl:value-of select="head/meta[@name='NTBMeldingsVersjon']/@content + 1"/></xsl:attribute>
		<xsl:attribute name="key"><xsl:value-of select="head/meta[@name='subject']/@content"/></xsl:attribute>
	</du-key>

	<doc.copyright>
		<xsl:attribute name="year"><xsl:value-of select="substring(head/meta[@name='timestamp']/@content, 1, 4)" /></xsl:attribute>
		<xsl:attribute name="holder">NTB</xsl:attribute>
	</doc.copyright>

	<key-list>
		<keyword>
			<xsl:attribute name="key"><xsl:value-of select="head/meta[@name='NTBStikkord']/@content"/></xsl:attribute>
 		</keyword>
	</key-list>
</docdata>

<pubdata>
	<xsl:attribute name="date.publication"><xsl:value-of select="$date-iso" /></xsl:attribute>
	<xsl:attribute name="item-length"><xsl:value-of select="string-length(body)"/></xsl:attribute>
	<xsl:attribute name="unit-of-measure">character</xsl:attribute>
</pubdata>

<revision-history>
	<xsl:attribute name="name"><xsl:value-of select="head/meta[@name='NTBMeldingsSign']/@content"/></xsl:attribute>
</revision-history>


</head>

</xsl:template> 
</xsl:stylesheet> 