<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" 
>

<xsl:output method="xml" encoding="UTF-8" indent="yes" standalone="yes" omit-xml-declaration="yes"/>

<!-- 
	Styleshet for transformasjon fra WORD XML-format med Notabene headers til NITF
	Sist endret Av Roar Vestre 29.11.2002
-->

<xsl:template match="/message">

<o:CustomDocumentProperties>

<xsl:comment>Adm felt fra Notabene </xsl:comment>

<o:timestamp dt:dt="string"><xsl:value-of select="adm/timestamp"/></o:timestamp>
<o:foldername dt:dt="string"><xsl:value-of select="adm/folder"/></o:foldername>
<o:filename dt:dt="string"><xsl:value-of select="adm/filename"/></o:filename>
<o:madebydistributor dt:dt="string"><xsl:value-of select="adm/madebydistributor"/></o:madebydistributor>

<xsl:comment>Andre header-felt fra Notabene</xsl:comment>

<o:subject dt:dt="string"><xsl:value-of select="subject"/></o:subject>

<o:NTBStoffgruppe dt:dt="string"><xsl:value-of select="group"/></o:NTBStoffgruppe>
<o:NTBUndergruppe dt:dt="string"><xsl:value-of select="subgroup"/></o:NTBUndergruppe>
<o:NTBHovedKategorier dt:dt="string"><xsl:value-of select="category"/></o:NTBHovedKategorier>

<xsl:for-each select="subcategory/field">
	<xsl:element name="{string(name)}" namespace="o">
		<xsl:value-of select="value"/>
	</xsl:element>
</xsl:for-each>

</o:CustomDocumentProperties>

</xsl:template>


</xsl:stylesheet>
