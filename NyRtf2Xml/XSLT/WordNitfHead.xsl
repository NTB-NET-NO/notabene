<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
	xmlns:o="urn:schemas-microsoft-com:office:office"
>
<!--
<xsl:output method="xml" encoding="ISO-8859-1" standalone="yes" indent="yes"/>
-->
<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til NITF
	Felles mal for NTIF-Header
	Opprettet Av Roar Vestre 27.11.2001
	Sist endret Av Roar Vestre 29.11.2002
	Lagt til "meta"-tagger
	
	NB! M� endres for � passe med nytt Word/Notabene format
	Slik at alle NITF tagger blir mappet mot tilsvarende meta tagger 
	i Word/notabene xml-formatet
-->

<xsl:template match="/">
	<!--
	This template is used for debuging only.
	Otherwise this Stylesheet is to be included and called from an other XSLT Stylesheet
	-->
	<xsl:apply-templates select="/w:wordDocument/o:CustomDocumentProperties"/>
</xsl:template>

<xsl:variable name="timestamp" select="/w:wordDocument/o:DocumentProperties/o:LastSaved"/>

<xsl:variable name="date-no">
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 15, 2)" />
</xsl:variable>

<xsl:variable name="date-iso">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
	<xsl:value-of select="substring($timestamp, 6, 2)" />
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
	<xsl:value-of select="substring($timestamp, 15, 2)" />
	<xsl:value-of select="substring($timestamp, 18, 2)" />
<!--	<xsl:text>+0100</xsl:text>-->	
</xsl:variable>

<xsl:variable name="date-iso-norm">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 15, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 18, 2)" />
<!--	<xsl:text>+01:00</xsl:text>-->
</xsl:variable>

<!-- Main Template for NITF-header 
<xsl:template name="messageHead"> 
-->
<xsl:template match="o:CustomDocumentProperties">

<head>
<title><xsl:value-of select="head/meta[@name='NTBStikkord']/@content"/></title>

<xsl:copy-of select="head/meta[@name='timestamp'] | head/meta[@name='foldername']"/>
<xsl:copy-of select="head/meta[@name='filename' or @name='madebydistributor']"/>

<meta name='ntb-dato'><xsl:attribute name="content"><xsl:value-of select="$date-no"/></xsl:attribute></meta>

<tobject>
	<xsl:attribute name="tobject.type"><xsl:value-of select="head/meta[@name='NTBStoffgruppe']/@content"/></xsl:attribute>
	<tobject.property>
		<xsl:attribute name="tobject.property.type"><xsl:value-of select="head/meta[@name='NTBUndergruppe']/@content"/></xsl:attribute>
	</tobject.property>
	<xsl:copy-of select="head/nitf/tobject.subject"/>
</tobject>

<!--
<tobject>
		<tobject.subject>
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.type"><xsl:value-of select="head/meta[@name='NTBHovedKategorier']/@content"/></xsl:attribute>
  		</tobject.subject>
  		<xsl:if test="head/meta[@name='NTBUnderKatKultur']">
			<tobject.subject tobject.subject.code="KUL">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatKultur']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
  		<xsl:if test="head/meta[@name='NTBUnderKatOkonomi']">
			<tobject.subject tobject.subject.code="OKO">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatOkonomi']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
  		<xsl:if test="head/meta[@name='NTBUnderKatKuriosa']">
			<tobject.subject tobject.subject.code="KUR">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatKuriosa']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
  		<xsl:if test="head/meta[@name='NTBUnderKatSport']">
			<tobject.subject tobject.subject.code="SPO">
  			<xsl:attribute name="tobject.subject.refnum">10000000</xsl:attribute>
  			<xsl:attribute name="tobject.subject.matter">
  				<xsl:value-of select="head/meta[@name='NTBUnderKatSport']/@content"/>
  			</xsl:attribute>
	  		</tobject.subject>
  		</xsl:if>
</tobject>
-->

<docdata>

<!-- evloc -->
<xsl:call-template name="_tokenize-delimiters">
	<xsl:with-param name="string">
		<xsl:value-of select="o:NTBOmraader"/>
	</xsl:with-param>
	<xsl:with-param name="element-name">evloc2</xsl:with-param>
	<xsl:with-param name="attribute1">state-prov</xsl:with-param>
</xsl:call-template>

<xsl:if test="o:NTBFylker!=''">
	<xsl:call-template name="_tokenize-delimiters">
		<xsl:with-param name="string">
			<xsl:value-of select="o:NTBFylker"/>
		</xsl:with-param>
		<xsl:with-param name="element-name">evloc</xsl:with-param>
		<xsl:with-param name="attribute1">state-prov</xsl:with-param>
		<xsl:with-param name="attribute1-value">Norge</xsl:with-param>
		<xsl:with-param name="attribute2">county-dist</xsl:with-param>
	</xsl:call-template>
</xsl:if>
<!--
<xsl:for-each select="o:NTBOmraader">
	<evloc>
		<xsl:attribute name="state-prov"><xsl:value-of select="o:NTBOmraader"/></xsl:attribute>
		<xsl:attribute name="county-dist"><xsl:value-of select="o:NTBFylker"/></xsl:attribute>
	</evloc>
</xsl:for-each>	
-->

	<doc-id>
		<xsl:attribute name="regsrc">
			<xsl:choose>
				<xsl:when test="head/meta[@name='NTBTjeneste'='NPKTjenesten' or @name='NTBTjeneste'='NPKDirektetjenesten']">
					<xsl:text>NPK</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>NTB</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:attribute name="id-string"><xsl:value-of select="head/meta[@name='NTBID']/@content"/></xsl:attribute>
	</doc-id>

	<urgency>
		<xsl:attribute name="ed-urg"><xsl:value-of select="head/meta[@name='NTBPrioritet']/@content"/></xsl:attribute>
	</urgency>

	<date.issue>
		<xsl:attribute name="norm"><xsl:value-of select="$date-iso-norm"/></xsl:attribute>
	</date.issue>

	<ed-msg>
		<xsl:attribute name="info"><xsl:value-of select="head/meta[@name='NTBBeskjedTilRed']/@content"/></xsl:attribute>
	</ed-msg>

	<du-key>
		<xsl:attribute name="version"><xsl:value-of select="head/meta[@name='NTBMeldingsVersjon']/@content + 1"/></xsl:attribute>
		<xsl:attribute name="key"><xsl:value-of select="head/meta[@name='subject']/@content"/></xsl:attribute>
	</du-key>

	<doc.copyright>
		<xsl:attribute name="year"><xsl:value-of select="substring(head/meta[@name='timestamp']/@content, 1, 4)" /></xsl:attribute>
		<xsl:attribute name="holder">
			<xsl:choose>
				<xsl:when test="head/meta[@name='NTBTjeneste'='NPKTjenesten' or @name='NTBTjeneste'='NPKDirektetjenesten']">
					<xsl:text>NPK</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>NTB</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</doc.copyright>

	<key-list>
		<keyword>
			<xsl:attribute name="key"><xsl:value-of select="head/meta[@name='NTBStikkord']/@content"/></xsl:attribute>
 		</keyword>
	</key-list>
</docdata>

<pubdata>
	<xsl:attribute name="date.publication"><xsl:value-of select="$date-iso" /></xsl:attribute>
	<xsl:attribute name="item-length"><xsl:value-of select="/w:wordDocument/o:DocumentProperties/o:Characters"/></xsl:attribute>
	<xsl:attribute name="unit-of-measure">character</xsl:attribute>
</pubdata>

<revision-history>
	<xsl:attribute name="name"><xsl:value-of select="/w:wordDocument/o:DocumentProperties/o:LastAuthor"/></xsl:attribute>
</revision-history>


</head>

</xsl:template> 

<xsl:template name="_tokenize-delimiters">
  <xsl:param name="string" />
  <xsl:param name="element-name"/>
  <xsl:param name="attribute1"/>
  <xsl:param name="attribute1-value"/>
  <xsl:param name="attribute2"/>
  <xsl:param name="delimiters">;</xsl:param>
  <xsl:param name="last-delimit">;</xsl:param>
  <xsl:variable name="delimiter" select="substring($delimiters, 1, 1)" />
  <xsl:choose>
    <xsl:when test="not($delimiter)">
 		<xsl:if test="$string!=''">
 			<xsl:choose>
 			<xsl:when test="$attribute2=''">
	 			<xsl:element name="{$element-name}">
		 			<xsl:attribute name="{$attribute1}">
				 		<xsl:value-of select="$string"/>
			 		</xsl:attribute>
		 		</xsl:element>
 			</xsl:when>
 			<xsl:otherwise>
	 			<xsl:element name="{$element-name}">
		 			<xsl:attribute name="{$attribute1}">
				 		<xsl:value-of select="$attribute1-value"/>
			 		</xsl:attribute>
		 			<xsl:attribute name="{$attribute2}">
				 		<xsl:value-of select="$string"/>
			 		</xsl:attribute>
		 		</xsl:element>
		 	</xsl:otherwise>	
 			</xsl:choose>
		</xsl:if>
    </xsl:when>
    <xsl:when test="contains($string, $delimiter)">
      <xsl:if test="not(starts-with($string, $delimiter))">
        <xsl:call-template name="_tokenize-delimiters">
			<xsl:with-param name="string" select="substring-before($string, $delimiter)" />
			<xsl:with-param name="delimiters" select="substring($delimiters, 2)" />
			<xsl:with-param name="element-name" select="$element-name"/>
			<xsl:with-param name="attribute1" select="$attribute1"/>
			<xsl:with-param name="attribute1-value" select="$attribute1-value"/>
			<xsl:with-param name="attribute2" select="$attribute2"/>
        </xsl:call-template>
      </xsl:if>
      <xsl:call-template name="_tokenize-delimiters">
        <xsl:with-param name="string" select="substring-after($string, $delimiter)" />
        <xsl:with-param name="delimiters" select="$delimiters" />
		<xsl:with-param name="element-name" select="$element-name"/>
		<xsl:with-param name="attribute1" select="$attribute1"/>
		<xsl:with-param name="attribute1-value" select="$attribute1-value"/>
		<xsl:with-param name="attribute2" select="$attribute2"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="_tokenize-delimiters">
        <xsl:with-param name="string" select="$string" />
        <xsl:with-param name="delimiters" select="substring($delimiters, 2)" />
		<xsl:with-param name="element-name" select="$element-name"/>
		<xsl:with-param name="attribute1" select="$attribute1"/>
		<xsl:with-param name="attribute1-value" select="$attribute1-value"/>
		<xsl:with-param name="attribute2" select="$attribute2"/>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>