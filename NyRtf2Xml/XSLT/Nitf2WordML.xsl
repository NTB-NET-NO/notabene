<?xml version="1.0" encoding="iso-8859-1"?>
<!--
	NTB Stylesheet for Transformasjon av NTBs NITF til WordML
	Laget av Roar Vestre 2003.10.08	
	Siste endret av Roar Vestre 2003.10.09
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
	xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
>
<xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="no" standalone="yes"/>

<!--
Stilnavn i Word, her kan man bytte til Avisens egne stilnavn i Word
NB! Innholdet i atributtet "name" m� IKKE endres, kun verdien!
Alle verdiene kan sendes inn som parameter til XSLT-transformasjonen
-->
<!--
Stilnavn i NTB (her er parameterene hardkodet som default)
-->
<xsl:param name="infolinje">_Infolinje</xsl:param>
<xsl:param name="ingress">_Ingress</xsl:param>
<xsl:param name="brodtekst">_Br�dtekst</xsl:param>
<xsl:param name="brodtekst_innrykk">_Br�dtekst_Innrykk</xsl:param>
<xsl:param name="mellomtittel">_Mellomtittel</xsl:param>
<xsl:param name="byline">_Byline</xsl:param>
<xsl:param name="tabellkode">_Tabellkode</xsl:param>
<xsl:param name="signatur">_Signatur</xsl:param>
<xsl:param name="bildetekst">_Bildetekst</xsl:param>
<xsl:param name="foto">_Foto</xsl:param>
<xsl:param name="lyd">_Lyd</xsl:param>

<!--
Eksempel p� eventuelle stilnavn i annen tenkt avis.
Endrer kun stilnavnene ikke utseendet 

<xsl:param name="infolinje">Header 1</xsl:param>
<xsl:param name="ingress">Header 2</xsl:param>
<xsl:param name="brodtekst">Normal</xsl:param>
<xsl:param name="brodtekst_innrykk">Normal2</xsl:param>
<xsl:param name="mellomtittel">Header 3</xsl:param>
<xsl:param name="byline">byline</xsl:param>
<xsl:param name="tabellkode">tab</xsl:param>
<xsl:param name="signatur">sign</xsl:param>
<xsl:param name="bildetekst">bildetekst</xsl:param>
<xsl:param name="foto">fotolink</xsl:param>
<xsl:param name="lyd">lydlink</xsl:param>
-->

<xsl:attribute-set name="string">
	<xsl:attribute name="dt:dt">string</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="date">
	<xsl:attribute name="dt:dt">date</xsl:attribute>
</xsl:attribute-set>

<!-- Hoved-template -->
<xsl:template match="/">
<xsl:processing-instruction name="mso-application">
	<xsl:text>progid="Word.Document"</xsl:text>
</xsl:processing-instruction>
<w:wordDocument
		xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
		xmlns:v="urn:schemas-microsoft-com:vml"
		xmlns:w10="urn:schemas-microsoft-com:office:word"
		xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core"
		xmlns:aml="http://schemas.microsoft.com/aml/2001/core"
		xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
		xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"
		w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
<!-- Kaller alle aktuelle templates --><xsl:apply-templates/>
</w:wordDocument>
</xsl:template>

<xsl:template match="nitf/body">
	<w:body>
		<wx:sect>

			<!-- Infolinje -->
			<w:p>
				<w:pPr>
					<w:pStyle w:val="Infolinje"/>
				</w:pPr>
				<w:r>
					<w:t><xsl:value-of select="body.head/hedline/hl1"/></w:t>
				</w:r>
			</w:p>

			<!-- Byline -->
			<xsl:if test="//byline">
			<w:p>
				<w:pPr>
					<w:pStyle w:val="Byline"/>
				</w:pPr>
				<w:r>
					<w:t><xsl:value-of select="//byline"/></w:t>
				</w:r>
			</w:p>
			</xsl:if>

			<!-- Avsnitt og Tabeller -->
			<xsl:apply-templates/>
			<!--<xsl:apply-templates select="//p | //table"/>-->

			<!-- Sideoppsett i Word -->
			<w:sectPr>
				<w:pgSz w:w="11907" w:h="16839"/>
				<w:pgMar w:top="1417" w:right="1417" w:bottom="1417" w:left="1417" w:header="708" w:footer="708" w:gutter="0"/>
				<w:cols w:space="708"/>
				<w:docGrid w:line-pitch="360"/>
			</w:sectPr>
		</wx:sect>
	</w:body>
</xsl:template>

<!-- Template for Avsnitt -->
<xsl:template match="p">
	<w:p>
		<w:pPr>
			<xsl:choose>
					<xsl:when test="@class='lead'"><w:pStyle w:val="Ingress"/></xsl:when>
					<xsl:when test="@class='txt'"><w:pStyle w:val="Brdtekst"/></xsl:when>
					<xsl:when test="@class='txt-ind'"><w:pStyle w:val="BrdtekstInnrykk"/></xsl:when>
					<xsl:otherwise><w:pStyle w:val="Brdtekst"/></xsl:otherwise>
			</xsl:choose>
		</w:pPr>
		<w:r>
			<w:t><xsl:value-of select="."/></w:t>
		</w:r>
	</w:p>

</xsl:template>

<xsl:template match="hl2">
	<w:p>
		<w:pPr>
			<w:pStyle w:val="Mellomtittel"/>
		</w:pPr>
		<w:r>
			<w:t><xsl:value-of select="."/></w:t>
		</w:r>
	</w:p>

</xsl:template>

<xsl:template match="tagline">
	<w:p>
		<w:pPr>
			<w:pStyle w:val="Signatur"/>
		</w:pPr>
		<w:r>
			<w:t><xsl:value-of select="a"/></w:t>
		</w:r>
	</w:p>

</xsl:template>

<!-- Template for Tabeller -->
<xsl:template match="table">
	<w:tbl>
		<w:tblPr>
			<w:tblStyle w:val="TableGrid"/>
			<w:tblW w:w="0" w:type="auto"/>
			<w:tblLook w:val="01E0"/>
		</w:tblPr>
		<!--
		<w:tblGrid>
			<w:gridCol w:w="1857"/>
			<w:gridCol w:w="1858"/>
			<w:gridCol w:w="1858"/>
			<w:gridCol w:w="1858"/>
			<w:gridCol w:w="1858"/>
		</w:tblGrid>
		-->
		<xsl:apply-templates select="tr"/>
	</w:tbl>
</xsl:template>

<xsl:template match="tr">
	<w:tr>
	<xsl:apply-templates select="td"/>
	</w:tr>
</xsl:template>

<xsl:template match="td">
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="0" w:type="auto"/>
			</w:tcPr>
			<w:p>
			<xsl:if test="@align">
				<w:pPr>
					<w:jc w:val="{@align}" />
				</w:pPr>
			</xsl:if>

				<w:r>
					<w:t><xsl:value-of select="."/></w:t>
				</w:r>
			</w:p>
		</w:tc>
</xsl:template>

<xsl:template match="media[@media-type='audio']">
	<w:p>
		<w:pPr>
			<w:pStyle w:val="Lyd"/>
		</w:pPr>
		<w:hlink>
			<!-- w:dest="http://194.19.39.29/kunde/ntb/mp3/.mp3"-->
			<xsl:attribute name="w:dest">
			<xsl:text>http://194.19.39.29/kunde/ntb/mp3/</xsl:text>
			<xsl:value-of select="/nitf/head/meta[@name='ntb-lyd']/@content"/>
			<xsl:text>.mp3</xsl:text>
			</xsl:attribute>
			<w:r>
				<w:rPr>
					<w:rStyle w:val="Hyperlink"/>
					<w:rFonts w:ascii="Verdana" w:h-ansi="Verdana"/>
					<wx:font wx:val="Verdana"/>
					<w:color w:val="003366"/>
					<w:sz w:val="15"/>
					<w:sz-cs w:val="15"/>
				</w:rPr>
				<w:t>NTB Lydfil (MP3)</w:t>
			</w:r>
		</w:hlink>
	</w:p>
</xsl:template>

<xsl:template match="media[@media-type='image']">
	<w:p>
		<w:pPr>
			<w:pStyle w:val="Foto"/>
		</w:pPr>
		<w:r>
			<w:fldChar w:fldCharType="begin"/>
		</w:r>
		<w:r>
			<w:instrText>HYPERLINK "http://80.91.34.200/seno/seno.cgi?/0/UNKNOWN/Q_<xsl:value-of select="media-reference/@source"/>"</w:instrText>
		</w:r>
		<w:r>
			<w:fldChar w:fldCharType="separate"/>
		</w:r>
		<w:r>
			<w:rPr>
				<w:rStyle w:val="Hyperlink"/>
			</w:rPr>
			<w:t>Bilde Scanpix: <xsl:value-of select="media-reference/@source"/></w:t>
		</w:r>
		<w:r>
			<w:fldChar w:fldCharType="end"/>
		</w:r>
	</w:p>
	<w:p>
		<w:pPr>
			<w:pStyle w:val="Bildetekst"/>
		</w:pPr>
		<w:r>
			<w:t><xsl:value-of select="media-caption"/></w:t>
		</w:r>
	</w:p>
</xsl:template>

<xsl:variable name="nitfdate">
	<xsl:value-of select="/nitf/head/pubdata/@date.publication"/>
</xsl:variable>

<xsl:variable name="time-word">
	<!--2003-10-07T11:00:00Z-->
	<xsl:value-of select="substring($nitfdate, 1, 4)"/>
	<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($nitfdate, 5, 2)"/>
	<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($nitfdate, 7, 2)"/>
	<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($nitfdate, 10, 2)"/>
	<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($nitfdate, 12, 2)"/>
	<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($nitfdate, 14, 2)"/>
	<xsl:text>Z</xsl:text>
</xsl:variable>

<!-- Template for Header -->
<xsl:template match="nitf/head">
	<o:DocumentProperties>
		<o:Title><xsl:value-of select="//hl1"/></o:Title>
		<o:Author></o:Author>
		<o:LastAuthor></o:LastAuthor>
		<o:Revision></o:Revision>
		<o:TotalTime></o:TotalTime>
		<o:Created><xsl:value-of select="$time-word"/></o:Created>
		<o:LastSaved><xsl:value-of select="$time-word"/></o:LastSaved>
		<o:Pages></o:Pages>
		<o:Words></o:Words>
		<o:Characters></o:Characters>
		<o:Company>NTB</o:Company>
		<o:Lines></o:Lines>
		<o:Paragraphs></o:Paragraphs>
		<o:CharactersWithSpaces></o:CharactersWithSpaces>
		<o:Version></o:Version>
	</o:DocumentProperties>
	<o:CustomDocumentProperties>
		<o:subject dt:dt="string"><xsl:value-of select="docdata/du-key/@key"/></o:subject>
		<o:NTBStikkord dt:dt="string"><xsl:value-of select="docdata/key-list/keyword/@key"/></o:NTBStikkord>
		<o:NTBPrioritet dt:dt="string"><xsl:value-of select="docdata/urgency/@ed-urg"/></o:NTBPrioritet>
		<o:NTBMeldingsSign dt:dt="string"><xsl:value-of select="revision-history/@name"/></o:NTBMeldingsSign>
		<o:NTBBeskjedTilRed dt:dt="string"><xsl:value-of select="docdata/ed-msg/@info"/></o:NTBBeskjedTilRed>
		<o:NTBMeldingsVersjon dt:dt="string"><xsl:value-of select="docdata/du-key/@version"/></o:NTBMeldingsVersjon>

		<o:NTBStoffgruppe dt:dt="string"><xsl:value-of select="tobject/@tobject.type"/></o:NTBStoffgruppe>
		<o:NTBUndergruppe dt:dt="string"><xsl:value-of select="tobject/tobject.property/@tobject.property.type"/></o:NTBUndergruppe>

		<o:NTBOmraader dt:dt="string"><xsl:for-each select="docdata/evloc[not(@county-dist)]">
				<xsl:value-of select="@state-prov"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBOmraader>

		<o:NTBFylker dt:dt="string"><xsl:for-each select="docdata/evloc[@county-dist]">
				<xsl:value-of select="@county-dist"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBFylker>

		<o:NTBHovedKategorier dt:dt="string"><xsl:for-each select="tobject/tobject.subject[not(@tobject.subject.matter)]">
				<xsl:value-of select="@tobject.subject.type"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBHovedKategorier>
		
		<o:NTBUnderKatKultur dt:dt="string"><xsl:for-each select="tobject/tobject.subject[@tobject.subject.code='KUL' and @tobject.subject.matter]">
				<xsl:value-of select="@tobject.subject.matter"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBUnderKatKultur>
		
		<o:NTBUnderKatKuriosa dt:dt="string"><xsl:for-each select="tobject/tobject.subject[@tobject.subject.code='KUR' and @tobject.subject.matter]">
				<xsl:value-of select="@tobject.subject.matter"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBUnderKatKuriosa>
		
		<o:NTBUnderKatOkonomi dt:dt="string"><xsl:for-each select="tobject/tobject.subject[@tobject.subject.code='OKO' and @tobject.subject.matter]">
				<xsl:value-of select="@tobject.subject.matter"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBUnderKatOkonomi>
		
		<o:NTBUnderKatSport dt:dt="string"><xsl:for-each select="tobject/tobject.subject[@tobject.subject.code='SPO' and @tobject.subject.matter]">
				<xsl:value-of select="@tobject.subject.matter"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBUnderKatSport>

		<o:NTBKatRefnum dt:dt="string"><xsl:for-each select="tobject/tobject.subject">
				<xsl:value-of select="@tobject.subject.refnum"/>
				<xsl:text>;</xsl:text>
			</xsl:for-each></o:NTBKatRefnum>

		<o:NTBEpostAdresse dt:dt="string"><xsl:value-of select="//tagline/a"/></o:NTBEpostAdresse>
		<o:NTBID dt:dt="string"><xsl:value-of select="docdata/doc-id/@id-string"/></o:NTBID>

		<xsl:for-each select="//meta">
			<xsl:element name="o:{@name}" use-attribute-sets="string"><xsl:value-of select="@content"/></xsl:element>
		</xsl:for-each>

	</o:CustomDocumentProperties>
	<w:fonts>
		<w:defaultFonts w:ascii="Times New Roman" w:fareast="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
		<w:font w:name="Tahoma">
			<w:panose-1 w:val="020B0604030504040204"/>
			<w:charset w:val="00"/>
			<w:family w:val="Swiss"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="61007A87" w:usb-1="80000000" w:usb-2="00000008" w:usb-3="00000000" w:csb-0="000101FF" w:csb-1="00000000"/>
		</w:font>
	</w:fonts>
	<w:styles>
		<w:versionOfBuiltInStylenames w:val="4"/>
		<w:latentStyles w:defLockedState="off" w:latentStyleCount="156"/>
		<w:style w:type="paragraph" w:default="on" w:styleId="Normal">
			<w:name w:val="Normal"/>
			<w:rsid w:val="00FF5677"/>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:sz w:val="24"/>
				<w:sz-cs w:val="24"/>
				<w:lang w:val="NO-BOK" w:fareast="NO-BOK" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:default="on" w:styleId="DefaultParagraphFont">
			<w:name w:val="Default Paragraph Font"/>
			<w:semiHidden/>
			<w:rsid w:val="00FF5677"/>
		</w:style>
		<w:style w:type="table" w:default="on" w:styleId="TableNormal">
			<w:name w:val="Normal Table"/>
			<wx:uiName wx:val="Table Normal"/>
			<w:semiHidden/>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
			<w:tblPr>
				<w:tblInd w:w="0" w:type="dxa"/>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa"/>
					<w:left w:w="108" w:type="dxa"/>
					<w:bottom w:w="0" w:type="dxa"/>
					<w:right w:w="108" w:type="dxa"/>
				</w:tblCellMar>
			</w:tblPr>
		</w:style>
		<w:style w:type="list" w:default="on" w:styleId="NoList">
			<w:name w:val="No List"/>
			<w:semiHidden/>
			<w:rsid w:val="00FF5677"/>
		</w:style>
		<w:style w:type="paragraph" w:styleId="TTV">
			<w:name w:val="TTV"/>
			<w:basedOn w:val="Normal"/>
			<w:rsid w:val="00FF5677"/>
			<w:pPr>
				<w:pStyle w:val="TTV"/>
				<w:ind w:right="1554"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New"/>
				<wx:font wx:val="Courier New"/>
				<w:sz w:val="32"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="PictureText">
			<w:name w:val="PictureText"/>
			<w:rsid w:val="00FF5677"/>
			<w:pPr>
				<w:pStyle w:val="PictureText"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Tahoma" w:h-ansi="Tahoma"/>
				<wx:font wx:val="Tahoma"/>
				<w:color w:val="FF0000"/>
				<w:sz w:val="18"/>
				<w:lang w:val="NO-BOK" w:fareast="NO-BOK" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Brdtekst">
			<!--<w:name w:val="_Br�dtekst"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$brodtekst"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:next w:val="BrdtekstInnrykk"/>
			<w:pPr>
				<w:pStyle w:val="Brdtekst"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:sz w:val="22"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="BrdtekstInnrykk">
			<!--<w:name w:val="_Br�dtekst_Innrykk"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$brodtekst_innrykk"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:rsid w:val="00FF5677"/>
			<w:pPr>
				<w:pStyle w:val="BrdtekstInnrykk"/>
				<w:ind w:first-line="397"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:sz w:val="22"/>
				<w:sz-cs w:val="22"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Byline">
			<!--<w:name w:val="_Byline"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$byline"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:next w:val="Ingress"/>
			<w:pPr>
				<w:pStyle w:val="Byline"/>
				<w:spacing w:before="120" w:after="120"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial"/>
				<wx:font wx:val="Arial"/>
				<w:i/>
				<w:color w:val="FF0000"/>
				<w:sz w:val="22"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Infolinje">
			<!--<w:name w:val="_Infolinje"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$infolinje"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:next w:val="Ingress"/>
			<w:rsid w:val="00FF5677"/>
			<w:pPr>
				<w:pStyle w:val="Infolinje"/>
				<w:spacing w:after="120"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial" w:cs="Arial"/>
				<wx:font wx:val="Arial"/>
				<w:b/>
				<w:b-cs/>
				<w:color w:val="000080"/>
				<w:sz w:val="30"/>
				<w:sz-cs w:val="30"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Ingress">
			<!--<w:name w:val="_Ingress"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$ingress"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:next w:val="BrdtekstInnrykk"/>
			<w:rsid w:val="00FF5677"/>
			<w:pPr>
				<w:pStyle w:val="Ingress"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:b/>
				<w:b-cs/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Mellomtittel">
			<!--<w:name w:val="_Mellomtittel"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$mellomtittel"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:next w:val="Brdtekst"/>
			<w:pPr>
				<w:pStyle w:val="Mellomtittel"/>
				<w:spacing w:before="120"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial"/>
				<wx:font wx:val="Arial"/>
				<w:b/>
				<w:color w:val="333399"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Signatur">
			<!--<w:name w:val="_Signatur"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$signatur"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:next w:val="Normal"/>
			<w:pPr>
				<w:pStyle w:val="Signatur"/>
				<w:spacing w:before="120" w:after="120"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Arial" w:h-ansi="Arial"/>
				<wx:font wx:val="Arial"/>
				<w:color w:val="800000"/>
				<w:sz w:val="22"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="Tabellkode">
			<!--<w:name w:val="_Tabellkode"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$tabellkode"/>
				</xsl:attribute>
			</w:name>
			<w:next w:val="Brdtekst"/>
			<w:pPr>
				<w:pStyle w:val="Tabellkode"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New"/>
				<wx:font wx:val="Courier New"/>
				<w:sz w:val="18"/>
				<w:lang w:val="NO-BOK" w:fareast="NO-BOK" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>

		<w:style w:type="paragraph" w:styleId="Bildetekst">
			<!--<w:name w:val="_Bildetekst"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$bildetekst"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:pPr>
				<w:pStyle w:val="Bildetekst"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Arial"/>
			</w:rPr>
		</w:style>

		<w:style w:type="paragraph" w:styleId="Foto">
			<!--<w:name w:val="_Foto"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$foto"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Normal"/>
			<w:rsid w:val="00683062"/>
			<w:pPr>
				<w:pStyle w:val="Foto"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Arial"/>
			</w:rPr>
		</w:style>

		<w:style w:type="paragraph" w:styleId="Lyd">
			<!--<w:name w:val="_Lyd"/>-->
			<w:name>
				<xsl:attribute name="w:val">
					<xsl:value-of select="$lyd"/>
				</xsl:attribute>
			</w:name>
			<w:basedOn w:val="Foto"/>
			<w:rsid w:val="00C43866"/>
			<w:pPr>
				<w:pStyle w:val="Lyd"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Arial"/>
			</w:rPr>
		</w:style>

		<w:style w:type="character" w:styleId="Hyperlink">
			<w:name w:val="Hyperlink"/>
			<w:basedOn w:val="DefaultParagraphFont"/>
			<w:rsid w:val="00683062"/>
			<w:rPr>
				<w:color w:val="0000FF"/>
				<w:u w:val="single"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="FollowedHyperlink">
			<w:name w:val="FollowedHyperlink"/>
			<w:basedOn w:val="DefaultParagraphFont"/>
			<w:rsid w:val="00683062"/>
			<w:rPr>
				<w:color w:val="800080"/>
				<w:u w:val="single"/>
			</w:rPr>
		</w:style>

		<w:style w:type="table" w:styleId="TableGrid">
			<w:name w:val="Table Grid"/>
			<w:basedOn w:val="TableNormal"/>
			<w:rsid w:val="00FF5677"/>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
			<w:tblPr>
				<w:tblInd w:w="0" w:type="dxa"/>
				<w:tblBorders>
					<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
					<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
					<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
					<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
					<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
					<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
				</w:tblBorders>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa"/>
					<w:left w:w="108" w:type="dxa"/>
					<w:bottom w:w="0" w:type="dxa"/>
					<w:right w:w="108" w:type="dxa"/>
				</w:tblCellMar>
			</w:tblPr>
		</w:style>
	</w:styles>
	<w:docPr>
		<w:view w:val="print"/>
		<w:zoom w:percent="100"/>
		<w:doNotEmbedSystemFonts/>
		<w:proofState w:grammar="clean"/>
		<w:attachedTemplate w:val="C:\Documents and Settings\rov.PORTAL\Application Data\Microsoft\Templates\NTBWord.dot"/>
		<w:linkStyles/>
		<w:defaultTabStop w:val="708"/>
		<w:hyphenationZone w:val="425"/>
		<w:characterSpacingControl w:val="DontCompress"/>
		<w:relyOnVML/>
		<w:validateAgainstSchema/>
		<w:saveInvalidXML w:val="off"/>
		<w:ignoreMixedContent w:val="off"/>
		<w:alwaysShowPlaceholderText w:val="off"/>
		<w:compat>
			<w:dontAllowFieldEndSelect/>
			<w:useWord2002TableStyleRules/>
		</w:compat>
	</w:docPr>
</xsl:template>

</xsl:stylesheet>