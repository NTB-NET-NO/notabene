<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="iso-8859-1" indent="yes"	standalone="yes"/>
<!-- doctype-system = "D:\Download\XML\NITF\nitf-3-1.dtd" /> -->

<!--
	Styleshet for transformasjon fra WORD XML-format med Notabene headers til NITF
	Sist endret Av Roar Vestre 29.11.2002
-->

<xsl:include href="NyNitfHead.xsl" />

<xsl:template match="/html">
<xsl:comment>
&lt;!DOCTYPE nitf SYSTEM "nitf-3-1.dtd">
</xsl:comment>
<nitf version="-//IPTC//DTD NITF 3.1//EN" change.date="July 3, 2002" change.time="19:30" baselang="no-NO">

<xsl:call-template name="messageHead"/> 

<body>
<body.head>
<hedline>
<!--	<hl1><xsl:value-of select="body//p[@class='Infolinje']"/></hl1> -->
	<hl1>
		<xsl:choose>
		<xsl:when test="body//p[@class='Infolinje']">
			<xsl:value-of select="body//p[@class='Infolinje']" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="string-length(body//p) &lt; 100"><xsl:value-of select="body//p" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="substring(body//p, 1, 100)" />.....</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
		</xsl:choose>
	</hl1>
</hedline>

<xsl:if test="body//p[@class='Byline']">
	<byline>
	<xsl:for-each select="body//p[@class='Byline']">
		<xsl:value-of select="."/>&#32;</xsl:for-each>
	</byline>
</xsl:if>

</body.head>
<body.content>
<xsl:apply-templates select="body/div/*"/>

<xsl:call-template name="media" />

</body.content>

<body.end>
	<tagline>
		<xsl:for-each select="head/meta[@name='NTBEpostAdresse']">
			<a>
				<xsl:attribute name="href">mailto:<xsl:value-of select="@content"/></xsl:attribute>
				<xsl:value-of select="@content"/>
			</a>
		</xsl:for-each>
	</tagline>
</body.end>

</body>
</nitf>	
</xsl:template>

<!-- Erstattet av 
<xsl:template match="head">
	<xsl:copy-of select="meta"/>
	<xsl:apply-templates select="xml"/>
</xsl:template>

<xsl:template match="xml/*/*">
	<meta><xsl:value-of select="."/></meta>
</xsl:template>
-->

<!--
<xsl:template match="p">
	<p>
	<xsl:attribute name="class"><xsl:value-of select="@class"/></xsl:attribute>
	<xsl:apply-templates/>
	</p>
</xsl:template>
-->

<!-- default template for unknown p-class names-->
<xsl:template match="p">
	<p class="txt"><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="p[@class='Brdtekst']">
	<p class="txt"><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="p[@class='BrdtekstInnrykk']">
	<p class="txt-ind"><xsl:apply-templates/></p>
</xsl:template>

<!-- Template to include wrong use of other Ingress-s as Mellomtittel -->
<xsl:template match="p[@class='Ingress' and position() &gt; 1]">
	<hl2><xsl:value-of select="."/></hl2>
</xsl:template>

<!-- Template for normal use of Ingress -->
<xsl:template match="p[@class='Ingress']">
	<p lede="true" class="lead"><xsl:value-of select="."/></p>
</xsl:template>

<!-- Empty Template. Will discard 1st infolinje in text -->
<xsl:template match="p[@class='Infolinje']">
</xsl:template>
 
<!-- Template to include wrong use of other infolinje-s as Mellomtittel -->
<xsl:template match="p[@class='Infolinje' and position() &gt; 1]">
	<hl2><xsl:value-of select="."/></hl2> 
</xsl:template>

<xsl:template match="p[@class='Mellomtittel']">
	<hl2><xsl:value-of select="."/></hl2>
</xsl:template>

<!-- In case use of Header 1 to 9 style in cut and pastes documents 
	into Notabene, convert to Mellomtittel (hl2) -->
<xsl:template match="h1 | h2 | h3 | h4 | h5 | h6">
	<hl2><xsl:value-of select="."/></hl2>
</xsl:template>

<xsl:template match="p[@class='Tabellkode']">
	<p class="table-code"><xsl:value-of select="."/></p>
</xsl:template>

<!-- Handles bold text -->
<xsl:template match="b">
	<em class="bold"><xsl:value-of select="."/></em>
</xsl:template>

<!-- Handles italic text -->
<xsl:template match="i">
	<em class="italic"><xsl:value-of select="."/></em>
</xsl:template>

<!-- Handles underlined text -->
<xsl:template match="u">
	<em class="underline"><xsl:value-of select="."/></em>
</xsl:template>

<!-- Handles all bold, italic and underlined text -->
<!--
<xsl:template match="b | i | u">
	<em> <xsl:attribute name="class"><xsl:value-of select="name()"/></xsl:attribute>
	<xsl:value-of select="."/></em>
</xsl:template>
-->

<!-- Handles soft line breaks in text -->
<xsl:template match="br">
	<!--</xsl:template><br><xsl:value-of select="."/></br>-->
	<br/>
</xsl:template>

<!-- Handles HTTP -->
<xsl:template match="a[@name='href']">
	<a><xsl:attribute name="href">
	<xsl:value-of select="@href"/>
	</xsl:attribute><xsl:value-of select="."/>
	</a>
</xsl:template>

<!--
<xsl:template match="span">
	<span><xsl:value-of select="."/></span>
	<span><xsl:apply-templates/></span>
</xsl:template>
-->

<xsl:template match="table">
	<table>
	<xsl:apply-templates select="tr"/>
	</table>
</xsl:template>

<xsl:template match="tr">
<tr>
	<xsl:apply-templates select="td"/>
</tr>
</xsl:template>

<xsl:template match="td">
	<td>
	<xsl:if test="p/@align!=''">
	<xsl:attribute name="align"><xsl:value-of select="p/@align"/></xsl:attribute>
	</xsl:if>
	<xsl:value-of select="."/>
	</td>
</xsl:template>

<xsl:template name="media">
	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '1']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr1']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst1']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '2']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr2']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst2']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '3']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr3']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst3']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '4']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr4']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst4']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '5']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr5']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst5']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '6']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr6']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst6']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '7']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr7']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst7']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '8']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr8']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst8']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '9']">
	<media media-type="image">
		<media-reference mime-type="image/jpeg">
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr9']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst9']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='ntb-lyd']/@content[. != '']">
		<media media-type="audio">
			<media-reference mime-type="application/x-shockwave-flash">
			<xsl:attribute name="source">http://194.19.39.29/kunde/ntb/flash/<xsl:value-of select="adm/ntb-lyd"/>.swf</xsl:attribute>
			</media-reference>
			<media-reference mime-type="image/gif" source="http://194.19.39.29/kunde/ntb/grafikk/ntb.gif"/>
			<media-reference mime-type="text/javascript" source="http://194.19.39.29/kunde/ntb/flashsound.js"/>
		</media>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>