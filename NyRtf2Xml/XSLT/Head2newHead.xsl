<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="UTF-8" indent="yes" standalone="yes" omit-xml-declaration="yes"/>

<!-- 
	Styleshet for transformasjon fra WORD XML-format med Notabene headers til NITF
	Sist endret Av Roar Vestre 29.11.2002
-->

<xsl:template match="/message">
<head>

<xsl:comment> -- Adm felt fra Notabene </xsl:comment>
<meta name="timestamp"><xsl:attribute name="content"><xsl:value-of select="adm/timestamp"/></xsl:attribute></meta>
<meta name="foldername"><xsl:attribute name="content"><xsl:value-of select="adm/folder"/></xsl:attribute></meta>
<meta name="filename"><xsl:attribute name="content"><xsl:value-of select="adm/filename"/></xsl:attribute></meta>
<meta name="madebydistributor"><xsl:attribute name="content"><xsl:value-of select="adm/madebydistributor"/></xsl:attribute></meta>

<xsl:comment> -- Andre header-felt fra Notabene</xsl:comment>
<meta name="subject"><xsl:attribute name="content"><xsl:value-of select="subject"/></xsl:attribute></meta>

<meta name="NTBStoffgruppe"><xsl:attribute name="content"><xsl:value-of select="group"/></xsl:attribute></meta>
<meta name="NTBUndergruppe"><xsl:attribute name="content"><xsl:value-of select="subgroup"/></xsl:attribute></meta>
<meta name="NTBHovedKategorier"><xsl:attribute name="content"><xsl:value-of select="category"/></xsl:attribute></meta>

<xsl:for-each select="subcategory/field">
<meta>
<xsl:attribute name="name"><xsl:value-of select="name"/></xsl:attribute>
<xsl:attribute name="content"><xsl:value-of select="value"/></xsl:attribute>
</meta>
</xsl:for-each>

<!--
<meta name="ntb-timesent"><xsl:attribute name="content"><xsl:value-of select="timesent"/></xsl:attribute></meta>
<meta name="ntb-importance"><xsl:attribute name="content"><xsl:value-of select="importance"/></xsl:attribute></meta>
-->

<xsl:comment> -- Samling av alle brukte fields fra Notabene </xsl:comment>
<xsl:for-each select="fields/field">
<meta>
<xsl:attribute name="name"><xsl:value-of select="name"/></xsl:attribute>
<xsl:attribute name="content"><xsl:value-of select="value"/></xsl:attribute>
</meta>
</xsl:for-each>

<xsl:if test="sms[.!='']">
<meta name="SMSTextFelt"><xsl:attribute name="content"><xsl:value-of select="sms"/></xsl:attribute></meta>
</xsl:if>

<xsl:if test="lyd[.!='']">
<meta name="ntb-lydfil"><xsl:attribute name="content"><xsl:value-of select="lyd"/></xsl:attribute></meta>
<!--meta name="ntb-lyd"><xsl:attribute name="content"><xsl:value-of select="lydfilnavn" /></xsl:attribute></meta-->

<!-- Extra insert for Sound-file with path!
<meta name="ntb-lydfil"><xsl:attribute name="content"><xsl:value-of select="adm/ntb-lydfil" /></xsl:attribute></meta>
-->
</xsl:if>

<!--
<meta name="ntb-distribusjonsKode"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBDistribusjonsKode']/value"/></xsl:attribute></meta>
<meta name="ntb-kanal"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBKanal']/value"/></xsl:attribute></meta>
<meta name="ntb-meldingsSign"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBMeldingsSign']/value"/></xsl:attribute></meta>
<meta name="ntb-meldingsType"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBMeldingsType']/value"/></xsl:attribute></meta>
<meta name="ntb-bilderAntall"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBBilderAntall']/value"/></xsl:attribute></meta>
<meta name="ntb-meldingsVersjon"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBMeldingsVersjon']/value"/></xsl:attribute></meta>
<meta name="ntb-utDato"><xsl:attribute name="content"><xsl:value-of select="fields/field[name='NTBUtDato']/value"/></xsl:attribute></meta>
-->
</head>

</xsl:template>


</xsl:stylesheet>
