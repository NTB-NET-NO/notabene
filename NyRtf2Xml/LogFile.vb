Imports System.Text
Imports System.IO

Public Module LogFile

    Public Const TEXT_LINE As String = "----------------------------------------------------------------------------------"
    Public Const DATE_TIME As String = "yyyy-MM-dd HH:mm:ss"
    Public Const DATE_LOGFILE As String = "yyyy-MM-dd"

    Public myEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")
    Public txtEncoding As Encoding = Encoding.GetEncoding(1252)

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.WriteLine(TEXT_LINE)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

    End Sub

    Public Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim strLine As String

        strLine = Format(Now, DATE_TIME) & ": " & strMessage

        Dim w As StreamWriter = New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, myEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False, Optional ByVal encode As String = "windows-1252")
        Dim w As New StreamWriter(strFileName, append, Encoding.GetEncoding(encode))
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Function ReadFile(ByVal strFileName As String, Optional ByVal encode As String = "windows-1252") As String
        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, Encoding.GetEncoding(encode))      ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

End Module
