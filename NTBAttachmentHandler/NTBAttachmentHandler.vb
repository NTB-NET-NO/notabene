Option Strict Off
Option Explicit On
Imports System.IO
Imports System.Text
Imports System
Imports VB = Microsoft.VisualBasic


Friend Class NTBAttachmentHandler
    Inherits System.Windows.Forms.Form

#Region "Windows Form Designer generated code "
    Public Sub New()
        MyBase.New()
        If m_vb6FormDefInstance Is Nothing Then
            If m_InitializingDefInstance Then
                m_vb6FormDefInstance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
                        m_vb6FormDefInstance = Me
                    End If
                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Public WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PollTimer = New System.Timers.Timer()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Enabled = True
        Me.PollTimer.Interval = 2000
        Me.PollTimer.SynchronizingObject = Me
        '
        'NTBAttachmentHandler
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(190, 142)
        Me.Name = "NTBAttachmentHandler"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "NTBAttachmentHandler"
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
#End Region
#Region "Upgrade Support "
    Private Shared m_vb6FormDefInstance As NTBAttachmentHandler
    Private Shared m_InitializingDefInstance As Boolean
    Public Shared Property DefInstance() As NTBAttachmentHandler
        Get
            If m_vb6FormDefInstance Is Nothing OrElse m_vb6FormDefInstance.IsDisposed Then
                m_InitializingDefInstance = True
                m_vb6FormDefInstance = New NTBAttachmentHandler()
                m_InitializingDefInstance = False
            End If
            DefInstance = m_vb6FormDefInstance
        End Get
        Set(ByVal Value As NTBAttachmentHandler)
            m_vb6FormDefInstance = Value
        End Set
    End Property
#End Region


    'MAPI objects
    Dim mpsSession As MAPI.Session
    Dim cdoInfoStores As MAPI.InfoStores
    Dim cdoInfoStore As MAPI.InfoStore

    Dim fRoot As MAPI.Folder
    Dim fError As MAPI.Folder

    'Variables to hold INI-values
    'These values are assigned in GetIniValues() in this code
    Dim strInfoStoreName As String
    Dim strProfileName As String
    Dim nRetryInterval As Integer
    Dim nInterval As Integer
    'Dim nRTFHeaderSize As Long
    'Dim nMaxMessageSize As Long
    Dim strCopyMessageType As String
    Dim intFolderNumber As Short
    Dim intSenderNumber As Short

    Dim iniFile As String
    Dim strFileFormat As Object
    Dim LOGFILE As String
    Dim strErrorFolder As String
    'Dim strDefaultOutDir As String
    Dim strTempDir As String
    Dim colIniFolders As Collection
    Dim blnChkLog As Boolean
    Dim strChkLog As String
    'Dim CHKLOGFILE As String
    Dim lngMsgSetCnt As Integer
    Dim lngOpenMsgDelay As Integer
    Dim lngCopyMsgDelay As Integer
    Dim strScanFldLast As String

    Dim blnScanLast As Boolean
    Dim strPictOutDir As String
    Dim strPDFOutDir As String
    Dim strOtherOutDir As String
    Dim strEPSOutDir As String
    Dim strDocOutDir As String
    Dim strSITOutDir As String

    Dim strFrom As String
    Dim strSubject As String
    Dim strBildeNavn As String
    Dim blnSendMail As Short
    Dim strMailAddress As String
    Dim strSenderMail As String
    Dim strTextBody As String
    Dim strSubjectHeader As String
    Dim strFolderHeader As String
    Dim strFileNameHeader As String

    Dim bPict As Boolean
    'Dim bOutDir As Boolean

    'For Attachment/Bilder
    Dim MailAttachment As MAPI.Attachment
    Dim strAttachmentType As String
    Dim strIndex As String
    Dim SearchChar As String
    Dim strPictName As String
    Dim MyPos As Short

    'Message counter
    Dim msgCnt As Integer

    'Collection to hold message folders
    Dim coFolders As Collection

    'Logger object
    Dim Logger As cLogger

    'String Constants
    Dim SEP As New VB6.FixedLengthString(1, ":")
    Dim VERSION As New VB6.FixedLengthString(27, "NTBAttachmentStore BETA 1.0")





    Private Sub NTBAttachmentHandler_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim strDisplayName As String
        Dim bStarted As Boolean
        Dim iIniErr As Short
        Dim varFolderName As Object


        On Error GoTo Err_Load

        Logger = New cLogger()
        coFolders = New Collection()
        '  Set coHeaders = New Collection


        iniFile = "C:\Documents and Settings\Administrator.NTSERVER\My Documents\Visual Studio Projects\NTBAttachmentHandler\NTBAttachmentHandler.ini"

        If CBool(LCase(CStr(VB.Left(VB.Command(), 2) = "-i"))) Then
            iniFile = Trim(VB.Right(VB.Command(), Len(VB.Command()) - 2))
        ElseIf VB.Command() <> "" Then
            MsgBox("Invalid command option")
            End
        End If


        'Read ini file values
        iIniErr = GetIniValues(iniFile)
        Logger.Log(LOGFILE, "Start !s", "Version: " & VERSION.Value)
        Logger.Log(LOGFILE, "Load !s", "Settings read from " & iniFile)


        'This service will be run whenever the timer-event occurs so start the timer
        PollTimer.Interval = nInterval 'set interval
        PollTimer.Start() 'enable timer



        Logger.Log(LOGFILE, "Load !s", "Timer started - PollTimer.GetLifetimeService " & PollTimer.GetLifetimeService)
        Exit Sub

Err_Load:
        Logger.Log(LOGFILE, "Load !x", Err.Description)
        End

    End Sub



    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        Dim F As cMsgFolder
        Dim M As MAPI.Message
        Dim objNewMessage As MAPI.Message
        Dim objCopyFolder As Object
        Dim blnKeepLooking As Boolean
        Dim blnProcessFolder As Boolean

        On Error GoTo Err_Timer

        'Disable time while processing
        PollTimer.Stop()

        'Log in if we're not there yet
        Do While fRoot Is Nothing
            Logger.Log(LOGFILE, "Timer debug", "G�r til GetSession")
            GetSession()
            Logger.Log(LOGFILE, "Timer debug", "Ferdig med GetSession")
            If strErrorFolder <> "" Then
                fError = GetFolderByName(strErrorFolder, fRoot.Folders)
            Else
                fError = Nothing
            End If

            If Not fRoot Is Nothing Then
                Logger.Log(LOGFILE, "Timer debug", "G�r til MakeFolderCollection")
                MakeFolderCollection()
                Logger.Log(LOGFILE, "Timer debug", "Ferdig med MakeFolderCollection")
            End If
        Loop

        Do
            blnKeepLooking = False

            'Folders to be scanned should already be stored in coFolders collection
            For Each F In coFolders

                'If folder is not empty - check it
                If F.Folder.Messages.Count > 0 Then

                    Logger.Log(LOGFILE, "Timer debug", "Teller opp ant. meldinger i folder --> " & F.Folder.Messages.Count)

                    'Check for delay
                    blnProcessFolder = True

                    'Check for delay
                    If F.Delay > 0 Then
                        If F.LastExport.AddSeconds(F.Delay) > Now Then
                            blnProcessFolder = False
                        Else
                            F.LastExport = Now
                        End If
                    End If

                    If blnProcessFolder Then

                        Logger.Log(LOGFILE, "Timer debug", "F�r F.Folder.Messages.Sort i " & F.Folder.Name)
                        F.Folder.Messages.Sort()

                        Logger.Log(LOGFILE, "Timer debug", "F�r F.Folder.Messages.GetFirst i " & F.Folder.Name)
                        M = F.Folder.Messages.GetFirst

                        Logger.Log(LOGFILE, "Timer debug", "G�r til rutine ExportMessage")

                        'Export and copy message
                        ExportMessage(M, F)

                        Logger.Log(LOGFILE, "Timer debug", "Tilbake fra rutine ExportMessage")

                        M = Nothing
                        blnKeepLooking = True
                        Exit For
                    End If
                End If

            Next F
        Loop Until blnKeepLooking = False

        F = Nothing

        PollTimer.Start()

        Exit Sub

Err_Timer:
        Logger.Log(LOGFILE, "Timer !x", Err.Description & " - " & Err.Number & Err.GetException.StackTrace)
        SendErrorPollTimer()
        PollTimer.Start()

    End Sub

    Public Sub SendErrorPollTimer()

        On Error Resume Next

        Dim oFolder As MAPI.Folder
        Dim oMessages As MAPI.Messages
        Dim omsg As Object
        Dim oRcpt As Object
        Dim CDOFileData

        Dim objAttach As Object

        Dim wshNetwork
        Dim wshShell
        Dim wshMiljoProsess

        Dim strSignatur As String
        Dim strMaskinnavn As String
        Dim strDomene As String

        Logger.Log(LOGFILE, "Rutine SendErrorPollTimer - Servicen m� restartes")

        wshNetwork = CreateObject("Wscript.Network")
        wshShell = CreateObject("Wscript.Shell")
        wshMiljoProsess = wshShell.Environment("Process")

        strSignatur = wshNetwork.UserName
        strMaskinnavn = wshNetwork.ComputerName
        strDomene = wshNetwork.UserDomain

        oFolder = mpsSession.Outbox
        oMessages = oFolder.Messages
        omsg = oMessages.Add

        oRcpt = omsg.Recipients
        oRcpt.Add(, "SMTP:terje@caps.no", ) ' CdoTo)
        oRcpt.Resolve()
        omsg.Type = "IPM.Note"
        omsg.Subject = "Restart NTBAttachmentHandler Servicen!"
        omsg.Text = "Restart NTBAttachmentHandler Servicen PollTimer_Elapsed har feilet"

        objAttach = omsg.Attachments.Add     ' add the attachment 

        With objAttach
            .Type = CDOFileData
            .Position = 0 ' render at first character of message 
            .Name = "C\NTBAttachmentHandler\NTBAttachmentHandler.log"
            .ReadFromFile("C:\ReportersOnTheRoadService\Logg\NTBAttachmentHandler.log")
        End With
        objAttach.Name = "J:\ReportersOnTheRoadService\Logg.log"
        objAttach = Nothing

        omsg.Update()
        omsg.Send(False, False)

        oRcpt = Nothing
        omsg = Nothing
        oMessages = Nothing
        oFolder = Nothing

        'denne rutine sender en SMS melding
        '  SMSWriteFile()

    End Sub






    Private Sub GetOutDir()

        '***[OutDir] Terje/Pictures 11.03.2002
        strPictOutDir = gfnGetFromIni(iniFile, "OutDir0" & intFolderNumber, "PictOutDir", "")
        bPict = Len(strPictOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues !s", "PictOutDir=" & strPictOutDir)

        '***[OutDir & t] Terje/PDF 21.03.2002
        strPDFOutDir = gfnGetFromIni(iniFile, "OutDir0" & intFolderNumber, "PDFOutDir", "")
        bPict = Len(strPDFOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues !s", "PDGOutDir=" & strPDFOutDir)

        '***[OutDir & t] Terje/Other 21.03.2002
        strOtherOutDir = gfnGetFromIni(iniFile, "OutDir0" & intFolderNumber, "OtherOutDir", "")
        bPict = Len(strOtherOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues !s", "OtherOutDir=" & strOtherOutDir)

        '***[OutDir & t] Terje/EPS 21.03.2002
        strEPSOutDir = gfnGetFromIni(iniFile, "OutDir0" & intFolderNumber, "EPSOutDir", "")
        bPict = Len(strEPSOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues !s", "EPSOutDir=" & strEPSOutDir)

        '***[OutDir & t] Terje/Doc 14.05.2003
        strDocOutDir = gfnGetFromIni(iniFile, "OutDir0" & intFolderNumber, "DocOutDir", "")
        bPict = Len(strDocOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues !s", "DocOutDir=" & strDocOutDir)

        '***[OutDir & t] Terje/SIT 21.03.2002
        strSITOutDir = gfnGetFromIni(iniFile, "OutDir0" & intFolderNumber, "SITOutDir", "")
        bPict = Len(strSITOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues !s", "SITOutDir=" & strSITOutDir)

        '***[FolderMail]
        Dim strFolderMail As String
        ' Get mail-adress - where to send messages
        strFolderMail = "FolderMail0" & intFolderNumber
        strMailAddress = gfnGetFromIni(iniFile, "FolderMail", strFolderMail, strMailAddress)
        Logger.Log(LOGFILE, "Where to send notification : ", "Mail address : " & strMailAddress)
    End Sub

    Private Sub CreateOutDirectorys()

        Logger.Log(LOGFILE, "Folders : ", "** Checking if Folder path exists! ")

        If (bPict) = True Then
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC/commoner/redir/redirect.htm?keyword="vbup1041"'
            If Dir(strPictOutDir, FileAttribute.Directory) = "" Then
                'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC/commoner/redir/redirect.htm?keyword="vbup1041"'
                Do While Dir(strPictOutDir, FileAttribute.Normal) <> ""
                    strPictOutDir = strPictOutDir & "0"
                Loop
                MkDir(strPictOutDir)
                Logger.Log(LOGFILE, "Lager folder : ", "** New folder created: " & strPictOutDir)
            End If
        End If
        If (bPict) = True Then
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC/commoner/redir/redirect.htm?keyword="vbup1041"'
            If Dir(strPDFOutDir, FileAttribute.Directory) = "" Then
                'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC/commoner/redir/redirect.htm?keyword="vbup1041"'
                Do While Dir(strPDFOutDir, FileAttribute.Normal) <> ""
                    strPDFOutDir = strPDFOutDir & "0"
                Loop
                MkDir(strPDFOutDir)
                Logger.Log(LOGFILE, "Lager folder : ", "** New folder created: " & strPDFOutDir)
            End If
        End If
        If (bPict) = True Then
            'UPGRADE_WARNING: Dir has a new behavior. Click for more: 'ms-help://MS.VSCC/commoner/redir/redirect.htm?keyword="vbup1041"'
            If Dir(strOtherOutDir, FileAttribute.Directory) = "" Then
                Do While Dir(strOtherOutDir, FileAttribute.Normal) <> ""
                    strOtherOutDir = strOtherOutDir & "0"
                Loop
                MkDir(strOtherOutDir)
                Logger.Log(LOGFILE, "Lager folder : ", "** New folder created: " & strOtherOutDir)
            End If
        End If
        If (bPict) = True Then
            If Dir(strEPSOutDir, FileAttribute.Directory) = "" Then
                Do While Dir(strEPSOutDir, FileAttribute.Normal) <> ""
                    strEPSOutDir = strEPSOutDir & "0"
                Loop
                MkDir(strEPSOutDir)
                Logger.Log(LOGFILE, "Lager folder : ", "** New folder created: " & strEPSOutDir)
            End If
        End If


        If (bPict) = True Then
           If Dir(strDocOutDir, FileAttribute.Directory) = "" Then
               Do While Dir(strDocOutDir, FileAttribute.Normal) <> ""
                    strDocOutDir = strDocOutDir & "0"
                Loop
                MkDir(strDocOutDir)
                Logger.Log(LOGFILE, "Lager folder : ", "** New folder created: " & strDocOutDir)
            End If
        End If



        If (bPict) = True Then
            If Dir(strSITOutDir, FileAttribute.Directory) = "" Then
                Do While Dir(strSITOutDir, FileAttribute.Normal) <> ""
                    strSITOutDir = strSITOutDir & "0"
                Loop
                MkDir(strSITOutDir)
                Logger.Log(LOGFILE, "Make folder : ", "** New folder created: " & strSITOutDir)
            End If
        End If
    End Sub

    Private Sub SenderDomainOutDir()

        Dim strFolderMail As String

        '***[OutDir] Terje/Pictures 11.03.2002
        strPictOutDir = gfnGetFromIni(iniFile, "SenderDomainOutDir0" & intSenderNumber, "PictOutDir", "")
        bPict = Len(strPictOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues SenderDomainOutDir", "PictOutDir=" & strPictOutDir)

        '***[OutDir & t] Terje/PDF 21.03.2002
        strPDFOutDir = gfnGetFromIni(iniFile, "SenderDomainOutDir0" & intSenderNumber, "PDFOutDir", "")
        bPict = Len(strPDFOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues SenderDomainOutDir", "PDGOutDir=" & strPDFOutDir)

        '***[OutDir & t] Terje/Other 21.03.2002
        strOtherOutDir = gfnGetFromIni(iniFile, "SenderDomainOutDir0" & intSenderNumber, "OtherOutDir", "")
        bPict = Len(strOtherOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues SenderDomainOutDir", "OtherOutDir=" & strOtherOutDir)

        '***[OutDir & t] Terje/EPS 21.03.2002
        strEPSOutDir = gfnGetFromIni(iniFile, "SenderDomainOutDir0" & intSenderNumber, "EPSOutDir", "")
        bPict = Len(strEPSOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues SenderDomainOutDir", "EPSOutDir=" & strEPSOutDir)

        '***[OutDir & t] Terje/doc 14.05.2003
        strDocOutDir = gfnGetFromIni(iniFile, "SenderDomainOutDir0" & intSenderNumber, "DocOutDir", "")
        bPict = Len(strEPSOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues SenderDomainOutDir", "DocOutDir=" & strDocOutDir)

        '***[OutDir & t] Terje/SIT 21.03.2002
        strSITOutDir = gfnGetFromIni(iniFile, "SenderDomainOutDir0" & intSenderNumber, "SITOutDir", "")
        bPict = Len(strSITOutDir) > 0
        Logger.Log(LOGFILE, "GetIniValues SenderDomainOutDir", "SITOutDir=" & strSITOutDir)

        '***[FolderMail]

        ' Get mail-adress - where to send messages
        strFolderMail = "SenderMail0" & intSenderNumber
        strSenderMail = gfnGetFromIni(iniFile, "SenderDomainMail", strFolderMail, strMailAddress)
        Logger.Log(LOGFILE, "Notifications for SenderDomains: ", "Mail address : " & strSenderMail)


    End Sub


    Private Sub ExportMessage(ByRef objMessage As MAPI.Message, ByRef clsMsgFolder As cMsgFolder)

        'For adresseinformasjon
        Dim CdoAddressEntrySender As MAPI.AddressEntry
        '    Dim CdoAddressEntryTo
        Dim objRecipColl As Object
        Dim iRecipCollIndex As Object
        Dim objOneRecip As Object
        Dim objAddrEntry As Object
        Dim strMsg As Object

        'For � h�ndtere egne senderdomain
        Dim strSender As String
        Dim intCounter As Object

        Dim filehandle As Object
        Dim fh As Short
        Dim strFileName As Object
        Dim tempFileName As String

        Dim textfilehandle As Short

        Dim outFileName As String
        Dim strTextFile As String
        Dim errmsg As String
        Dim tmr1 As Object
        Dim tmr2 As Date
        Dim t As Double
        Dim i As Short
        Dim objCopyMessage As MAPI.Message
        Dim strNow As String


        ' For � finne ut hvor bilder skal lagres i systemet
        Dim strFromFolder As String
        Dim intFromFolder As Short
        strFromFolder = clsMsgFolder.FolderName
        Dim strFolder As String

        'Stopper prosessering dersom det kommer en melding med f�lgene innhold eller type:
        If strSubject = "Stop processing!" Or objMessage.Type = "IPM.Note.Stop" Then
            Logger.Log(LOGFILE, "ExportMessage !Licence", "Licence not Registrated - Contact Caps on phone : + 47 90 54 90 33")
            Exit Sub
        End If


        Logger.Log(LOGFILE, "From Folder :  ", "Foldername: " & clsMsgFolder.FolderName)

        On Error GoTo export_err

        filehandle = FreeFile()

        i = 1
        Do
            strFolder = gfnGetFromIni(iniFile, "Folders", "Folder" & VB.Format(i, "00"), "")
            If LCase(strFolder) = LCase(strFromFolder) Then
                intFolderNumber = i
                GetOutDir()
                'MsgBox strFromFolder & " - " & "Folder" & Format(i)
                Exit Do
            End If
            i = i + 1
        Loop Until i > 9


        'Skjekker om utdestinasjon (if path er spesifisert)
        CreateOutDirectorys()


        ' Finner hvilke som skal ha meldingen

        '        Set CdoAddressEntryTo = objMessage.Recipients

        objRecipColl = objMessage.Recipients


        ' Set the Recipient object variable from Recipients_Item()
        If 0 = objRecipColl.Count Then
            strFrom = "nouser@caps.no"
            'Exit Function
        Else
            ''''''''''''''''''''''iRecipCollIndex = 1
            '''''''''''''''''''''''objOneRecip = objRecipColl.Item(iRecipCollIndex)


            ' could also be objRecipColl(iRecipCollIndex) since .Item is default
            '''''''objAddrEntry = objOneRecip.AddressEntry

            ''''''''''''''''''''''   Logger.Log(LOGFILE, "To :  ", "To Mailaddress " & objAddrEntry)

            ' strMsg = "Recipient full address = " & objOneRecip.Address
            ' strMsg = strMsg & "; AddressEntry type = " & objAddrEntry.Type
            ' strMsg = strMsg & "; AddressEntry address = " & objAddrEntry.Address
            ' Logger.Log LOGFILE, "To :  ", "To Mailaddress " & strMsg

            CdoAddressEntrySender = objMessage.Sender
            strFrom = CdoAddressEntrySender.Address
        End If

        On Error Resume Next


        blnSendMail = 1
        strBildeNavn = ""
        strTextBody = objMessage.Text
        strSubject = objMessage.Subject

        strSenderMail = ""

        MyPos = InStr(1, strFrom, "@", 1)
        intCounter = Len(strFrom)
        MyPos = intCounter - MyPos
        strSender = VB.Right(strFrom, MyPos)
        MyPos = InStr(1, strSender, ".", 1)
        strSender = Mid(strSender, 1, MyPos - 1)

        If strSender <> "" Then
            Logger.Log(LOGFILE, "Message Senders Domain : ", strSender)
        End If

        On Error GoTo export_err

        i = 1
        Dim strSenderName As Object
        Do
            strSenderName = gfnGetFromIni(iniFile, "SenderDomain", "Sender" & VB.Format(i, "00"), "")
            If LCase(strSenderName) = LCase(strSender) Then
                intSenderNumber = i
                strSenderMail = gfnGetFromIni(iniFile, "SenderDomainMail", "SenderMail0" & intSenderNumber, "")
                SenderDomainOutDir()
                CreateOutDirectorys()
                blnSendMail = 2
                Exit Do
            End If

            i = i + 1
        Loop Until i > 9


        ' For p� plukke vekk typebetegnelsen i bildenavnet
        SearchChar = "."


        For Each MailAttachment In objMessage.Attachments
            strAttachmentType = UCase(VB.Right(CStr(MailAttachment.Name), 3))
            strIndex = "A" & MailAttachment.Index & "-"

            MyPos = InStr(1, CStr(MailAttachment.Name), SearchChar, 0)
            strPictName = Mid(CStr(MailAttachment.Name), 1, MyPos - 1)

            Select Case strAttachmentType
                Case "JPG"
                    MailAttachment.WriteToFile(strPictOutDir & "\" & strIndex & strPictName & "." & strAttachmentType)
                    strTextFile = strPictOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & strAttachmentType

                Case "TIF"
                    MailAttachment.WriteToFile(strPictOutDir & "\" & strIndex & strPictName & "." & strAttachmentType)
                    strTextFile = strPictOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & strAttachmentType

                Case "BMP"
                    MailAttachment.WriteToFile(strPictOutDir & "\" & strIndex & strPictName & "." & strAttachmentType)
                    strTextFile = strPictOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & strAttachmentType

                Case "IFF"
                    MailAttachment.WriteToFile(strPictOutDir & "\" & strIndex & strPictName & ".TIF")
                    strTextFile = strPictOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "TIF"

                Case "PEG"
                    MailAttachment.WriteToFile(strPictOutDir & "\" & strIndex & strPictName & ".JPG")
                    strTextFile = strPictOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "JPG"

                Case "GIF"
                    MailAttachment.WriteToFile(strPictOutDir & "\" & strIndex & strPictName & ".GIF")
                    strTextFile = strPictOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "GIF"

                Case "EPS"
                    MailAttachment.WriteToFile(strEPSOutDir & "\" & strIndex & strPictName & ".EPS")
                    strTextFile = strEPSOutDir & "\" & strIndex & strPictName & ".txt"
                    textfilehandle = FreeFile()
                    FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    PrintLine(textfilehandle, strTextBody)
                    FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "EPS"

                Case "DOC"
                    MailAttachment.WriteToFile(strDocOutDir & "\" & strIndex & strPictName & ".Doc")
                    'strTextFile = strDocOutDir & "\" & strIndex & strPictName & ".txt"
                    'textfilehandle = FreeFile()
                    'FileOpen(textfilehandle, strTextFile, OpenMode.Output)
                    'PrintLine(textfilehandle, strTextBody)
                    'FileClose(textfilehandle)
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "Doc"

                Case "sit"
                    MailAttachment.WriteToFile(strSITOutDir & "\" & strIndex & strPictName & ".sit")
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "sit"

                Case "PDF"
                    MailAttachment.WriteToFile(strPDFOutDir & "\" & strIndex & strPictName & ".PDF")
                    strBildeNavn = strBildeNavn & " - " & strPictName & "." & "PDF"

                Case Else
                    MailAttachment.WriteToFile(strOtherOutDir & "\" & strIndex & strPictName & "." & strAttachmentType)
                    strBildeNavn = strBildeNavn & " - " & strPictName
            End Select
        Next MailAttachment

        If objMessage.Attachments.Count = 0 Then
            blnSendMail = 3
        End If

        Call SendMail()

        'Make copies
        For i = 1 To clsMsgFolder.CopyFolders.Count()
            tmr1 = Now
            objCopyMessage = objMessage.CopyTo(clsMsgFolder.CopyFolders.Item(i).ID)

            objCopyMessage.Update()
            tmr2 = Now
            t = tmr2.Subtract(tmr1).TotalMilliseconds

            Logger.Log(LOGFILE, clsMsgFolder.Folder.Name & " Recived message ", "'" & strSubject & "' A copy is in => " & clsMsgFolder.CopyFolders.Item(i).Name & " (" & VB.Format(t, "#0") & " secs)")
            'Wait befor copy of next msg
            Sleep((lngCopyMsgDelay))
            objCopyMessage = Nothing
        Next

        'Delete message
        objMessage.Delete()

        'For each msg increase message counter
        msgCnt = msgCnt + 1

        'Sleep for a few ms. Take some load off the server
        Sleep((lngOpenMsgDelay))
        Exit Sub


export_err:
        Select Case Err.Number
            Case 0
                Err.Clear()
                On Error Resume Next
            Case 76
                Logger.Log(LOGFILE, "Creating Folders - Error : ", Err.Number & " --- " & Err.Description)
            Case Else
                Logger.Log(LOGFILE, "ExportMessage !x", Err.Description & " --- " & errmsg)
        End Select

        If fError Is Nothing Then
            '    errmsg = "Message '" & msg.Subject & "' deleted from " & folderName
            '    msg.Delete
        Else
            errmsg = " - Recived message '" & strSubject & "' moved from " & clsMsgFolder.FolderName & " to error folder"
            objMessage.MoveTo(fError.ID)
        End If

    End Sub

    Private Sub MakeFolderCollection()
        Dim varIniFolder As Object
        Dim clsMsgFolder As cMsgFolder
        Dim colCopyFolders As Collection
        Dim objFolder As MAPI.Folder
        Dim arrCopyFolders() As String
        Dim i As Short

        'make folder collection of out-folders
        For Each varIniFolder In colIniFolders
            clsMsgFolder = New cMsgFolder()
            clsMsgFolder.FolderName = varIniFolder.Folder
            clsMsgFolder.Folder = GetFolderByName(varIniFolder.Folder, (fRoot.Folders))

            clsMsgFolder.OutDir = varIniFolder.OutDir
            clsMsgFolder.Delay = varIniFolder.Delay

            Logger.Log(LOGFILE, "Folders !s", "= Checking folder : " & varIniFolder.Folder & " (" & clsMsgFolder.Folder.Name & ")")

            If varIniFolder.CopyFolders <> "" Then
                colCopyFolders = New Collection()
                arrCopyFolders = Split(varIniFolder.CopyFolders, ";")
                For i = 0 To UBound(arrCopyFolders)
                    objFolder = GetFolderByName(arrCopyFolders(i), (fRoot.Folders))
                    colCopyFolders.Add(objFolder)
                    Logger.Log(LOGFILE, "Folders !s", "= Message copy to: " & arrCopyFolders(i) & " (" & objFolder.Name & ")")
                    objFolder = Nothing
                Next
                clsMsgFolder.CopyFolders = colCopyFolders
            End If


            '  Logger.Log LOGFILE, "Folders !s", "= Delay: " & clsMsgFolder.Delay

            coFolders.Add(clsMsgFolder)

            colCopyFolders = Nothing
            clsMsgFolder = Nothing

        Next varIniFolder

        'Release memory
        colIniFolders = Nothing
    End Sub



    Private Function GetIniValues(ByRef iniFile As String) As Short

        'Date:          2002.09.05
        'Description:   Assigns values from ini file to variables

        On Error GoTo GetIniError

        Dim strtemp As Object

        blnChkLog = False


        '***[WinNT]
        On Error Resume Next '*** Se on Error Goto GetIniError lenger ned

        'get logfile location. NB! Read this value first from ini file, or nothing will be logged
        LOGFILE = gfnGetFromIni(iniFile, "WinNT", "LogFile", "C:\NTBAttachmentHandler.log")

        'Slettes i oppstart for at den ikke skal vokse for stor
        Kill(LOGFILE)
        Logger.Log(LOGFILE, "GetIniValues !s", "-- Logging started --")

        On Error GoTo GetIniError

        'get name of temporary folder
        strTempDir = gfnGetFromIni(iniFile, "WinNT", "TempDir", "C:\TEMP")
        Logger.Log(LOGFILE, "GetIniValues !s", "TempDir=" & strTempDir)

        '***[Exchange]
        'get profile name
        strProfileName = gfnGetFromIni(iniFile, "Exchange", "Profile", "")
        Logger.Log(LOGFILE, "GetIniValues", "Profile = " & strProfileName)

        'get infostore name from section "Exchange" of ini file, default value = "Public folders"
        strInfoStoreName = gfnGetFromIni(iniFile, "Exchange", "InfoStore", "")
        Logger.Log(LOGFILE, "GetIniValues !s", "InfoStore=" & strInfoStoreName)

        'get message type of copies
        '    strCopyMessageType = gfnGetFromIni(iniFile, "Exchange", "CopyMessageType", "")
        '    Logger.Log LOGFILE, "GetIniValues !s", "CopyMessageType=" & strCopyMessageType

        'get root folder
        'strRootFolder = gfnGetFromIni(iniFile, "Exchange", "RootFolder", "")
        'Logger.Log LOGFILE, "GetIniValues !s", "RootFolder=" & strRootFolder

        'get error folder
        strErrorFolder = gfnGetFromIni(iniFile, "Exchange", "ErrorFolder", "")
        Logger.Log(LOGFILE, "GetIniValues !s", "ErrorFolder=" & strErrorFolder)

        'get poll interval
        nInterval = 2000
        'nInterval = CLng(Val(gfnGetFromIni(iniFile, "WinNT", "Interval", "1")) * 1000)
        'Logger.Log(LOGFILE, "GetIniValues !s", "Interval=" & nInterval)

        'get logon retry interval
        nRetryInterval = 60000
        'nRetryInterval = CLng(Val(gfnGetFromIni(iniFile, "Exchange", "RetryInterval", "10") * 1000))
        'Logger.Log LOGFILE, "GetIniValues !s", "RetryInterval=" & nRetryInterval

        'get msg delay interval
        lngOpenMsgDelay = 100
        'lngOpenMsgDelay = CLng(Val(gfnGetFromIni(iniFile, "Exchange", "MsgDelay", "100")))
        'Logger.Log LOGFILE, "GetIniValues !s", "MsgDelay=" & lngOpenMsgDelay

        'get msg copy delay interval
        lngCopyMsgDelay = 100
        'lngCopyMsgDelay = CLng(Val(gfnGetFromIni(iniFile, "Exchange", "CopyMsgDelay", "100")))
        'Logger.Log LOGFILE, "GetIniValues !s", "CopyMsgDelay=" & lngCopyMsgDelay

        'get msg count
        lngMsgSetCnt = 20
        'lngMsgSetCnt = CLng(Val(gfnGetFromIni(iniFile, "Exchange", "MsgCount", "5")))
        'Logger.Log LOGFILE, "GetIniValues !s", "MsgCount=" & lngMsgSetCnt


        '***[Mail]
        ' Get mail-adress - where to send messages
        strMailAddress = gfnGetFromIni(iniFile, "Mail", "Mailaddress", "")
        Logger.Log(LOGFILE, "GetIniValues !s", "Mailadr=" & strMailAddress)

        '***[Folders]
        'get a collection of names of folders
        GetFoldersFromIni(iniFile)
        Logger.Log(LOGFILE, "GetIniValues", "Folders retrieved.")

        '***[ScanLastFolder]
        strScanFldLast = gfnGetFromIni(iniFile, "ScanLastFolder", "ScanLast", "")
        Logger.Log(LOGFILE, "GetIniValues !s", "ScanLast=" & strScanFldLast)

        Exit Function

GetIniError:
        Logger.Log(LOGFILE, "GetIniValues !x", Err.Number & " " & Err.Description)

    End Function

    Sub GetFoldersFromIni(ByRef iniFile As String)
        Dim clsIniFolder As cIniFolders
        Dim i As Short
        Dim strFolder As String
        Dim strOutDir As String

        colIniFolders = New Collection()

        'get ini values from keys Folder01, Folder02, Folder03...
        i = 1

        Do
            strFolder = gfnGetFromIni(iniFile, "Folders", "Folder" & VB.Format(i, "00"), "")

            If strFolder = "" Then
                Exit Do
            Else
                clsIniFolder = New cIniFolders()
                clsIniFolder.Folder = strFolder
                clsIniFolder.CopyFolders = gfnGetFromIni(iniFile, "Folders", "Copies" & VB.Format(i, "00"), "")
                colIniFolders.Add(clsIniFolder)

                clsIniFolder = Nothing
            End If

            i = i + 1
        Loop Until i > 99

        'if more than 99 out-folders... raise an error
        If i > 99 Then Err.Raise(vbObjectError + 14, "GetFolders !x", "** Too many folders **")

    End Sub

    Private Sub GetSession()

        Dim strPublicRootID As String
        Dim tagPublicRootEntryID As Integer
        'Dim strProfile As String

        Err.Clear()

        On Error Resume Next

        'Ikke i bruk for �yeblikket - sluttet plutselig � funke 01.04.2002 - Terje
        tagPublicRootEntryID = &H66310102

        'Create new session
        mpsSession = New MAPI.Session()
        'dersom det er angitt et profilnavn i ini fila, bruk den, ellers logg p� den gjeldende session
        If strProfileName = "" Then
            'logg p� med gjeldende profil
            mpsSession.Logon(, , False, False, 0, True)
        Else
            'logg p� angitt profil
            mpsSession.Logon(ProfileName:=strProfileName, ShowDialog:=True, NewSession:=True, ParentWindow:=0, NoMail:=True)
        End If

        fRoot = Nothing

        Logger.Log(LOGFILE, "GetSession !s", "Getting InfoStore and RootFolder")

        For Each cdoInfoStore In mpsSession.InfoStores
            Logger.Log(LOGFILE, "Find InfoStore !s ", cdoInfoStore.Name)
            If cdoInfoStore.Name = strInfoStoreName Then
                strPublicRootID = cdoInfoStore.Fields(tagPublicRootEntryID).Value
                fRoot = mpsSession.GetFolder(strPublicRootID, cdoInfoStore.ID)
                'isInfoStore = cdoInfoStore.Name
                Exit For
            End If
        Next


        ' Sender mail til meg selv n�r programmet starter
        Call SendTerje()

        'Check errors
        If Err.Number <> 0 Then
            Logger.Log(LOGFILE, "GetSession !x", Err.Number & " " & Err.Description)
            mpsSession.Logoff()
            mpsSession = Nothing
            Sleep(nRetryInterval)
        Else
            Logger.Log(LOGFILE, "GetSession !s", "Logged on to Exchange.")
        End If
    End Sub

    Private Function GetInfoStoreByName(ByRef ses As MAPI.InfoStores, ByRef strInfoStoreName As String) As MAPI.InfoStore

        'Date:          2003.05.15
        'Function:      Retrieves a folder object by supplying a name

        '  Dim infs As MAPI.InfoStores
        Dim inf As MAPI.InfoStore


        For Each inf In ses.Session
            If inf.Name = strInfoStoreName Then
                Return inf
            End If
        Next inf

        'if program reaches this point the infostore was not found
        GetInfoStoreByName = Nothing
        Err.Raise(vbObjectError + 13, "cParseFile.GetInfoStoreByName", "InfoStore not found: " & strInfoStoreName & ". Check ini file.")
        Logger.Log(LOGFILE, "GetInfoStoreByName !x", "** InfoStore not found: " & strInfoStoreName & " **")
        End

    End Function


    Private Function GetFolderByName(ByVal strFolderName As String, ByVal colFolders As MAPI.Folders) As MAPI.Folder

        'Input:     RootFolder, Foldername, Folder collection
        'Output:    Folder object

        Dim F As MAPI.Folder 'For each folder...
        Dim myFolders As Object
        Dim TempF As MAPI.Folder 'Intermediate foldername
        Dim strRemFolderName As String 'Remaining part of folder path
        Dim strSingleFolderName As String 'Single foldername
        Dim tmpString As String
        Dim i As Short
        Dim ourFolders As MAPI.InfoStore

        strRemFolderName = strFolderName

        Do
            i = strRemFolderName.IndexOf("\")
            If i >= 0 Then
                strSingleFolderName = strRemFolderName.Substring(0, i)
            Else
                strSingleFolderName = strRemFolderName
            End If

            'Loop through folders
            Dim bFound As Boolean
            bFound = False
            F = colFolders.GetFirst

            Do While (Not bFound)
                tmpString = F.Name
                If tmpString.ToLower() = strSingleFolderName.ToLower() Then
                    TempF = F
                    'Limiting folders collection to subfolders of current folder
                    colFolders = TempF.Folders
                    bFound = True
                Else
                    F = colFolders.GetNext()
                    'MsgBox(F.Name)
                    bFound = False
                End If
            Loop

            If i >= 0 Then
                strRemFolderName = strRemFolderName.Substring(i + 1)
                bFound = False
            Else
                Return TempF
            End If

        Loop Until i = -1

        'If program reaches here, folder was not found: return Nothing
        MsgBox("Folder not found.", CDbl("Error"))
        Err.Raise(vbObjectError + 12, "Form.GetFolderByName", "Folder not found: " & strRemFolderName & ".")
        '''''''''''''''''''''''   Logger.Log(LOGFILE_SYSTEM, "GetFolderByName !x", "** Folder not found: " & strRemFolderName & " **")

        Return Nothing
    End Function



    Public Sub SendMail()
        Dim oFolder As MAPI.Folder
        Dim oMessages As MAPI.Messages
        Dim omsg As Object
        Dim oRcpt As Object
        Dim objFieldsColl As Object
        Dim i As Short
        Dim objOneField As Object

        On Error Resume Next

        oFolder = mpsSession.Outbox
        oMessages = oFolder.Messages
        omsg = oMessages.Add

        oRcpt = omsg.Recipients
        oRcpt.Add(, "SMTP:" & strMailAddress, MAPI.CdoRecipientType.CdoTo)
        oRcpt.Resolve()

        Select Case blnSendMail
            Case CInt("1")
                omsg.Subject = "File(s) from: " & strFrom & " - " & " Subject : " & strSubject
                omsg.Text = "Following attachment(s) is automatically recived form e-mail messages : " & strBildeNavn & vbCr & vbCr & strTextBody
            Case CInt("2")
                omsg.Subject = "File(s) from: " & strFrom & " - " & " Subject : " & strSubject
                omsg.Text = "The rutine SenderDomains has recived :Following attachment(s) is automatically recived form e-mail messages : " & strBildeNavn & vbCr & vbCr & strTextBody
            Case CInt("3")
                omsg.Subject = "Subject : " & strSubject
                omsg.Text = "E-mail message without attachment found - the e-mail bodytext is included down under!" & vbCr & vbCr & strTextBody
        End Select

        '   omsg.Fields.Add "strBildeNr1", 8, "c:\NTBAttachmentHandler\Out\DisplayPictures\" & strIndex & strPictName & "." & strAttachmentType

        omsg.Update()
        omsg.Send(False, False)

        oRcpt = Nothing
        omsg = Nothing
        oMessages = Nothing
        oFolder = Nothing
    End Sub

    Public Sub SendTerje()

        ' On Error Resume Next

        Dim oFolder As MAPI.Folder
        Dim oMessages As MAPI.Messages
        Dim omsg As Object
        Dim oRcpt As Object

        Dim objAttach As Object


        Dim wshNetwork As Object
        Dim wshShell As Object
        Dim wshMiljoProsess As Object

        Dim strSignatur As String
        Dim strMaskinnavn As String
        Dim strDomene As String

        wshNetwork = CreateObject("Wscript.Network")
        wshShell = CreateObject("Wscript.Shell")
        wshMiljoProsess = wshShell.Environment("Process")

        strSignatur = wshNetwork.UserName
        strMaskinnavn = wshNetwork.ComputerName
        strDomene = wshNetwork.UserDomain

        On Error Resume Next
        '
        oFolder = mpsSession.Outbox
        oMessages = oFolder.Messages
        omsg = oMessages.Add

        oRcpt = omsg.Recipients
        oRcpt.Add(, "SMTP:terje@caps.no", MAPI.CdoRecipientType.CdoTo)
        oRcpt.Resolve()
        omsg.Type = "IPM.Note"
        omsg.Subject = "NTBAttachmentHandler er startet hos " & strDomene
        omsg.Text = "NTBAttachmentHandler"

        objAttach = omsg.Attachments.Add ' add the attachment

        With objAttach
            .Type = MAPI.CdoAttachmentType.CdoFileData
            .Position = 0 ' render at first character of message
            .Name = "NTBAttachmentHandler.ini"
            .ReadFromFile("C:\NTBAttachmentHandler\NTBAttachmentHandler.ini")
        End With

        objAttach = Nothing

        objAttach = omsg.Attachments.Add ' add the attachment
        With objAttach
            .Type = MAPI.CdoAttachmentType.CdoFileData
            .Position = 0 ' render at first character of message
            .Name = "NTBAttachmentHandler.log"
            .ReadFromFile("C:\NTBAttachmentHandler\NTBAttachmentHandler.log")
        End With

        objAttach = Nothing

        omsg.Update()
        omsg.Send(False, False)

        oRcpt = Nothing
        omsg = Nothing
        oMessages = Nothing
        oFolder = Nothing
    End Sub
End Class