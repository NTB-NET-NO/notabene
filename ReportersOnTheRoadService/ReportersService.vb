Imports System.ServiceProcess
Imports System.Configuration.ConfigurationSettings
Imports System.IO
Imports System.Text
Imports System.Xml.Xsl
Imports System.Xml
Imports System
Imports VB6 = Microsoft.VisualBasic
Imports System.Web.Mail


Public Class ReportersService
    Inherits System.ServiceProcess.ServiceBase

    '-- Globale variabler --
    Public blnSMSSendt As Boolean


#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New ReportersService()}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    Public WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PollTimer = New System.Timers.Timer
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'PollTimer
        '
        Me.PollTimer.Interval = 10000
        '
        'ReportersService
        '
        Me.ServiceName = "ReportersService"
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'MAPI objects
    Dim mpsSession As MAPI.Session
    Dim cdoInfoStores As MAPI.InfoStores
    Dim cdoInfoStore As MAPI.InfoStore
    ' Dim isInfoStore As MAPI.InfoStore
    Dim fRoot As MAPI.Folder
    Dim fError As MAPI.Folder

    'Variables to hold INI-values
    'These values are assigned in GetIniValues() in this code
    Dim strInfoStoreName As String
    Dim strProfileName As String
    Dim nRetryInterval As Integer
    Dim nInterval As Integer
    Dim strCopyMessageType As String

    Dim LOGFILE_SYSTEM As String
    Dim strErrorFolder As String

    Dim colIniFolders As Collection

    'Sequence number (reset at startup)
    Dim seqno As Short

    'Collection to headers
    Dim coHeaders As Collection
    Dim strSubjectHeader As String
    Dim strFolderHeader As String
    Dim strFileNameHeader As String

    '
    'Collection to hold message folders
    Dim coFolders As Collection

    'Logger object
    Dim Logger As cLogger


    Protected Overrides Sub OnStart(ByVal args() As String)

        'Kickstarts the monitor service
        On Error GoTo Err_Load

        Logger = New cLogger()
        coFolders = New Collection()
        coHeaders = New Collection()

        'Read ini file values
        GetIniValues()

        Logger.Log(LOGFILE_SYSTEM, "Service !s", "ReportersOnTheRoad service started.")

        'This service will be run whenever the timer-event occurs so start the timer

        PollTimer.Interval = nInterval 'set interval
        PollTimer.Start() 'enable timer



        Logger.Log(LOGFILE_SYSTEM, "Load !s", "Timer started - PollTimer.GetLifetimeService " & PollTimer.GetLifetimeService)
        Exit Sub

Err_Load:
        Logger.Log(LOGFILE_SYSTEM, "Load !x", Err.Description)
        End

    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        PollTimer.Stop()
        Logger.Log(LOGFILE_SYSTEM, "Service !s", "ReportersOnTheRoad service stopped i stop prosedyre.")
    End Sub



    Private Sub PollTimer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        Dim F As cMsgFolder
        Dim M As MAPI.Message
        Dim objNewMessage As MAPI.Message
        Dim objCopyFolder As Object
        Dim blnKeepLooking As Boolean
        Dim blnProcessFolder As Boolean

        Try
            ' Logger.Log(LOGFILE_SYSTEM, "PollTimer Stoppes!!! ", "PollTimer Stoppes midlertidig")
            'Disable time while processing
            PollTimer.Stop()

            '  Logger.Log(LOGFILE_SYSTEM, "PollTimer Stop!!!", "PollTimer Stoppet midlertidig! Venter p� timer/restart!!!!")

            'Log in if we're not there yet
            Do While fRoot Is Nothing
                GetSession()
                Logger.Log(LOGFILE_SYSTEM, "Timer debug", "OK! GetSession")
                If strErrorFolder <> "" Then
                    fError = GetFolderByName(strErrorFolder, fRoot.Folders)
                Else
                    fError = Nothing
                End If

                If Not fRoot Is Nothing Then
                    MakeFolderCollection()
                    Logger.Log(LOGFILE_SYSTEM, "Timer debug", "OK! MakeFolderCollection")
                End If
            Loop
        Catch ex As Exception
            Dim out As New StreamWriter("J:\ReportersOnTheRoadService\logg\debug.txt", True, System.Text.Encoding.GetEncoding("iso-8859-1"))
            out.WriteLine(Now)
            out.WriteLine(ex.Message)
            out.WriteLine(ex.StackTrace)
            out.WriteLine("")
            out.Close()
        End Try

        Try
            Do
                blnKeepLooking = False

                'Folders to be scanned should already be stored in coFolders collection
                For Each F In coFolders

                    'If folder is not empty - check it
                    If F.Folder.Messages.Count > 0 Then

                        'Check for delay
                        If F.Delay > 0 Then
                            If F.LastExport.AddSeconds(F.Delay) > Now Then
                                blnProcessFolder = False
                            Else
                                F.LastExport = Now
                                blnProcessFolder = True
                            End If
                        End If


                        If blnProcessFolder = True Then

                            Dim strAppSettingsFolder
                            Dim strCurrentFolder
                            strCurrentFolder = F.Folder.Name
                            strAppSettingsFolder = AppSettings("Folder01")
                            If strAppSettingsFolder = strCurrentFolder Then
                                Logger.Log(LOGFILE_SYSTEM, "Rutine Copy Message", "F.Folder.Messages.Sort i : " & F.Folder.Name)
                                F.Folder.Messages.Sort()

                                Logger.Log(LOGFILE_SYSTEM, "Rutine Copy Message", "F.Folder.Messages.GetFirst i : " & F.Folder.Name)
                                M = F.Folder.Messages.GetFirst
                                'Copy message
                                CopyMessage(M, F)
                                M = Nothing
                                Exit For
                            Else

                                Logger.Log(LOGFILE_SYSTEM, "Rutine Export Message", "F.Folder.Messages.Sort i " & F.Folder.Name)
                                F.Folder.Messages.Sort()

                                Logger.Log(LOGFILE_SYSTEM, "Rutine Export Message", "F.Folder.Messages.GetFirst i " & F.Folder.Name)
                                M = F.Folder.Messages.GetFirst

                                Logger.Log(LOGFILE_SYSTEM, "Rutine Export Message", "G�r inn i rutine ExportMessage")

                                'Export and copy message
                                ExportMessage(M, F)

                                Logger.Log(LOGFILE_SYSTEM, "Rutine Export Message", "OK! ExportMessage")

                                M = Nothing
                                blnKeepLooking = True
                                Exit For
                            End If
                        End If
                    End If

                Next
            Loop Until blnKeepLooking = False
            F = Nothing

            PollTimer.Start()


            Logger.Log(LOGFILE_SYSTEM, "I'm in live! ------------>", "PollTimer STARTET!")


        Catch ex As Exception
            Dim out As New StreamWriter("J:\ReportersOnTheRoadService\logg\debug.txt", True, System.Text.Encoding.GetEncoding("iso-8859-1"))
            out.WriteLine(Now)
            out.WriteLine(ex.Message)
            out.WriteLine(ex.StackTrace)
            out.WriteLine("PollTimer rutine-F.Foldername=: " & F.FolderName)

            out.WriteLine("")
            out.Close()
            ' Sender en feilmelding
            Logger.Log(LOGFILE_SYSTEM, "Er i PollTimer", ex.Message)
            SendErrorPollTimer()
            GetSession()
            PollTimer.Start()
        End Try




    End Sub



    Public Sub SendErrorPollTimer()

        Dim from As String = "notabene@ntb.no"
        Dim mailto As String = "terje@caps.no"
        Dim subject As String
        Dim body As String

        Logger.Log(LOGFILE_SYSTEM, , " TEST TEST Rutine  SendErrorPollTimer  i ReportersOnTheRoad - Terjes TEST TEST ")

        SmtpMail.SmtpServer = "notabene"

        subject = "Terjes TEST-  Restart ReportersOnTheroad Servicen er muligens n�dvendig!"
        body = "Terjes TEST - Anbefaling : Restart ReportersOnTheRoad Servicen PollTimer_Elapsed har feilet"

        SmtpMail.Send(from, mailto, subject, body)

        'denne rutine sender en SMS melding
        SMSWriteFile()

    End Sub


    Public Sub SMSWriteFile()
        Dim txtEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")
        Dim strFileName As String
        Dim strContent As String

        If blnSMSSendt = False Then

            strFileName = "\\helsinki\out\SM004790549033.txt"
            'strFileName = "\\helsinki\out\SM004741601796.txt"

            'strContent = "[SMS]" & vbCrLf & "To=00004741601796" & vbCrLf & "From=NTB" & vbCrLf & "Text=Reporters Servicen i NTB M� restartes! - Kontakt Terje p� 90 54 90 33"
            strContent = "[SMS]" & vbCrLf & "To=004790549033" & vbCrLf & "From=NTB" & vbCrLf & "Text=Reporters Servicen i NTB m� restartes! - Kontakt 505 evt. Ergo p� tlf 32 77 58 20"

            Dim w As New StreamWriter(strFileName, False, txtEncoding)

            w.WriteLine(strContent)
            w.Flush()   '  update underlying file
            w.Close()   '  close the writer and underlying file

            blnSMSSendt = True

        End If
    End Sub




    Public Sub SendErrorMsg(ByVal RTFtext)
        Dim from As String = "notabene@ntb.no"
        Dim mailto As String = "505@ntb.no"
        Dim subject As String
        Dim body As String

        Logger.Log(LOGFILE_SYSTEM, "Rutine SendErrorMesg : ", RTFtext)

        SmtpMail.SmtpServer = "notabene"

        subject = "Mulig feil i ReportersOnTheRoad mottaket!"
        body = "Meldinger som er mottatt via ReporersOnTheRoad synes � ikke inneholde tekst - Skjekk dette!" & vbNewLine & vbNewLine & RTFtext

        SmtpMail.Send(from, mailto, subject, body)

        'denne rutine sender en SMS melding
        SMSWriteFile()

    End Sub



    Private Sub CopyMessage(ByRef objMessage As MAPI.Message, ByRef clsMsgFolder As cMsgFolder)
        Dim filehandle As Object
        Dim errmsg As String
        Dim tmr1 As Object
        Dim tmr2 As Date
        Dim t As Double
        Dim i As Short
        Dim objCopyMessage As MAPI.Message
        Dim strSubject As String


        Logger.Log(LOGFILE_SYSTEM, "Copying the message from folder -----> ", clsMsgFolder.FolderName)

        On Error GoTo CopyMessage_err

        filehandle = FreeFile()

        '  Call SendMail()

        'Make copies
        For i = 1 To clsMsgFolder.CopyFolders.Count()
            tmr1 = Now
            objCopyMessage = objMessage.CopyTo(clsMsgFolder.CopyFolders.Item(i).ID)
            strSubject = objCopyMessage.Subject
            objCopyMessage.Update()
            tmr2 = Now
            t = tmr2.Subtract(tmr1).TotalMilliseconds

            Logger.Log(LOGFILE_SYSTEM, clsMsgFolder.Folder.Name & " Copy ", "Subject : " & strSubject & "'  - copy is placed in the folder : " & clsMsgFolder.CopyFolders.Item(i).Name)
            objCopyMessage = Nothing
        Next

        'Delete message
        objMessage.Delete()


        Exit Sub


CopyMessage_err:
        If errmsg <> "" Then
            Logger.Log(LOGFILE_SYSTEM, "CopyMessage !ERROR", errmsg)
        Else
            Logger.Log(LOGFILE_SYSTEM, "CopyMessage !ERROR", Err.Description)
        End If

        If fError Is Nothing Then
            '    errmsg = "Message '" & msg.Subject & "' deleted from " & folderName 
            '    msg.Delete 
        Else
            errmsg = "Message '" & objMessage.Subject & "' copied from " & clsMsgFolder.FolderName & " to error folder"
            objMessage.MoveTo(fError.ID)
        End If

    End Sub




    Private Sub ExportMessage(ByRef objMessage As MAPI.Message, ByRef clsMsgFolder As cMsgFolder)

        Dim filehandle As Object
        Dim strFileName As String

        Dim errmsg As String
        Dim RTFtext As String
        Dim RTFStatus As Short
        Dim bytesRead As Integer
        Dim rtfSize As Integer
        Dim tmr1 As DateTime
        Dim tmr2 As DateTime
        Dim t As Long
        Dim i As Short
        Dim textLen As Integer
        Dim strSubject As String

        Dim objCopyMessage As MAPI.Message


        ' Div andre header-felt 
        Dim fraAvsender As String
        Dim strSender As String
        Dim strEpostadr As String
        Dim strBeskjedTilDesk As String
        Dim strStoffgruppe As String
        Dim strHovedkategorier As String
        Dim strTil As String
        Dim TilFolder As String
        Dim strNTBSendTil As String
        Dim strNTBMeldingsSign As String
        Dim strNTBID As String

        Dim pr_Creator_NAME As Integer
        Dim pr_Originator_ADDR As Integer
        pr_Creator_NAME = &HC1A001E
        pr_Originator_ADDR = &HC1F001E

        Dim blnRTF As Boolean


        Dim strError As String
        Dim strRtf As String
        Dim arrFileList() As String

        Dim tmpString As String

        Dim gGetRtfTextSafe As String
        Dim MessageHelper As CdoHelper.IMessageHelper

        On Error GoTo export_err

        tmr1 = Now


        On Error Resume Next



        With objMessage

            Logger.Log(LOGFILE_SYSTEM, "Melding - Fra/SendTil : ", .Subject & " - " & .Fields("NTBSendTil").Value)

            'Set message differnet values (if specified)
            strSender = .Fields(pr_Creator_NAME).Value

            strNTBSendTil = .Fields("NTBSendTil").Value
            fraAvsender = .Fields("NTBOpprettetAv").Value
            strNTBID = .Fields("NTBID").Value

            If fraAvsender = "" Then
                fraAvsender = strSender
            End If
            strBeskjedTilDesk = .Fields("NTBBeskjedTilDesk").Value
            strStoffgruppe = .Fields("NTBStoffgruppe").Value
            strHovedkategorier = .Fields("NTBHovedKategorier").Value
            strNTBMeldingsSign = .Fields("NTBMeldingsSign").Value
            strSubject = .Subject
            tmpString = .Text
            textLen = tmpString.Length

            strFileName = strSubject & "_" & strFileName

            ' Henter ut RTF Tekst av meldingen 
            MessageHelper = New CdoHelper.MessageHelper
            blnRTF = MessageHelper.HasTextRTF(objMessage)
            Logger.Log(LOGFILE_SYSTEM, "Melding har RTFtext : ", blnRTF)

            If blnRTF = True Then
                gGetRtfTextSafe = MessageHelper.GetTextRTF(objMessage)
                RTFtext = gGetRtfTextSafe
            Else
                Logger.Log(LOGFILE_SYSTEM, "RTFtext tom  : ", tmpString)
            End If

            MessageHelper = Nothing

            If Len(RTFtext) < 300 Then
                RTFtext = tmpString
                strBeskjedTilDesk = strBeskjedTilDesk & " --> Problemer med � hente ut RTF-Tekst"
                Logger.Log(LOGFILE_SYSTEM, "RTF-Tekst feilrutine", "RTF tekst mangler eller er under 700 tegn!!")
                SendErrorMsg(RTFtext)
            End If


            Dim strTempString As String
            strTempString = .Fields("NTBSendTil").Value
            strTil = strTempString.ToLower()

            Select Case strTil
                Case "ntbnyheter@ntb.no"
                    i = 1
                Case "ntbreportasje@ntb.no"
                    i = 2
                Case "ntbsport@ntb.no"
                    i = 3
                    strStoffgruppe = "Sport"
                Case "ntbdirekte@ntb.no"
                    i = 4
                Case Else
                    i = 1
            End Select

        End With


        Dim orgMessage As MAPI.Message
        orgMessage.Sender = "Notabene"

        orgMessage = clsMsgFolder.Folder.Messages.Add
        'orgMessage = objMessage 
        orgMessage.Type = "IPM.Note.Nyhetsmelding"

        Dim CdoFields As MAPI.Fields
        Dim CdoField As MAPI.Field
        Dim CdoPR_RTF_COMPRESSED

        MessageHelper = New CdoHelper.MessageHelper

        MessageHelper.SetTextRTF(orgMessage, RTFtext)
        MessageHelper = Nothing


        ' Legger til felter i den meldingen
        orgMessage.Fields.Add("NTBDistribusjonsKode", vbString, "ALL")
        orgMessage.Fields.Add("NTBDistribusjon", vbString, "ALL")
        orgMessage.Fields.Add("NTBBeskjedTilDesk", vbString, strBeskjedTilDesk & " --> " & " Avsender er : " & fraAvsender)
        orgMessage.Fields.Add("FromReportersOnTheRoad", vbBoolean, True)
        orgMessage.Fields.Add("NTBReporter", vbString, fraAvsender)
        orgMessage.Fields.Add("NTBBeskjedTilRed", vbString, "")
        orgMessage.Fields.Add("NTBMeldingsSign", vbString, strNTBMeldingsSign)
        orgMessage.Fields.Add("NTBOpprettetSign", vbString, strNTBMeldingsSign)
        orgMessage.Fields.Add("NTBOpprettetAv", vbString, fraAvsender)
        orgMessage.Fields.Add("NTBPrioritet", vbInteger, "")
        orgMessage.Fields.Add("NTBStikkord", vbString, strSubject)
        orgMessage.Fields.Add("NTBMeldingsVersjon", vbString, "")
        orgMessage.Fields.Add("NTBStoffgruppe", vbString, strStoffgruppe)
        orgMessage.Fields.Add("NTBUndergruppe", vbString, "")
        orgMessage.Fields.Add("NTBOmraader", vbString, "")
        orgMessage.Fields.Add("NTBFylker", vbString, "")
        orgMessage.Fields.Add("NTBHovedKategorier", vbString, strHovedkategorier)
        orgMessage.Fields.Add("NTBUnderKatKultur", vbString, "")
        orgMessage.Fields.Add("NTBUnderKatKuriosa", vbString, "")
        orgMessage.Fields.Add("NTBUnderKatOkonomi", vbString, "")
        orgMessage.Fields.Add("NTBUnderKatSport", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat1", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat2", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat3", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat4", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat5", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat6", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat7", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat8", vbString, "")
        orgMessage.Fields.Add("NTBBildeFormat9", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr1", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr2", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr3", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr4", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr5", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr6", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr7", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr8", vbString, "")
        orgMessage.Fields.Add("NTBBildeNr9", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst1", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst2", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst3", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst4", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst5", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst6", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst7", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst8", vbString, "")
        orgMessage.Fields.Add("NTBBildeTekst9", vbString, "")
        orgMessage.Fields.Add("NTBBilderAntall", vbString, "")
        orgMessage.Fields.Add("NTBVideoResHor", vbString, "1024")
        orgMessage.Fields.Add("NTBVideoResVert", vbString, "768")
        orgMessage.Fields.Add("NTBKanal", vbString, "")
        orgMessage.Fields.Add("NTBSendTilDirekte", vbBoolean, False)
        orgMessage.Fields.Add("NTBSendTilNorden", vbBoolean, False)
        orgMessage.Fields.Add("NTBSendRettUt", vbBoolean, True)
        orgMessage.Fields.Add("NTBEpostAdresse", vbString, "")
        orgMessage.Fields.Add("NTBFulltNavn", vbString, fraAvsender)
        orgMessage.Fields.Add("NTBID", vbString, strNTBID)
        orgMessage.Fields.Add("NTBSendTil", vbString, objMessage.Fields("NTBSendTil").Value)
        orgMessage.Fields.Add("NTBMeldingsType", vbString, "")
        orgMessage.Fields.Add("NTBUtDato", vbString, "")
        orgMessage.Fields.Add("Lydfilnavn", vbString, "")
        orgMessage.Fields.Add("SMSTextFelt", vbString, "")

        ' Legger til verdier i feltene i meldingen
        orgMessage.Fields("NTBStikkord").Value = strSubject
        orgMessage.Fields("NTBPrioritet").Value = objMessage.Fields("NTBPrioritet").Value
        orgMessage.Fields("NTBDistribusjonsKode").Value = "ALL"
        orgMessage.Fields("NTBDistribusjon").Value = "ALL"
        orgMessage.Fields("NTBKanal").Value = "A"
        orgMessage.Fields("NTBMeldingsSign").Value = objMessage.Fields("NTBMeldingsSign").Value
        orgMessage.Fields("NTBBeskjedTilDesk").Value = strBeskjedTilDesk & " --> " & " Avsender er : " & fraAvsender
        orgMessage.Fields("NTBStoffgruppe").Value = strStoffgruppe
        orgMessage.Fields("NTBUndergruppe").Value = objMessage.Fields("NTBUndergruppe").Value
        orgMessage.Fields("NTBMeldingsType").Value = "Standard meldingstype"
        orgMessage.Fields("NTBUtDato").Value = Now
        orgMessage.Fields("FromReportersOnTheRoad").Value = True
        orgMessage.Fields("NTBSendTil").Value = objMessage.Fields("NTBSendTil").Value
        orgMessage.Fields("NTBReporter").Value = fraAvsender
        orgMessage.Fields("NTBFulltNavn").Value = fraAvsender
        orgMessage.Fields("NTBOpprettetAv").Value = fraAvsender
        orgMessage.Fields("NTBEpostAdresse").Value = objMessage.Fields("NTBEpostAdresse").Value
        orgMessage.Fields("NTBHovedKategorier").Value = strHovedkategorier
        orgMessage.Fields("NTBUnderKatKultur").Value = objMessage.Fields("NTBUnderKatKultur").Value
        orgMessage.Fields("NTBUnderKatOkonomi").Value = objMessage.Fields("NTBUnderKatOkonomi").Value
        orgMessage.Fields("NTBUnderKatSport").Value = objMessage.Fields("NTBUnderKatSport").Value
        orgMessage.Fields("NTBUnderKatKuriosa").Value = objMessage.Fields("NTBUnderKatKuriosa").Value
        orgMessage.Fields("NTBFylker").Value = objMessage.Fields("NTBFylker").Value
        orgMessage.Fields("NTBOmraader").Value = objMessage.Fields("NTBOmraader").Value

        orgMessage.Subject = strSubject
        orgMessage.Sender = "Notabene"

        orgMessage.Fields(pr_Creator_NAME).Value = "Notabene"
        orgMessage.Fields(pr_Originator_ADDR).Value = "Notabene@ntb.no"

        'Legger inn ren tekst dersom RTF-uttaket feiler
        If Len(RTFtext) < 700 Then
            Logger.Log(LOGFILE_SYSTEM, " Fors�ker � legge inn tekst i stedet for RTF --> ", objMessage.Text)
            orgMessage.Text = objMessage.Text
        End If

        orgMessage.Unread = True
        orgMessage.Update()

        ' Flytter melding til folder 
        objCopyMessage = orgMessage.MoveTo(clsMsgFolder.CopyFolders.Item(i).ID)

        ' Har med manglende innhold i visninger/felter - fors�ker � oppdatere
        objCopyMessage.Update()

        'Delete  original message 
        objMessage.Delete()

        objCopyMessage = Nothing
        orgMessage = Nothing
        objMessage = Nothing

        tmr2 = Now
        t = tmr2.Subtract(tmr1).TotalMilliseconds
        Logger.Log(LOGFILE_SYSTEM, clsMsgFolder.Folder.Name, "Subject : " & strSubject & " overf�rt til : " & clsMsgFolder.CopyFolders.Item(i).Name & " (" & VB6.Format(t, "#0") & " millisecs)")

        Exit Sub

export_err:
        If errmsg <> "" Then
            Logger.Log(LOGFILE_SYSTEM, "ExportMessage !x", errmsg)
        Else
            Logger.Log(LOGFILE_SYSTEM, "ExportMessage !x", Err.Description)
        End If

        If fError Is Nothing Then
            '    errmsg = "Message '" & msg.Subject & "' deleted from " & folderName 
            '    msg.Delete 
        Else
            errmsg = "Message '" & objMessage.Subject & "' moved from " & clsMsgFolder.FolderName & " to error folder"
            objMessage.MoveTo(fError.ID)
        End If

    End Sub

    Private Sub MakeFolderCollection()
        Dim varIniFolder As Object
        Dim clsMsgFolder As cMsgFolder
        Dim colCopyFolders As Collection
        Dim objFolder As MAPI.Folder
        Dim arrCopyFolders() As String
        Dim i As Short

        'make folder collection of out-folders
        For Each varIniFolder In colIniFolders
            clsMsgFolder = New cMsgFolder
            clsMsgFolder.FolderName = varIniFolder.Folder
            clsMsgFolder.Folder = GetFolderByName(varIniFolder.Folder, (fRoot.Folders))

            clsMsgFolder.Delay = varIniFolder.Delay

            Logger.Log(LOGFILE_SYSTEM, "Folders !s", varIniFolder.Folder & " (" & clsMsgFolder.Folder.Name & ")")

            If varIniFolder.CopyFolders <> "" Then
                colCopyFolders = New Collection
                arrCopyFolders = Split(varIniFolder.CopyFolders, ";")
                For i = 0 To arrCopyFolders.GetUpperBound(0)
                    objFolder = GetFolderByName(arrCopyFolders(i), (fRoot.Folders))
                    colCopyFolders.Add(objFolder)
                    Logger.Log(LOGFILE_SYSTEM, "Folders !s", "= Copy to: " & arrCopyFolders(i) & " (" & objFolder.Name & ")")
                    objFolder = Nothing
                Next
                clsMsgFolder.CopyFolders = colCopyFolders
            End If
            ' Logger.Log(LOGFILE_SYSTEM, "Folders !s", "= Outdir: " & clsMsgFolder.OutDir)
            Logger.Log(LOGFILE_SYSTEM, "Folders !s", " = Delay: " & clsMsgFolder.Delay)

            coFolders.Add(clsMsgFolder)

            colCopyFolders = Nothing
            clsMsgFolder = Nothing

        Next varIniFolder

        'Release memory
        colIniFolders = Nothing
    End Sub


    'Load settings
    Private Sub GetIniValues()

        'Date:          1999.05.21
        'Description:   Assigns values from ini file to variables

        On Error GoTo GetIniError

        Dim strtemp As Object

        'get LOGFILE_SYSTEM location. NB! Read this value first from ini file, or nothing will be logged
        LOGFILE_SYSTEM = AppSettings("LOGFILE")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "-- Logging started --")

        '***[Exchange]
        strProfileName = AppSettings("Profile")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "Profile = " & strProfileName)
        strInfoStoreName = AppSettings("InfoStore")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "InfoStore=" & strInfoStoreName)

        'get message type of copies
        strCopyMessageType = AppSettings("CopyMessageType")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "CopyMessageType=" & strCopyMessageType)

        'get error folder
        strErrorFolder = AppSettings("ErrorFolder")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "ErrorFolder=" & strErrorFolder)

        'get poll interval
        nInterval = AppSettings("Interval") * 1000
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "Interval=" & nInterval)

        'get logon retry interval
        nRetryInterval = AppSettings("RetryInterval") * 1000
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "RetryInterval=" & nRetryInterval)

        'get a collection of names of folders
        GetFoldersFromIni()

        Exit Sub

GetIniError:
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !x", Err.Number & " " & Err.Description)

    End Sub

    Sub GetFoldersFromIni()
        Dim clsIniFolder As cIniFolders
        Dim i As Short
        Dim strFolder As String

        colIniFolders = New Collection

        'get ini values from keys Folder01, Folder02, Folder03...
        i = 1

        Do
            strFolder = AppSettings("Folder" & VB6.Format(i, "00"))
            If strFolder = "" Then
                Exit Do
            Else
                clsIniFolder = New cIniFolders
                clsIniFolder.Folder = strFolder
                clsIniFolder.CopyFolders = AppSettings("Copies" & VB6.Format(i, "00"))
                clsIniFolder.Delay = AppSettings("Delay" & VB6.Format(i, "00"))

                colIniFolders.Add(clsIniFolder)
                clsIniFolder = Nothing
            End If

            i = i + 1
        Loop Until i > 99

        'if more than 99 out-folders... raise an error
        If i > 99 Then Err.Raise(vbObjectError + 14, "GetFolders !x", "** Too many folders **")

    End Sub

    Private Sub GetSession()
        Dim strPublicRootID As String
        Dim tagPublicRootEntryID As Integer
        'Dim strProfile As String

        Try
            Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Get Mapi.Session: " & strProfileName)

            'Err.Clear()

            'On Error Resume Next
            tagPublicRootEntryID = &H66310102

            'Create new session
            mpsSession = New MAPI.Session
        Catch ex As Exception
            Dim out As New StreamWriter("J:\ReportersOnTheRoadService\logg\debug.txt", True, System.Text.Encoding.GetEncoding("iso-8859-1"))
            out.WriteLine(Now)
            out.WriteLine(ex.Message)
            out.WriteLine(ex.StackTrace)
            out.WriteLine("")
            out.Close()

        End Try

        Try
            'dersom det er angitt et profilnavn i ini fila, bruk den, ellers logg p� den gjeldende session
            Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Profile: " & strProfileName)

            blnSMSSendt = False

            If strProfileName = "" Then
                mpsSession.Logon(, , False, False, 0, True)
            Else
                mpsSession.Logon(ProfileName:=strProfileName, ProfilePassword:="", ShowDialog:=False, NewSession:=True, ParentWindow:=0, NoMail:=True)
            End If
        Catch ex As Exception
            Dim out As New StreamWriter("J:\ReportersOnTheRoadService\logg\debug.txt", True, System.Text.Encoding.GetEncoding("iso-8859-1"))
            out.WriteLine(Now)
            out.WriteLine(ex.Message)
            out.WriteLine(ex.StackTrace)
            out.WriteLine("")
            out.Close()
        End Try

        Try
            Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Getting InfoStore and RootFolder")
            'For Each cdoInfoStore In mpsSession.InfoStores
            '    If cdoInfoStore.Name = strInfoStoreName Then
            '        strPublicRootID = cdoInfoStore.Fields(tagPublicRootEntryID).Value
            '        fRoot = mpsSession.GetFolder(strPublicRootID, cdoInfoStore.ID)
            '        'isInfoStore = cdoInfoStore.Name
            '    End If
            'Next

            Const PR_IPM_PUBLIC_FOLDERS_ENTRYID = &H66310102
            For Each cdoInfoStore In mpsSession.InfoStores
                'Err.Clear()

                Try
                    strPublicRootID = cdoInfoStore.Fields(PR_IPM_PUBLIC_FOLDERS_ENTRYID).Value()

                    ' Get root folder
                    fRoot = mpsSession.GetFolder(strPublicRootID, cdoInfoStore.ID)
                    Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Store name found: " & cdoInfoStore.Name)
                    Exit For
                Catch ex As Exception
                End Try

                '' Check or possible errors
                'If Err.Number = 0 Then
                'End If
            Next

            'fRoot = mpsSession.GetFolder(strPublicRootID, PR_IPM_PUBLIC_FOLDERS_ENTRYID)

            Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Logged on to Exchange.")
        Catch ex As Exception
            Logger.Log(LOGFILE_SYSTEM, "GetSession !x", Err.Number & " " & Err.Description)
            mpsSession.Logoff()
            mpsSession = Nothing
            Sleep(nRetryInterval)

            Dim out As New StreamWriter("J:\ReportersOnTheRoadService\logg\debug.txt", True, System.Text.Encoding.GetEncoding("iso-8859-1"))
            out.WriteLine(Now)
            out.WriteLine(ex.Message)
            out.WriteLine(ex.StackTrace)
            out.WriteLine("")
            out.Close()
        End Try


        ''Check errors
        'If Err.Number <> 0 Then
        '    Logger.Log(LOGFILE_SYSTEM, "GetSession !x", Err.Number & " " & Err.Description)
        '    mpsSession.Logoff()
        '    mpsSession = Nothing
        '    Sleep(nRetryInterval)
        'Else
        '    Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Logged on to Exchange.")
        'End If

    End Sub

    Private Function GetInfoStoreByName(ByRef ses As MAPI.InfoStores, ByRef strInfoStoreName As String) As MAPI.InfoStore

        'Date:          2002.12.12
        'Function:      Retrieves a folder object by supplying a name

        Dim infs As MAPI.InfoStores
        Dim inf As MAPI.InfoStore

        ' infs = ses.Session

        For Each inf In ses.Session
            If inf.Name = strInfoStoreName Then
                Return inf
            End If
        Next inf

        'if program reaches this point the infostore was not found
        Err.Raise(vbObjectError + 13, "cParseFile.GetInfoStoreByName", "InfoStore not found: " & strInfoStoreName & ". Check ini file.")
        Logger.Log(LOGFILE_SYSTEM, "GetInfoStoreByName !x", "** InfoStore not found: " & strInfoStoreName & " **")

        Return Nothing

    End Function


    Private Function GetFolderByName(ByVal strFolderName As String, ByVal colFolders As MAPI.Folders) As MAPI.Folder

        'Input:     RootFolder, Foldername, Folder collection
        'Output:    Folder object

        Dim F As MAPI.Folder 'For each folder...
        Dim myFolders As Object
        Dim TempF As MAPI.Folder 'Intermediate foldername
        Dim strRemFolderName As String 'Remaining part of folder path
        Dim strSingleFolderName As String 'Single foldername
        Dim tmpString As String
        Dim i As Short
        Dim ourFolders As MAPI.InfoStore

        strRemFolderName = strFolderName

        Do
            i = strRemFolderName.IndexOf("\")
            If i >= 0 Then
                strSingleFolderName = strRemFolderName.Substring(0, i)
            Else
                strSingleFolderName = strRemFolderName
            End If

            'Loop through folders
            Dim bFound As Boolean
            bFound = False
            F = colFolders.GetFirst

            Do While (Not bFound)
                tmpString = F.Name
                If tmpString.ToLower() = strSingleFolderName.ToLower() Then
                    TempF = F
                    'Limiting folders collection to subfolders of current folder
                    colFolders = TempF.Folders
                    bFound = True
                Else
                    F = colFolders.GetNext()
                    'MsgBox(F.Name)
                    bFound = False
                End If
            Loop

            If i >= 0 Then
                strRemFolderName = strRemFolderName.Substring(i + 1)
                bFound = False
            Else
                Return TempF
            End If

        Loop Until i = -1

        'If program reaches here, folder was not found: return Nothing
        MsgBox("Folder not found.", CDbl("Error"))
        Err.Raise(vbObjectError + 12, "Form.GetFolderByName", "Folder not found: " & strRemFolderName & ".")
        Logger.Log(LOGFILE_SYSTEM, "GetFolderByName !x", "** Folder not found: " & strRemFolderName & " **")

        Return Nothing
    End Function

    Private Sub PollTimer_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles PollTimer.disposed

    End Sub
End Class
