Option Strict Off
Option Explicit On
Friend Class cHeaderField
	'Name:          cHeaderField
	'Date:          2000.02.01
	'Author:        Jens Erik Taasen
	'Description:   Class holding Fingerpost headers for the
	'               output file with corresponding fields
	'               in the Exchange form.
	
	
	'local vars
	Private mvarFipHeader As String
	Private mvarFormField As String
	Private mvarDefaultValue As String
	
	
	Public Property FipHeader() As String
		Get
			FipHeader = mvarFipHeader
		End Get
		Set(ByVal Value As String)
			mvarFipHeader = Value
		End Set
	End Property
	
	
	Public Property FormField() As String
		Get
			FormField = mvarFormField
		End Get
		Set(ByVal Value As String)
			mvarFormField = Value
		End Set
	End Property
	
	
	Public Property DefaultValue() As String
		Get
			DefaultValue = mvarDefaultValue
		End Get
		Set(ByVal Value As String)
			mvarDefaultValue = Value
		End Set
	End Property
End Class