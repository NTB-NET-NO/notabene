Imports Word
Imports System.Text
Imports System.Xml.Xsl
Imports System.Xml
Imports System.IO
Imports System.Configuration.ConfigurationSettings

Public Class WordRtf2Html



    Dim objWord As Word.ApplicationClass
    Dim objWordDoc As Word.DocumentClass

    Public Sub New()
        objWord = New Word.ApplicationClass()
        objWordDoc = New Word.DocumentClass()
    End Sub

    Public Sub Quit()
        'objWord.Quit()
        'objWordDoc = Nothing
        'objWord = Nothing
    End Sub

    Protected Overrides Sub finalize()
        objWord.Quit()
        objWordDoc = Nothing
        objWord = Nothing
    End Sub

    Public Function ConvertDoc(ByVal strFileName As String, Optional ByVal strXmlHead As String = "", Optional ByVal bDeleteTempFiles As Boolean = False) As String
        Dim strTempFile As String = (strFileName & ".htm")
        Dim strHtml As String

        objWordDoc = objWord.Documents.Open(strFileName)

        'Lagrer som XML og lukker
        'strTempFile = Path.GetFileNameWithoutExtension(strTempFile)
        objWordDoc.SaveAs(strTempFile, Word.WdSaveFormat.wdFormatHTML)
        objWordDoc.Close()

        strHtml = LogFile.ReadFile(strTempFile)


        ' Bygg inn bDeleteTempFiles som en gobal variabel i App.Config -- Terje
        bDeleteTempFiles = AppSettings("DeleteTempFiles")
        If bDeleteTempFiles Then
            File.Delete(strFileName)
            File.Delete(strTempFile)
        End If

        Return XmlTidy(strHtml, strXmlHead)

    End Function

    Public Function XmlTidy(ByRef strHtml As String, Optional ByRef strXmlHead As String = "") As String
        Dim swXml As StringBuilder = New StringBuilder()
        Dim intStart, intSlutt, i As Integer
        Dim chrOne As Char
        Dim bTag, bAttr, bSetQuot, bEndTag, bEqual As Boolean
        Dim strTemp As String
        Dim intStartHtml As Integer

        'Dim strHtml As String = strHtmlInn.Replace("&nbsp;", "")

        intStartHtml = strHtml.IndexOf(">") + 1
        swXml.Append("<html>")

        For i = intStartHtml To strHtml.Length - 1 'swXml.Length
            chrOne = strHtml.Chars(i)

            If chrOne = "<" Then
                If LCase(strHtml.Substring(i + 1, 4)) = "meta" _
                Or LCase(strHtml.Substring(i + 1, 4)) = "link" _
                Or LCase(strHtml.Substring(i + 1, 2)) = "br" Then
                    bEndTag = True
                End If
                bAttr = False
                bTag = True
            End If

            If bTag Then
                If Not bAttr Then
                    ' Not inside atribute
                    If chrOne = "=" Then
                        If strHtml.Chars(i + 1) = """" Or strHtml.Chars(i + 1) = "'" Then
                            bSetQuot = False
                        Else
                            bSetQuot = True
                        End If
                        bAttr = True
                        bEqual = True
                    End If
                Else
                    ' If inside atribute 
                    If Not bEqual And (chrOne = """" Or chrOne = "'") Then
                        bSetQuot = False
                        bAttr = False

                    ElseIf chrOne = " " Then
                        If bSetQuot Then
                            swXml.Append("'")
                            bAttr = False
                            bSetQuot = False
                        End If
                    End If
                    If bEqual And bSetQuot Then
                        swXml.Append("'")
                    End If
                    bEqual = False

            End If

                If chrOne = " " Or chrOne = vbCr Then
                    If bSetQuot Then
                        swXml.Append("'")
                    End If
                    bSetQuot = False
                ElseIf chrOne = ">" Then
                    If bSetQuot Then
                        swXml.Append("'")
                    End If
                    If bEndTag Then
                        swXml.Append("/")
                    End If
                    bEndTag = False
                    bTag = False
                    bAttr = False
                    bSetQuot = False
                End If

                If bTag And chrOne = vbCr Then
                    swXml.Append(" ")
                ElseIf bTag And chrOne = vbLf Then
                    'Nothing to to, prevents breaks and double spacing inside tags
                Else
                    swXml.Append(chrOne)
                End If

            Else
                swXml.Append(chrOne)
                bEqual = False
            End If

            'for debug:
            strTemp = swXml.ToString
        Next
        swXml.Replace("&nbsp;", "")
        'swXml.Replace("&nbsp;", "#160;")
        swXml.Replace("<span style='mso-tab-count:1'>����������� </span><br>", "")
        swXml.Replace("link rel='File-List", "link rel='File-List'")
        swXml.Replace("link rel='File-List''", "link rel='File-List'")
        swXml.Replace("<!--[if gte mso 9]>", "")
        swXml.Replace("<!--[if gte mso 10]>", "")
        swXml.Replace("<![endif]-->", "")
        swXml.Replace("<![if !supportLineBreakNewLine]>", "")
        swXml.Replace("<![endif]>", "")
        swXml.Replace("<o:", "<")
        swXml.Replace("</o:", "</")
        swXml.Replace("<w:", "<")
        swXml.Replace("</w:", "</")

        'swXml.Replace("xmlns=""http://www.w3.org/TR/REC-html40""", "")
        swXml.Insert(0, "<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf)

        If strXmlHead <> "" Then
            Dim intPos As Integer
            strXmlHead = strXmlHead.Replace("<head>", "")
            strXmlHead = strXmlHead.Replace("</head>", "")
            intPos = swXml.ToString.IndexOf("<head>" & vbCrLf) + 8
            If intPos = -1 Then
                intPos = swXml.ToString.IndexOf("<head>") + 6
            End If
            If intPos > -1 Then
                swXml.Insert(intPos, strXmlHead)
            End If
        End If
        Return swXml.ToString
    End Function

    Public Sub LagNitfFile(ByRef strBodyXml As String, ByVal xslFile As String, ByVal strNitfFile As String)
        Dim xwNitf As XmlTextWriter = New XmlTextWriter(strNitfFile, System.Text.Encoding.GetEncoding("iso-8859-1"))
        'Dim xwNitf As TextWriter = New StreamWriter(strNitfFile, False, System.Text.Encoding.GetEncoding("iso-8859-1"))

        'Har problemer med formattering .NET XslTransform, b�r bruke MSXML4.dll i stedet!
        '.NET XslTransform gir ny linje f�r end-tag og klarer ikke timme tagger som: <p/> (skriver alltid <p></p>)

        Dim xmlBody As XmlDocument = New XmlDocument()

        Try
            xmlBody.LoadXml(strBodyXml)
        Catch
            LogFile.WriteFile(AppSettings("XmlTempDir") & "\" & strNitfFile & "-debug.xml", strBodyXml)
        End Try

        Dim xslNitf As XslTransform = New XslTransform()
        xwNitf.Formatting = Formatting.Indented
        xwNitf.IndentChar = Chr(9)
        xwNitf.Indentation = 0

        xwNitf.WriteStartDocument(True)

        xslNitf.Load(xslFile)
        xslNitf.Transform(xmlBody, Nothing, xwNitf)

        xwNitf.Flush()
        xwNitf.Close()

    End Sub

    Public Shared Function LoadNotabeneFieldsHeader(ByVal strMapFile As String, ByRef ObjMessage As Object, ByRef strError As String)
        Dim strFieldName As String
        Dim strXmlFieldName As String
        Dim strXmlFieldContent As String
        Dim strXmlHead As String

        Dim xmlMap As XmlDocument = New XmlDocument()
        xmlMap.Load(strMapFile)

        strError = ""
        strXmlHead = "<head>" & vbCrLf

        Dim dtNow As Date = Now

        AddXmlHeadNode(strXmlHead, "timestamp", Format(dtNow, "yyyy.MM.dd HH:mm:ss"))
        AddXmlHeadNode(strXmlHead, "NitfDate", Format(dtNow, "yyyyMMddTHHmmss"))
        AddXmlHeadNode(strXmlHead, "ntb-date", Format(dtNow, "dd.MM.yyyy HH:mm:ss"))

        Dim mapNode As XmlNode
        For Each mapNode In xmlMap.SelectNodes("/notabene/field")
            Try
                strFieldName = mapNode.Attributes("notabene").InnerText
                strXmlFieldName = mapNode.Attributes("xml").InnerText

                ' Henter feltverdier fra Notabene
                If strXmlFieldName = "NTBSendTil" Then
                    ' MsgBox(strXmlFieldContent)
                End If
                strXmlFieldContent = ObjMessage.Fields(strFieldName).Value
                strXmlFieldContent = strXmlFieldContent.Replace("""", "")

                ' Kode som legger til xml 
                AddXmlHeadNode(strXmlHead, strXmlFieldName, strXmlFieldContent)
            Catch e As Exception
                strError = e.Message & vbCrLf & e.StackTrace
            End Try
        Next

        strXmlHead &= "</head>" & vbCrLf
        Return strXmlHead
    End Function

    Private Shared Sub AddXmlHeadNode(ByRef strXmlHead As String, ByVal strName As String, ByVal strContent As String)
        strXmlHead &= "<meta name=""" & strName & """ content=""" & strContent & """/>" & vbCrLf
    End Sub

End Class
