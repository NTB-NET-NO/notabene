Option Strict Off
Option Explicit On
Module modMapiRTF
	Public Declare Function ReadRTF Lib "mapirtf.dll"  Alias "readrtf"(ByVal ProfileName As String, ByVal SrcMsgID As String, ByVal StoreID As String, ByRef MsgRTF As String, ByRef bytesRead As Integer) As Short
	
	Public Declare Function WriteRTF Lib "mapirtf.dll"  Alias "writertf"(ByVal ProfileName As String, ByVal MessageID As String, ByVal StoreID As String, ByVal cText As String) As Short
End Module