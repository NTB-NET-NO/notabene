Imports System.Text
Imports System.IO

Public Class LogFile

    Public Shared Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strFile As String = strLogPath & "\Error-" & Format(Now, "yyyy-MM-dd") & ".log"

        Dim w As StreamWriter = New StreamWriter(strFile, True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 

        Dim strLine As String
        On Error Resume Next
        strLine = "Error: " & Format(Now, "yyyy-MM-dd hh:mm:ss") & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.StackTrace & vbCrLf
        strLine &= "------------------------------------------------------------"

        'Console.WriteLine(strLine)

        w.WriteLine(strLine)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, "yyyy-MM-dd") & ".log"

        Dim w As StreamWriter = New StreamWriter(strFile, True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        Dim strLine As String

        strLine = Format(Now, "yyyy-MM-dd hh:mm:ss") & ": " & strMessage

        'Console.WriteLine(strLine)

        w.WriteLine(strLine)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, "yyyy-MM-dd") & ".log"

        Dim w As StreamWriter = New StreamWriter(strFile, True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        Dim strLine As String

        strLine = strMessage

        'Console.WriteLine(strLine)

        w.WriteLine(strLine)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteFile(ByRef strFileName As String, ByRef strContent As String)
        Dim w As StreamWriter = New StreamWriter(strFileName, True, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        w.WriteLine(strContent)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, ByVal append As Boolean)
        Dim w As StreamWriter = New StreamWriter(strFileName, append, Encoding.GetEncoding("iso-8859-1")) '  create a Char writer 
        w.WriteLine(strContent)
        w.Flush()                              '  update underlying file
        w.Close()                              '  close the writer and underlying file
    End Sub

    Public Shared Function ReadFile(ByVal strFileName As String) As String
        ' Simple File reader returns File as String
        Dim sr As StreamReader = New StreamReader(strFileName, Encoding.GetEncoding("iso-8859-1")) ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Shared Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

End Class

