<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="iso-8859-1" indent="yes" standalone="yes"/>
<xsl:template match="/html">
<nitf>
<header>
	<xsl:copy-of select="head"/>
</header>

<body>
<body.head>
<hedline>
	<hl1><xsl:value-of select="body/p[@class='_Infolinje']"/></hl1>
</hedline>
</body.head>
<body.content>
<xsl:apply-templates select="body/p"/>
</body.content>
</body>
</nitf>	
</xsl:template>


<xsl:template match="p[@class='_Br�dtekst']">
	<p style="main"><xsl:value-of select="."/></p>
</xsl:template>

<xsl:template match="p[@class='normal']">
	<p style="normal"><xsl:value-of select="."/></p>
</xsl:template>

<xsl:template match="p[@class='_Br�dtekst_Innrykk']">
	<p innrykk="true" style="indent"><xsl:value-of select="."/></p>
</xsl:template>

<xsl:template match="p[@class='_Ingress']">
	<p lede="true"><xsl:value-of select="."/></p>
</xsl:template>

<xsl:template match="p[@class='_Infolinje']">
<!--	<hl1><xsl:value-of select="."/></hl1> -->
</xsl:template>

<xsl:template match="p[@class='_Mellomtittel']">
	<hl2><xsl:value-of select="."/></hl2>
</xsl:template>


</xsl:stylesheet>