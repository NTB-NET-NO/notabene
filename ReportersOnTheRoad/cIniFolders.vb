Option Strict Off
Option Explicit On
Friend Class cIniFolders
	Private strFolder As String
	Private strOutDir As String
    Private strCopies As String
	Private intDelay As Short
	
	'Folder to watch
	
	Public Property Folder() As String
		Get
			Folder = strFolder
		End Get
		Set(ByVal Value As String)
			strFolder = Value
		End Set
	End Property
	
	'Alternative output directory
	
	Public Property OutDir() As String
		Get
			OutDir = strOutDir
		End Get
		Set(ByVal Value As String)
			strOutDir = Value
		End Set
	End Property
	
	'Copy folders (separated by ";")
	
	Public Property CopyFolders() As String
		Get
			CopyFolders = strCopies
		End Get
		Set(ByVal Value As String)
			strCopies = Value
		End Set
    End Property

    'Delay
    Public Property Delay() As Short
        Get
            Delay = intDelay
        End Get
        Set(ByVal Value As Short)
            intDelay = Value
        End Set
    End Property
End Class