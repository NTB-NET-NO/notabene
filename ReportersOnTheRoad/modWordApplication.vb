Option Strict Off
Option Explicit On
Module modWordApplication
	
	'-----------------------------------------------------------------
	'Author : John Henrik Andersen / NetCenter
	'Date   : 1998.08.04
	'
	'Description:
	'   tries to aquire a link to an existing Word application.
	'   if none is found, it creates a new instance of Word.
	'-----------------------------------------------------------------
	Public Function gGetWordApplication() As Word.Application
		Dim wdApp As Word.Application
		On Error Resume Next
		wdApp = GetObject( , "Word.Application")
		'Set wdApp = CreateObject("Word.Application")
		If Err.Number <> 0 Then
			wdApp = New Word.Application
		End If
		On Error GoTo 0
		gGetWordApplication = wdApp
	End Function
End Module