Option Strict Off
Option Explicit On
Module modWinAPISleep
	
	'Win API function Sleep
	Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Integer)
End Module