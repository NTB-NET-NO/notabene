Imports System.IO
Imports System.Configuration.ConfigurationSettings
Imports VB = Microsoft.VisualBasic
Imports System.Text
Imports System.Xml.Xsl
Imports System.Xml
Imports System

Friend Class ReportersOnTheRoad
    Inherits System.Windows.Forms.Form

#Region "Windows Form Designer generated code "
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer


    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents Timer As System.Windows.Forms.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer = New System.Windows.Forms.Timer(Me.components)
        '
        'Timer
        '
        '
        'ReportersOnTheRoad
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(224, 127)
        Me.Location = New System.Drawing.Point(93, 108)
        Me.Name = "ReportersOnTheRoad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ReportersOnTheRoad NET"

    End Sub
#End Region

    'MAPI objects
    Dim mpsSession As MAPI.Session
    Dim cdoInfoStores As MAPI.InfoStores
    Dim cdoInfoStore As MAPI.InfoStore
    ' Dim isInfoStore As MAPI.InfoStore
    Dim fRoot As MAPI.Folder
    Dim fError As MAPI.Folder

    '-- Globale variabler --
    Private strXMLTempDir As String = AppSettings("XmlTempDir")
    Private strXmlInputDir As String = AppSettings("XmlInputDir")
    Private strXmlOutputDir As String = AppSettings("XmlOutputDir")

    Private xslFile As String = AppSettings("xslFile")
    Private strXmlMAPFile As String = AppSettings("XmlMAPFile")
    Private strConvFile As String = AppSettings("ConvFile")

    ' Global switch for utskift av ukonverterte RTF-koder
    Private bKeepRtfCodes As Boolean = LCase(AppSettings("KeepRtfCodes")) = "true" _
                                    Or LCase(AppSettings("KeepRtfCodes")) = "yes" _
                                    Or LCase(AppSettings("KeepRtfCodes")) = "1"

    Private isParEnd As Boolean

    Private htStyles As Hashtable = New Hashtable()
    Private htChars As Hashtable = New Hashtable()


    'Variables to hold INI-values
    'These values are assigned in GetIniValues() in this code
    Dim strInfoStoreName As String
    Dim strProfileName As String
    Dim nRetryInterval As Integer
    Dim nInterval As Integer
    Dim nRTFHeaderSize As Integer
    Dim nMaxMessageSize As Integer
    Dim strCopyMessageType As String
    Dim strMapFile As String
    Dim strHeaderSep As String
    Dim strFixedHeader As String
    Dim strHeaderCrLf As String
    Dim strFileFormat As String
    Dim LOGFILE_SYSTEM As String
    Dim strErrorFolder As String
    Dim strDefaultOutDir As String
    Dim strTempDir As String
    Dim colIniFolders As Collection

    'Sequence number (reset at startup)
    Dim seqno As Short

    'Collection to headers
    Dim coHeaders As Collection
    Dim strSubjectHeader As String
    Dim strFolderHeader As String
    Dim strFileNameHeader As String

    'Collection to hold message folders
    Dim coFolders As Collection

    'Logger object
    Dim Logger As cLogger

    'String Constants
    Const SEP As String = ":"

    Private Sub ReportersOnTheRoad_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        Dim strDisplayName As String
        Dim bStarted As Boolean
        Dim iIniErr As Short
        Dim varFolderName As Object
        Dim iniFile As String

        On Error GoTo Err_Load

        Logger = New cLogger()
        coFolders = New Collection()
        coHeaders = New Collection()

        'Read ini file values
        GetIniValues()
        Logger.Log(LOGFILE_SYSTEM, "Load !s", "Settings read from xml.config-file")

        Logger.Log(LOGFILE_SYSTEM, "Load !s", "Headers read from " & strMapFile)
        GetHeaderFields(strMapFile)

        'This service will be run whenever the timer-event occurs so start the timer
        Timer.Interval = nInterval 'set interval
        Timer.Start() 'enable timer

        Exit Sub

Err_Load:
        Logger.Log(LOGFILE_SYSTEM, "Load !x", Err.Description)
        End

    End Sub

    Private Sub Notify_Timer(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick

        Dim F As cMsgFolder
        Dim M As MAPI.Message
        Dim objNewMessage As MAPI.Message
        Dim objCopyFolder As Object
        Dim blnKeepLooking As Boolean
        Dim blnProcessFolder As Boolean

        On Error GoTo Err_Timer

        'Disable time while processing
        Timer.Stop()

        'Log in if we're not there yet
        Do While fRoot Is Nothing
            GetSession()
            If strErrorFolder <> "" Then
                fError = GetFolderByName(strErrorFolder, fRoot.Folders)
            Else
                fError = Nothing
            End If

            If Not fRoot Is Nothing Then
                MakeFolderCollection()
            End If
        Loop

        Do
            blnKeepLooking = False

            'Folders to be scanned should already be stored in coFolders collection
            For Each F In coFolders

                'If folder is not empty - check it
                If F.Folder.Messages.Count > 0 Then

                    'Check for delay
                    blnProcessFolder = True

                    'Check for delay
                    If F.Delay > 0 Then
                        If F.LastExport.AddSeconds(F.Delay) > Now Then
                            blnProcessFolder = False
                        Else
                            F.LastExport = Now
                        End If
                    End If

                    If blnProcessFolder Then
                        F.Folder.Messages.Sort()
                        M = F.Folder.Messages.GetFirst

                        'Export and copy message
                        ExportMessage(M, F)

                        M = Nothing
                        blnKeepLooking = True
                        Exit For
                    End If
                End If

            Next F
        Loop Until blnKeepLooking = False

        F = Nothing

        Timer.Start()

        Exit Sub

Err_Timer:
        Logger.Log(LOGFILE_SYSTEM, "Timer !x", Err.Description)
        Timer.Start()

    End Sub


    Private Sub ExportMessage(ByRef objMessage As MAPI.Message, ByRef clsMsgFolder As cMsgFolder)

        Dim aField As MAPI.Field
        Dim aHeader As cHeaderField
        Dim filehandle As Object
        Dim fh As Short
        Dim strFileName As String
        Dim tempFileName As String
        Dim outFileName As String
        Dim errmsg As String
        Dim RTFtext As String
        Dim RTFStatus As Short
        Dim bytesRead As Integer
        Dim rtfSize As Integer
        Dim tmr1 As DateTime
        Dim tmr2 As DateTime
        Dim t As Long
        Dim i As Short
        Dim textLen As Integer
        Dim strSubject As String
        ''Dim objCopyFolder As MAPI.Folder
        Dim objCopyMessage As MAPI.Message
        ''Dim wdApp As Word.Application

        On Error GoTo export_err

        tmr1 = Now
        filehandle = FreeFile()

        'Make the file name
        seqno += 1
        If seqno > 9999 Then seqno = 1
        Dim tmpString As String = New String(objMessage.ID)
        tmpString = tmpString.Substring(tmpString.Length - 10).Substring(0, 6)

        strFileName = tmpString

        tmpString = seqno
        tmpString = tmpString.PadLeft(4, "0")

        strFileName = tmpString & strFileName

        'MsgBox(strFileName)

        i = 0
        tempFileName = strTempDir & "\" & strFileName
        Do While Dir(tempFileName) <> ""
            tempFileName = strTempDir & "\" & strFileName & "_" & i
            i += 1
        Loop

        'Status = "Open output file."
        FileOpen(filehandle, tempFileName, OpenMode.Output) 'Open file for output

        'Start of header
        PrintLine(filehandle, strHeaderSep)
        If strFixedHeader <> "" Then PrintLine(filehandle, strFixedHeader)
        PrintLine(filehandle, strFolderHeader & ":" & clsMsgFolder.Folder.Name)
        PrintLine(filehandle, strFileNameHeader & ":" & strFileName)

        With objMessage

            Dim fraAvsender As String
            Dim strBeskjedTilDesk As String
            Dim strStoffgruppe As String
            Dim strTil As String
            Dim TilFolder As String

            'Logger.Log LOGFILE_SYSTEM, "ExportMessage !o", "Reading '" & .Subject & "' (" & Len(.Text) & " chars)"
            tmpString = New String(.Text)
            textLen = tmpString.Length

            strSubject = .Subject

            PrintLine(filehandle, strSubjectHeader & ":" & strSubject)

            On Error Resume Next
            'Loop through and write headers to the output file
            For Each aHeader In coHeaders
                aField = .Fields(aHeader.FormField)
                If Not aField Is Nothing Then
                    If aField.Value <> "" Then
                        If strHeaderCrLf <> "" Then
                            PrintLine(filehandle, aHeader.FipHeader & ":" & Replace(aField.Value, vbCrLf, strHeaderCrLf))
                        Else
                            PrintLine(filehandle, aHeader.FipHeader & ":" & aField.Value)
                        End If
                    Else ''If aHeader.DefaultValue <> "" Then
                        PrintLine(filehandle, aHeader.FipHeader & ":" & aHeader.DefaultValue)
                    End If
                End If
                aField = Nothing
            Next aHeader

            aHeader = Nothing

            'End of header
            PrintLine(filehandle, strHeaderSep)

            'Read RTF text from message
            'rtfsize is redimmed if the text is larger than this
            rtfSize = (2 * textLen) + nRTFHeaderSize
            Do While True
                If rtfSize > nMaxMessageSize Then rtfSize = nMaxMessageSize

                RTFtext = Space(rtfSize)
                RTFStatus = ReadRTF(strProfileName, .ID, .StoreID, RTFtext, bytesRead)
                If RTFStatus <> 0 Then
                    errmsg = "Error reading RTF text"
                    GoTo export_err

                    'redim if the text was longer than the allocated space
                ElseIf bytesRead = Len(RTFtext) Then
                    If rtfSize >= nMaxMessageSize Then
                        errmsg = "Message bigger than the maximum (" & nMaxMessageSize & ")"
                        GoTo export_err
                    End If

                    Logger.Log(LOGFILE_SYSTEM, "ExportMessage !x", "Message too big (" & rtfSize & " allocated) - resizing to " & CInt(rtfSize + (rtfSize / 2)))
                    rtfSize = CInt(rtfSize + (rtfSize / 2))

                Else
                    Exit Do

                End If
            Loop

            'Print RTF to file
            PrintLine(filehandle, Trim(RTFtext))

            'close output file
            FileClose(filehandle)

            'move output file to output folder
            outFileName = clsMsgFolder.OutDir & "\" & strFileName & "." & strFileFormat
            Do While Dir(outFileName) <> ""
                outFileName = clsMsgFolder.OutDir & "\" & strFileName & i & "." & strFileFormat
            Loop
            Rename(tempFileName, outFileName)

            'log with timer
            tmr2 = Now
            t = tmr2.Subtract(tmr1).TotalMilliseconds
            Logger.Log(LOGFILE_SYSTEM, clsMsgFolder.Folder.Name & " !o", outFileName & " (" & textLen & " chars, " & t & " millisecs)")

            'Make copies
            tmpString = .Fields("NTBSendTil").Value
            strTil = tmpString.ToLower()

            Select Case strTil
                Case "ntbnyheter@ntb.no"
                    i = 1
                    strStoffgruppe = "Nyheter"
                Case "ntbreportasje@ntb.no"
                    i = 2
                    strStoffgruppe = "Nyheter"
                Case "ntbsport@ntb.no"
                    i = 3
                    strStoffgruppe = "Sport"
                Case "ntbdirekte@ntb.no"
                    i = 4
                Case Else
                    i = 1
                    strStoffgruppe = "Nyheter"
            End Select


            tmr1 = Now

            'Set message differnet values (if specified)
            .Type = "IPM.Note." & strCopyMessageType
            fraAvsender = .Fields("NTBOpprettetAv").Value
            strBeskjedTilDesk = .Fields("NTBBeskjedTilDesk").Value
            strStoffgruppe = .Fields("NTBStoffgruppe").Value
            strSubject = .Subject.Value

            .Fields("NTBPrioritet").Value = .Fields("NTBPrioritet").Value
            .Fields("NTBDistribusjonsKode").Value = "ALL"
            .Fields("NTBDistribusjon").Value = "ALL"
            .Fields("NTBKanal").Value = "A"
            .Fields("NTBMeldingsSign").Value = .Fields("NTBMeldingsSign").Value
            .Fields("NTBBeskjedTilDesk").Value = fraAvsender & " - Var avsender i Notabene"
            .Fields("FromReportersOnTheRoad").Value = True
            .Fields("NTBMeldingsType").Value = "Standard meldingstype"
            .Fields("NTBReporter").Value = .Fields("NTBReporter").Value
            .Fields("NTBFulltNavn").Value = .Fields("NTBFulltNavn").Value
            .Fields("NTBOpprettetAv").Value = .Fields("NTBOpprettetAv").Value
            .Fields("NTBEpostAdresse").Value = .Fields("NTBEpostAdresse").Value
            .Fields("NTBUtDato").Value = Now
            .Fields("NTBStoffgruppe").Value = strStoffgruppe

            Dim pr_Creator_NAME As Integer
            Dim pr_Originator_ADDR As Integer
            pr_Creator_NAME = &HC1A001E
            pr_Originator_ADDR = &HC1F001E

            .Fields(pr_Creator_NAME).Value = "Notabene"
            .Fields(pr_Originator_ADDR).Value = "Notabene@ntb.no"
            .Fields(pr_Creator_NAME).Value = "Notabene"

            .Update()

            objCopyMessage = .CopyTo(clsMsgFolder.CopyFolders.Item(i).ID)
            objCopyMessage.Unread = True
            objCopyMessage.Update()
            tmr2 = Now
            t = tmr2.Subtract(tmr1).TotalMilliseconds
            Logger.Log(LOGFILE_SYSTEM, clsMsgFolder.Folder.Name & " !c", "'" & strSubject & "' => " & clsMsgFolder.CopyFolders.Item(i).Name & " (" & VB6.Format(t, "#0") & " millisecs)")

            objCopyMessage = Nothing

            'Delete  original message
            '.Delete()

        End With

        'XMLExportMessage(RTFtext, objMessage)
        objMessage.Delete()

        Exit Sub

export_err:
        If errmsg <> "" Then
            Logger.Log(LOGFILE_SYSTEM, "ExportMessage !x", errmsg)
        Else
            Logger.Log(LOGFILE_SYSTEM, "ExportMessage !x", Err.Description)
        End If

        If fError Is Nothing Then
            '    errmsg = "Message '" & msg.Subject & "' deleted from " & folderName
            '    msg.Delete
        Else
            errmsg = "Message '" & objMessage.Subject & "' moved from " & clsMsgFolder.FolderName & " to error folder"
            objMessage.MoveTo(fError.ID)
        End If

    End Sub


    'Public Sub XMLExportMessage(ByRef RTFText As String, ByRef objMessage As MAPI.Message)
    '    'XML setup

    '    Dim xmlMap As XmlDocument = New XmlDocument()
    '    xmlMap.Load(strXmlMAPFile)
    '    Dim strError As String
    '    Dim strFieldName As String
    '    ' Dim strXmlFieldName As String
    '    Dim strXmlFieldContent As String

    '    Init()
    '    Dim strRtf As String
    '    Dim strRtfFile As String
    '    Dim strNitfFile As String
    '    Dim strXHtml As String
    '    Dim strBodyXml As String
    '    Dim strOutFile As String
    '    Dim strXmlHead As String


    '    '  For Each strRtfFile In Directory.GetFiles(strXmlInputDir, "*.rtf")
    '    '' Les inn Rtf-dokumentet fra fil
    '    ' strRtf = LogFile.ReadFile(strRtfFile)
    '    strRtf = RTFText

    '    ' strNitfFile = strXmlOutputDir & "\" & Path.GetFileNameWithoutExtension(strRtfFile) & ".xml"

    '    strNitfFile = strXmlOutputDir & "\" & objMessage.Subject & ".xml"
    '    Dim i As Integer
    '    i = 0
    '    Do While Dir(strNitfFile) <> ""
    '        strNitfFile = strXMLTempDir & "\" & objMessage.Subject & "_" & i & ".xml"
    '        i += 1
    '    Loop


    '    ' Converterer RTF til XML-Body
    '    strBodyXml = Rtf2Xml(strRtf, strNitfFile)

    '    ' Lag XML-header fra Notabene fields
    '    ' Terje! Her kan du legge til kode som lager henter feltverdier fra Notabene
    '    strXmlHead = _LoadNotabeneFieldsHeader(strXmlMAPFile, objMessage, strError)
    '    If strError <> "" Then
    '        'MsgBox(strError)
    '    End If

    '    strXHtml = "<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf
    '    strXHtml &= "<html>" & strXmlHead & strBodyXml & "</html>"

    '    ' Skriv ut XML (XHTML), for debug:
    '    'strOutFile = strXMLTempDir & "\" & Path.GetFileNameWithoutExtension(strRtfFile) & "_body.xml"
    '    strOutFile = strXMLTempDir & "\" & objMessage.Subject & "_body-debug.xml"

    '    Dim t As Integer
    '    t = 0

    '    Do While Dir(strOutFile) <> ""
    '        strOutFile = strXMLTempDir & "\" & objMessage.Subject & "_body-debug" & "_" & t & ".xml"
    '        t += 1
    '    Loop

    '    LogFile.WriteFile(strOutFile, strXHtml, False)

    '    _LagNitfFile(strXHtml, xslFile, strNitfFile)

    '    '   Next

    'End Sub

    'Private Sub Init()
    '    _LoadRtfChars(strConvFile)

    '    If Not Directory.Exists(strXmlTempDir) Then
    '        Directory.CreateDirectory(strXmlTempDir)
    '    End If

    '    If Not Directory.Exists(strXmlOutputDir) Then
    '        Directory.CreateDirectory(strXmlOutputDir)
    '    End If

    'End Sub

    'Private Sub _LoadRtfChars(ByVal strXmlMAPFile As String)
    '    Dim strRtfVal As String
    '    Dim strAnsiVal As String
    '    Dim xmlMap As XmlDocument = New XmlDocument()

    '    htChars.Clear()
    '    xmlMap.Load(strXmlMAPFile)

    '    Dim mapNode As XmlNode
    '    For Each mapNode In xmlMap.SelectNodes("/rtfchar/field")
    '        Try
    '            strRtfVal = mapNode.Attributes("rtf").InnerText
    '            strAnsiVal = mapNode.Attributes("ansi").InnerText
    '            'If strAnsiVal = "" Then
    '            'htChars.Add(strRtfVal, Nothing)
    '            'Else
    '            htChars.Add(strRtfVal, strAnsiVal)
    '            'End If

    '        Catch e As Exception
    '            'MsgBox(e.Message & vbCrLf & e.StackTrace)
    '        End Try
    '    Next
    'End Sub

    'Private Function _LoadNotabeneFieldsHeader(ByVal strXmlMAPFile As String, ByRef objMessage As MAPI.Message, ByRef strError As String)
    '    Dim strFieldName As String
    '    Dim strXmlFieldName As String
    '    Dim strXmlFieldContent As String
    '    Dim strXmlHead As String

    '    Dim xmlMap As XmlDocument = New XmlDocument()
    '    xmlMap.Load(strXmlMAPFile)

    '    strError = ""
    '    strXmlHead = "<head>" & vbCrLf

    '    AddXmlHeadNode(strXmlHead, "timestamp", Format(Now, "yyyy.MM.dd HH:mm:ss"))

    '    Dim mapNode As XmlNode
    '    For Each mapNode In xmlMap.SelectNodes("/notabene/field")
    '        Try
    '            strFieldName = mapNode.Attributes("notabene").InnerText
    '            strXmlFieldName = mapNode.Attributes("xml").InnerText

    '            ' Terje! Her kan du legge til en funksjon GetNotabeneFieldVal som lager henter feltverdier fra Notabene
    '            'strXmlFieldContent = GetNotabeneFieldVal(strFieldName)
    '            strXmlFieldContent = objMessage.Fields(strFieldName).Value

    '            ' Kode som legger til xml 
    '            AddXmlHeadNode(strXmlHead, strXmlFieldName, strXmlFieldContent)
    '        Catch e As Exception
    '            strError = e.Message & vbCrLf & e.StackTrace
    '        End Try
    '    Next

    '    strXmlHead &= "</head>" & vbCrLf
    '    Return strXmlHead
    'End Function

    'Private Sub AddXmlHeadNode(ByRef strXmlHead As String, ByVal strName As String, ByVal strContent As String)
    '    strXmlHead &= "<meta name=""" & strName & """ content=""" & strContent & """/>" & vbCrLf
    'End Sub

    'Public Function Rtf2Xml(ByRef strRtf As String, ByVal strFileName As String)
    '    Dim strFirstXml As String
    '    Dim strOutFile As String
    '    Dim xmlRtf As XmlDocument = New XmlDocument()
    '    Dim strFileNoExt As String = Path.GetFileNameWithoutExtension(strFileName)

    '    ' Lag f�rste versjon av XML
    '    strFirstXml = _MakeFirstXml(strRtf)

    '    ' Skriv ut for debug:
    '    strOutFile = strXMLTempDir & "\" & strFileNoExt & "_first.xml"

    '    Dim i As Integer
    '    i = 0
    '    Do While Dir(strOutFile) <> ""
    '        strOutFile = strXMLTempDir & "\" & strFileNoExt & i & "_first.xml"
    '        i += 1
    '    Loop
    '    LogFile.WriteFile(strOutFile, strFirstXml, False)

    '    ' Legg XML-string in i et XMLDocument-objekt
    '    xmlRtf.LoadXml(strFirstXml)

    '    ' Ekstraher alle stilene inn i en HashTabell
    '    _LoadRtfStyles(xmlRtf)

    '    Dim strBodyXml As String

    '    ' Lag XML-header:
    '    strBodyXml &= "<body>" & vbCrLf

    '    ' Behandle avsnittene i teksten
    '    Dim pardNodelist As XmlNodeList
    '    pardNodelist = xmlRtf.SelectNodes("/rtf/group/pard")
    '    Dim pardNode As XmlNode
    '    Dim strPard, strP, strStyleName As String

    '    ' For hvert avsnitt med ny stil
    '    isParEnd = True
    '    For Each pardNode In pardNodelist
    '        strPard = pardNode.InnerXml '*** Viser hele XML-innholdet i noden. Kun for debug.

    '        ' Finn stilnavnet
    '        strStyleName = _HentStil(pardNode.FirstChild.InnerText)

    '        ' Nytt avsnitt:
    '        'strBodyXml &= "<p class=""" & strStyleName & """>"
    '        ' Hent tekst og gj�r BR om til nye avsnitt
    '        _TraverseNodes(pardNode.ChildNodes, strBodyXml, 0, strStyleName)

    '        ' Slutt p� avsnitt:
    '        If Not isParEnd Then
    '            strBodyXml &= "</p>" & vbCrLf
    '            isParEnd = True
    '        End If
    '    Next
    '    strBodyXml &= "</body>"
    '    Return strBodyXml
    'End Function

    'Private Function _MakeFirstXml(ByRef strRtf As String) As String
    '    Dim sbRtf As StringBuilder = New StringBuilder()
    '    sbRtf.Append("<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>")
    '    sbRtf.Append("<rtf>" & strRtf & "</rtf>")

    '    sbRtf.Replace("{", "<group>")
    '    sbRtf.Replace("}", "</group>")

    '    sbRtf.Replace("\pard", "<pard/>")
    '    sbRtf.Replace("\par ", "<br/>")
    '    sbRtf.Replace("\par", "<br/>")

    '    sbRtf.Replace("\trowd", "<trowd/>")
    '    sbRtf.Replace("\cell", "<cell/>")
    '    sbRtf.Replace("\row", "<row/>")

    '    ' Fjerner et un�dig <br/> der det allikevel er slutt p� avsnitt:
    '    'sbRtf.Replace("<br/></group>", "</group>")

    '    sbRtf.Replace("<pard/>", "</pard><pard>")

    '    ' Lagger til siste </pard>:
    '    sbRtf.Replace("</group></rtf>", "</pard></group></rtf>")

    '    ' Fjerner f�rste </pard> som ikke kan v�re start-tag, og returnerer ny XML
    '    Return Replace(sbRtf.ToString, "</pard>", "", 1, 1)

    'End Function

    'Private Sub _LoadRtfStyles(ByRef xmlRtf As XmlDocument)
    '    Dim strStyle As String
    '    Dim strStyleName As String
    '    Dim strStyleNum As String
    '    Dim intStart As Integer
    '    Dim intSlutt As Integer

    '    htStyles.Clear()

    '    'Ekstraherer rtf-stylesheet til Hashtabell
    '    Dim styleNodelist As XmlNodeList

    '    On Error Resume Next ' obs obs
    '    'Finner alle stil-noder:
    '    styleNodelist = xmlRtf.SelectNodes("/rtf/group/group[text()='\stylesheet']/group")
    '    Dim styleNode As XmlNode
    '    For Each styleNode In styleNodelist
    '        strStyle = styleNode.InnerText
    '        intStart = strStyle.IndexOf("\s", 0)
    '        intSlutt = strStyle.IndexOf("\", intStart + 1)
    '        If intSlutt = -1 Or intStart = -1 Then
    '            strStyleNum = "0"
    '        Else
    '            strStyleNum = strStyle.Substring(intStart + 1, intSlutt - intStart - 1)
    '            'End If

    '            'If strStyleNum <> "*" Then
    '            intStart = strStyle.IndexOf("\snext", 1)
    '            intStart = strStyle.IndexOf(" ", intStart)
    '            strStyleName = strStyle.Substring(intStart + 1, strStyle.Length - intStart - 2)
    '            strStyleName = strStyleName.Replace("\'f8", "�")
    '            htStyles.Add(strStyleNum, strStyleName)
    '        End If
    '    Next
    'End Sub

    'Function _HentStil(ByVal strStyle As String) As String
    '    Dim strStyleNum, strStyleName As String
    '    Dim intStart, intSlutt As Integer
    '    ' Finn stilnavnet 
    '    intStart = strStyle.IndexOf("\s", 1)
    '    intSlutt = strStyle.IndexOf("\", intStart + 1)
    '    strStyleNum = strStyle.Substring(intStart + 1, intSlutt - intStart - 1).Trim
    '    strStyleName = htStyles(strStyleNum)
    '    If strStyleName = "" Then
    '        strStyleName = "normal"
    '    End If
    '    Return strStyleName
    'End Function

    'Private Sub _TraverseNodes(ByVal pardNodes As XmlNodeList, ByRef strBodyXml As String, ByVal intLevel As Integer, ByVal strStyleName As String)
    '    Dim strP As String

    '    Dim pNode As XmlNode
    '    For Each pNode In pardNodes
    '        strP = pNode.InnerXml '*** Viser hele XML-innholdet i noden. Kun for debug.

    '        Select Case pNode.NodeType
    '            Case XmlNodeType.Element
    '                If pNode.Name = "br" Then
    '                    If isParEnd Then
    '                        'strBodyXml &= "<br/>" & vbCrLf
    '                        strBodyXml &= "<p class=""" & strStyleName & """/>" & vbCrLf
    '                        isParEnd = True
    '                    Else
    '                        strBodyXml &= "</p>" & vbCrLf '& "<p class=""" & strStyleName & """>"
    '                        isParEnd = True
    '                    End If
    '                End If
    '                If intLevel < 1 Then
    '                    'Rekursivt kall ett niv� ned i Node-treet:
    '                    _TraverseNodes(pNode.ChildNodes, strBodyXml, intLevel + 1, strStyleName)
    '                End If
    '            Case XmlNodeType.Text
    '                If intLevel = 1 Then
    '                    If isParEnd Then
    '                        strBodyXml &= "<p class=""" & strStyleName & """>" & _SubstChars(pNode.InnerText)
    '                        isParEnd = False
    '                    Else
    '                        strBodyXml &= _SubstChars(pNode.InnerText)
    '                    End If
    '                End If
    '            Case Else
    '                ' Will not appear
    '        End Select
    '    Next

    'End Sub

    'Sub _LagNitfFile(ByRef strBodyXml As String, ByVal xslFile As String, ByVal strNitfFile As String)
    '    Dim xwNitf As XmlTextWriter = New XmlTextWriter(strNitfFile, System.Text.Encoding.GetEncoding("iso-8859-1"))
    '    'Dim xwNitf As TextWriter = New StreamWriter(strNitfFile, False, System.Text.Encoding.GetEncoding("iso-8859-1"))

    '    'Har problemer med formattering .NET XslTransform, b�r bruke MSXML4.dll i stedet!
    '    '.NET XslTransform gir ny linje f�r end-tag og klarer ikke timme tagger som: <p/> (skriver alltid <p></p>)

    '    Dim xmlBody As XmlDocument = New XmlDocument()
    '    xmlBody.LoadXml(strBodyXml)
    '    Dim xslNitf As XslTransform = New XslTransform()
    '    xwNitf.Formatting = Formatting.Indented
    '    xwNitf.IndentChar = Chr(9)
    '    xwNitf.Indentation = 0

    '    xwNitf.WriteStartDocument(True)

    '    xslNitf.Load(xslFile)
    '    xslNitf.Transform(xmlBody, Nothing, xwNitf)

    '    xwNitf.Flush()
    '    xwNitf.Close()
    'End Sub


    '' Kun for testing
    'Private Sub _AddAttrXmlMAPFile()
    '    ' Legger til nye atributter i en eksisterende xml-fil
    '    Dim xmlMap As XmlDocument = New XmlDocument()
    '    Dim strFieldName As String

    '    xmlMap.Load(strXmlMAPFile)

    '    Dim mapAttributes As XmlAttributeCollection
    '    Dim attr As XmlAttribute

    '    Dim mapNode As XmlNode
    '    For Each mapNode In xmlMap.SelectNodes("/notabene/field")
    '        strFieldName = mapNode.Attributes("notabene").InnerText
    '        'strXmlFieldName = mapNode.Attributes("xml").InnerText

    '        attr = xmlMap.CreateNode(XmlNodeType.Attribute, "xml", "")
    '        attr.Value = strFieldName

    '        mapAttributes = mapNode.Attributes
    '        mapAttributes.Append(attr)

    '        ' For debug:
    '        strFieldName = mapNode.OuterXml
    '    Next

    '    xmlMap.Save(Path.GetFullPath(strXmlMAPFile) & Path.GetFileNameWithoutExtension(strXmlMAPFile) & "_new.xml")

    'End Sub

    'Function _SubstChars(ByVal strLine As String) As String
    '    ' Bytter Internationale tegn fra RTF-coder
    '    'Dim chrLine(), chrNewLine(Len(strLine)) As Char
    '    Dim chrA As Char
    '    Dim i As Integer
    '    Dim strNewLine As String
    '    Dim strNewChar As String
    '    Dim bEscape As Boolean
    '    Dim bEscapeEnd As Boolean
    '    Dim strCharEscape As String

    '    'Return strLine

    '    Dim strChar As String
    '    For i = 0 To strLine.Length - 1
    '        chrA = strLine.Substring(i, 1)
    '        Select Case chrA
    '            Case "\"
    '                If bEscape Then
    '                    bEscapeEnd = True
    '                    i -= 1
    '                Else
    '                    If strLine.Substring(i + 1, 1) = "'" Then
    '                        '"\'f8"
    '                        strChar = strLine.Substring(i, 4)
    '                        strNewChar = htChars(strChar)
    '                        If strNewChar <> "" Then
    '                            strNewLine &= strNewChar
    '                        Else
    '                            strNewLine &= strChar
    '                        End If
    '                        i += 3
    '                    Else
    '                        bEscape = True
    '                        strCharEscape = "\"
    '                    End If
    '                End If
    '            Case Chr(13)
    '                If bEscape Then
    '                    bEscapeEnd = True
    '                End If
    '                ' Skip Cr
    '            Case Chr(10)
    '                ' Skip Lf
    '            Case " "
    '                If bEscape Then
    '                    bEscapeEnd = True
    '                Else
    '                    strNewLine &= strLine.Substring(i, 1)
    '                End If
    '            Case Else
    '                If bEscape Then
    '                    strCharEscape &= strLine.Substring(i, 1)
    '                Else
    '                    strNewLine &= strLine.Substring(i, 1)
    '                End If
    '        End Select
    '        If bEscapeEnd Then
    '            strNewChar = htChars(strCharEscape)
    '            If strNewChar Is Nothing And bKeepRtfCodes Then
    '                strNewLine &= strCharEscape
    '            ElseIf strNewChar = "" Then
    '                ' Empty
    '            Else
    '                strNewLine &= strNewChar
    '            End If
    '            bEscape = False
    '            bEscapeEnd = False
    '            strCharEscape = ""
    '        End If

    '    Next

    '    Return strNewLine

    'End Function




    Private Sub MakeFolderCollection()
        Dim varIniFolder As Object
        Dim clsMsgFolder As cMsgFolder
        Dim colCopyFolders As Collection
        Dim objFolder As MAPI.Folder
        Dim arrCopyFolders() As String
        Dim i As Short

        'make folder collection of out-folders
        For Each varIniFolder In colIniFolders
            clsMsgFolder = New cMsgFolder()
            clsMsgFolder.FolderName = varIniFolder.Folder
            clsMsgFolder.Folder = GetFolderByName(varIniFolder.Folder, (fRoot.Folders))

            clsMsgFolder.OutDir = varIniFolder.OutDir
            clsMsgFolder.Delay = varIniFolder.Delay

            Logger.Log(LOGFILE_SYSTEM, "Folders !s", varIniFolder.Folder & " (" & clsMsgFolder.Folder.Name & ")")

            If varIniFolder.CopyFolders <> "" Then
                colCopyFolders = New Collection()
                arrCopyFolders = Split(varIniFolder.CopyFolders, ";")
                For i = 0 To arrCopyFolders.GetUpperBound(0)
                    objFolder = GetFolderByName(arrCopyFolders(i), (fRoot.Folders))
                    colCopyFolders.Add(objFolder)
                    Logger.Log(LOGFILE_SYSTEM, "Folders !s", "= Copy to: " & arrCopyFolders(i) & " (" & objFolder.Name & ")")
                    objFolder = Nothing
                Next
                clsMsgFolder.CopyFolders = colCopyFolders
            End If
            Logger.Log(LOGFILE_SYSTEM, "Folders !s", "= Outdir: " & clsMsgFolder.OutDir)
            Logger.Log(LOGFILE_SYSTEM, "Folders !s", "= Delay: " & clsMsgFolder.Delay)

            coFolders.Add(clsMsgFolder)

            colCopyFolders = Nothing
            clsMsgFolder = Nothing

        Next varIniFolder

        'Release memory
        colIniFolders = Nothing
    End Sub

    Private Sub GetHeaderFields(ByRef mapFile As String)
        'Date:          2000.02.01
        'Description:   Retrieves a list of header fields

        Dim thisheader As cHeaderField
        Dim strLine As String
        Dim strType As String
        Dim strFipHdr As String
        Dim strFormFld As String
        Dim strDefVal As String
        Dim fh As Short
        Dim i As Short
        Dim parts As String()

        fh = FreeFile()
        FileOpen(fh, mapFile, OpenMode.Input)
        strLine = LineInput(fh)
        Do While Not EOF(fh)
            strLine = LTrim(strLine)

            If strLine.Substring(0, 1) <> ";" And strLine.Trim() <> "" Then
                i = strLine.IndexOf(":")
                If i > 0 Then
                    strType = strLine.Substring(0, i)

                    Select Case LCase(strType)
                        Case "fixed"
                            strFixedHeader = strLine.Substring(i + 1)

                        Case "subject"
                            strSubjectHeader = strLine.Substring(i + 1)

                        Case "foldername"
                            strFolderHeader = strLine.Substring(i + 1)

                        Case "filename"
                            strFileNameHeader = strLine.Substring(i + 1)

                        Case Else
                            parts = strLine.Split(":")
                            If parts.GetUpperBound(0) > 0 Then
                                thisheader = New cHeaderField()
                                thisheader.FormField = strType
                                thisheader.FipHeader = parts(1)
                                If parts.GetUpperBound(0) = 2 Then
                                    thisheader.DefaultValue = parts(2)
                                End If
                                coHeaders.Add(thisheader, thisheader.FormField)
                                Logger.Log(LOGFILE_SYSTEM, "GetHeaderFields !s", thisheader.FormField & " -> " & thisheader.FipHeader & " (" & thisheader.DefaultValue & ")")
                                thisheader = Nothing
                            End If

                    End Select
                End If
            End If

            strLine = LineInput(fh)
        Loop
        FileClose(fh)

    End Sub

    Private Sub GetIniValues()

        'Date:          1999.05.21
        'Description:   Assigns values from ini file to variables

        On Error GoTo GetIniError

        Dim strtemp As Object

        'get LOGFILE_SYSTEM location. NB! Read this value first from ini file, or nothing will be logged
        LOGFILE_SYSTEM = AppSettings("LOGFILE")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "-- Logging started --")

        'get output folder
        strDefaultOutDir = AppSettings("DefaultOutDir")

        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "DefaultOutDir=" & strDefaultOutDir)

        If Not Directory.Exists(strDefaultOutDir) Then
            Directory.CreateDirectory(strDefaultOutDir)
            Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "New folder created: " & strDefaultOutDir)
        End If

        strFileFormat = AppSettings("FileFormat").ToLower()
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "FileFormat=" & strFileFormat)

        strTempDir = AppSettings("TempDir")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "TempDir=" & strTempDir)

        If Not Directory.Exists(strTempDir) Then
            Directory.CreateDirectory(strTempDir)
            Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "Directory created: " & strTempDir)
        End If

        '***[Exchange]
        strProfileName = AppSettings("Profile")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "Profile = " & strProfileName)
        strInfoStoreName = AppSettings("InfoStore")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "InfoStore=" & strInfoStoreName)

        'get message type of copies
        strCopyMessageType = AppSettings("CopyMessageType")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "CopyMessageType=" & strCopyMessageType)

        'get root folder

        'get error folder
        strErrorFolder = AppSettings("ErrorFolder")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "ErrorFolder=" & strErrorFolder)

        'get poll interval
        nInterval = AppSettings("Interval") * 1000
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "Interval=" & nInterval)

        'get logon retry interval
        nRetryInterval = AppSettings("RetryInterval") * 1000
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "RetryInterval=" & nRetryInterval)

        '***[Message]

        'get map file
        strMapFile = AppSettings("MapFile")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "MapFile=" & strMapFile)

        'get header separator
        strHeaderSep = AppSettings("HeaderSep")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "HeaderSep=" & strHeaderSep)

        'string to replace crlf
        strHeaderCrLf = AppSettings("HeaderCrLf")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "HeaderCrLf=" & strHeaderCrLf)

        'get expected header size
        nRTFHeaderSize = AppSettings("RTFHeaderSize")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "RTFHeaderSize=" & nRTFHeaderSize)

        'get max message size
        nMaxMessageSize = AppSettings("MaxMessageSize")
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !s", "MaxMessageSize=" & nMaxMessageSize)

        'get a collection of names of folders
        GetFoldersFromIni()

        Exit Sub

GetIniError:
        Logger.Log(LOGFILE_SYSTEM, "GetIniValues !x", Err.Number & " " & Err.Description)

    End Sub

    Sub GetFoldersFromIni()
        Dim clsIniFolder As cIniFolders
        Dim i As Short
        Dim strFolder As String
        Dim strOutDir As String

        colIniFolders = New Collection()

        'get ini values from keys Folder01, Folder02, Folder03...
        i = 1

        Do
            strFolder = AppSettings("Folder" & VB6.Format(i, "00"))
            If strFolder = "" Then
                Exit Do
            Else
                clsIniFolder = New cIniFolders()
                clsIniFolder.Folder = strFolder
                clsIniFolder.CopyFolders = AppSettings("Copies" & VB6.Format(i, "00"))
                clsIniFolder.Delay = AppSettings("Delay" & VB6.Format(i, "00"))
                strOutDir = AppSettings("Outdir" & VB6.Format(i, "00"))

                If strOutDir = "" Then
                    strOutDir = strDefaultOutDir
                Else
                    If Not Directory.Exists(strOutDir) Then
                        Directory.CreateDirectory(strOutDir)
                        Logger.Log(LOGFILE_SYSTEM, "GetFoldersFromIni !s", "** New folder created: " & strOutDir)
                    End If
                End If
                clsIniFolder.OutDir = strOutDir

                colIniFolders.Add(clsIniFolder)
                clsIniFolder = Nothing
            End If

            i = i + 1
        Loop Until i > 99

        'if more than 99 out-folders... raise an error
        If i > 99 Then Err.Raise(vbObjectError + 14, "GetFolders !x", "** Too many folders **")

    End Sub

    Private Sub GetSession()
        Dim strPublicRootID As String
        Dim tagPublicRootEntryID As Integer
        'Dim strProfile As String

        Err.Clear()

        On Error Resume Next

        tagPublicRootEntryID = &H66310102

        'Create new session
        mpsSession = New MAPI.Session()

        'dersom det er angitt et profilnavn i ini fila, bruk den, ellers logg p� den gjeldende session
        Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Profile: " & strProfileName)
        If strProfileName = "" Then
            'logg p� med gjeldende profil
            mpsSession.Logon(, , False, False, 0, True)
        Else
            mpsSession.Logon(ProfileName:=strProfileName, ShowDialog:=False, NewSession:=False)
        End If

        Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Getting InfoStore and RootFolder")
        For Each cdoInfoStore In mpsSession.InfoStores
            If cdoInfoStore.Name = strInfoStoreName Then
                strPublicRootID = cdoInfoStore.Fields(tagPublicRootEntryID).Value
                fRoot = mpsSession.GetFolder(strPublicRootID, cdoInfoStore.ID)
                'isInfoStore = cdoInfoStore.Name
            End If
        Next


        'Check errors
        If Err.Number <> 0 Then
            Logger.Log(LOGFILE_SYSTEM, "GetSession !x", Err.Number & " " & Err.Description)
            mpsSession.Logoff()
            mpsSession = Nothing
            Sleep(nRetryInterval)
        Else
            Logger.Log(LOGFILE_SYSTEM, "GetSession !s", "Logged on to Exchange.")
        End If

    End Sub

    Private Function GetInfoStoreByName(ByRef ses As MAPI.InfoStores, ByRef strInfoStoreName As String) As MAPI.InfoStore

        'Date:          1999.05.21
        'Function:      Retrieves a folder object by supplying a name
        'Author:        Svein Terje Gaup
        'Input:         session object, name of infostore as string
        'Output:        An infostore object
        'Called by:     Post
        'On failure:    Raises error +13. Then ends.

        Dim infs As MAPI.InfoStores
        Dim inf As MAPI.InfoStore

        ' infs = ses.Session

        For Each inf In ses.Session
            If inf.Name = strInfoStoreName Then
                Return inf
            End If
        Next inf

        'if program reaches this point the infostore was not found
        Err.Raise(vbObjectError + 13, "cParseFile.GetInfoStoreByName", "InfoStore not found: " & strInfoStoreName & ". Check ini file.")
        Logger.Log(LOGFILE_SYSTEM, "GetInfoStoreByName !x", "** InfoStore not found: " & strInfoStoreName & " **")

        Return Nothing

    End Function


    Private Function GetFolderByName(ByVal strFolderName As String, ByVal colFolders As MAPI.Folders) As MAPI.Folder

        'Input:     RootFolder, Foldername, Folder collection
        'Output:    Folder object

        Dim F As MAPI.Folder 'For each folder...
        Dim myFolders As Object
        Dim TempF As MAPI.Folder 'Intermediate foldername
        Dim strRemFolderName As String 'Remaining part of folder path
        Dim strSingleFolderName As String 'Single foldername
        Dim tmpString As String
        Dim i As Short
        Dim ourFolders As MAPI.InfoStore

        strRemFolderName = strFolderName

        Do
            i = strRemFolderName.IndexOf("\")
            If i >= 0 Then
                strSingleFolderName = strRemFolderName.Substring(0, i)
            Else
                strSingleFolderName = strRemFolderName
            End If

            'Loop through folders
            Dim bFound As Boolean
            bFound = False
            F = colFolders.GetFirst

            Do While (Not bFound)
                tmpString = F.Name
                If tmpString.ToLower() = strSingleFolderName.ToLower() Then
                    TempF = F
                    'Limiting folders collection to subfolders of current folder
                    colFolders = TempF.Folders
                    bFound = True
                Else
                    F = colFolders.GetNext()
                    'MsgBox(F.Name)
                    bFound = False
                End If
            Loop

            If i >= 0 Then
                strRemFolderName = strRemFolderName.Substring(i + 1)
                bFound = False
            Else
                Return TempF
            End If

        Loop Until i = -1

        'If program reaches here, folder was not found: return Nothing
        MsgBox("Folder not found.", CDbl("Error"))
        Err.Raise(vbObjectError + 12, "Form.GetFolderByName", "Folder not found: " & strRemFolderName & ".")
        Logger.Log(LOGFILE_SYSTEM, "GetFolderByName !x", "** Folder not found: " & strRemFolderName & " **")

        Return Nothing
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class