Option Strict Off
Option Explicit On
Friend Class cLogger
	'Name:          cLogger
	'Date:          99.11.24
	'Author:        Svein Terje Gaup
	'Description:   Class to write data to a log file.
	'Properties:    FileName, Message, Source
	
	
	'local variable(s) to hold property value(s)
	Private mvarFilename As String 'local copy
	Private mvarMessage As String 'local copy
	Private mvarSource As String 'local copy
	
	Public Sub Log(Optional ByRef strFileName As String = "", Optional ByRef strSource As String = "", Optional ByRef strMessage As String = "")
		'Date:          99.11.24
		'Author:        Svein Terje Gaup
		'Description:   This is the main method of this class.
		'               It writes data to a log-file.
		
		Dim filehandle As Short
		
		'If any of the optional parameters are set,
		'change value of properties accordingly.
		If strFileName <> "" Then Me.FileName = strFileName
		If strSource <> "" Then Me.Source = strSource
		If strMessage <> "" Then Me.Message = strMessage
		
		'Get an unused file handle
		filehandle = FreeFile
		
		On Error Resume Next
		
		'If no filename is specified, set it to a default value
		If mvarFilename = "" Then mvarFilename = "distributor.log"
		
		'Open file for appending
		FileOpen(filehandle, mvarFilename, OpenMode.Append)
		
		'If file is not found then create a new one
		If Err.Number <> 0 Then
			FileOpen(filehandle, mvarFilename, OpenMode.Output)
		End If
		
		On Error GoTo 0
		
		'Write data to log-file, then close file
		PrintLine(filehandle, Now & " " & mvarSource & " : " & mvarMessage)
		FileClose(filehandle)
		
	End Sub
	
	Public Sub Init(ByRef strFileName As String, ByRef strSource As String, ByRef strMessage As Object)
		
	End Sub
	
	
	Public Property Source() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Source
			Source = mvarSource
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Source = 5
			mvarSource = Value
		End Set
	End Property
	
	
	Public Property Message() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Message
			Message = mvarMessage
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Message = 5
			mvarMessage = Value
		End Set
	End Property
	
	
	Public Property FileName() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Filename
			FileName = mvarFilename
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Filename = 5
			mvarFilename = Value
		End Set
	End Property
End Class