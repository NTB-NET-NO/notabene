Option Strict Off
Option Explicit On
Friend Class cMsgFolder
	Private objFolder As MAPI.Folder
	Private strFolderName As String
	Private strOutDir As String
	Private intDelay As Short
    Private datLastExport As Date
	Private colCopyFolders As New Collection
	
	'Base name of the path
	
	Public Property FolderName() As String
		Get
			FolderName = strFolderName
		End Get
		Set(ByVal Value As String)
			strFolderName = Value
		End Set
	End Property
	
	'Pointer to the MAPI folder
	
	Public Property Folder() As MAPI.Folder
		Get
			Folder = objFolder
		End Get
		Set(ByVal Value As MAPI.Folder)
			objFolder = Value
		End Set
	End Property
	
	'Pointer to collection of MAPI folders (for copies)
	
	Public Property CopyFolders() As Collection
		Get
			CopyFolders = colCopyFolders
		End Get
		Set(ByVal Value As Collection)
			colCopyFolders = Value
		End Set
	End Property
	
	'Alternative out dir
	
	Public Property OutDir() As String
		Get
			OutDir = strOutDir
		End Get
		Set(ByVal Value As String)
			strOutDir = Value
		End Set
	End Property
	
	'Delay (secs between each export)
	
	Public Property Delay() As Short
		Get
			Delay = intDelay
		End Get
		Set(ByVal Value As Short)
			intDelay = Value
		End Set
	End Property
	
	'Time of the last export
	
	Public Property LastExport() As Date
		Get
			LastExport = datLastExport
		End Get
		Set(ByVal Value As Date)
			datLastExport = Value
		End Set
	End Property
End Class